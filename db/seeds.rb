# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

ConfigModule.create name: 'BigBlueButton', url_local: '/visios', font: 'video', order: 45,
                    color: '#0C66AD', credential: true, active: true

ConfigModule.create name: 'Framemo', url_local: '/memos', font: 'sticky-note', order: 50,
                    color: '#33d17a', active: true

zigouigoui_cm = ConfigModule.create name: 'Zigouigoui', url_local: '/folders', font: 'folder', order: 2, color: '#00FF48',
                                    credential: false, data_loss_on_deactivation: false, active: true

ConfigModule.create name: 'Etherpad', url_local: '/pads', font: 'file-alt', order: 60,
                    color: '#0cb75f', active: true

sf_cm = ConfigModule.create name: 'ShuttleForm', url_local: '/shuttle_forms', font: 'route', order: 70,
                            color: '#0cb75f', data_loss_on_deactivation: false, active: true
Instance.create(config_module: sf_cm, name: 'ShuttleForm', description: 'ShuttleForm', active: true)
Instance.create(config_module: zigouigoui_cm, name: 'Zigouigoui', description: 'Zigouigoui', active: true)
