# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.2].define(version: 2025_01_14_082730) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.boolean "deleted", default: false
    t.string "permissions"
    t.bigint "classroom_id"
    t.datetime "updated_at"
    t.string "filename_audit"
    t.string "content_audit"
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["classroom_id"], name: "index_active_storage_attachments_on_classroom_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "archives", force: :cascade do |t|
    t.string "archivable_type"
    t.bigint "archivable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "participant_id", null: false
    t.index ["archivable_type", "archivable_id"], name: "index_archive_modules_on_archivable"
    t.index ["participant_id"], name: "index_archives_on_participant_id"
  end

  create_table "audits", force: :cascade do |t|
    t.integer "auditable_id"
    t.string "auditable_type"
    t.integer "associated_id"
    t.string "associated_type"
    t.integer "user_id"
    t.string "user_type"
    t.string "username"
    t.string "action"
    t.text "audited_changes"
    t.integer "version", default: 0
    t.string "comment"
    t.string "remote_address"
    t.string "request_uuid"
    t.datetime "created_at"
    t.index ["associated_type", "associated_id"], name: "associated_index"
    t.index ["auditable_type", "auditable_id", "version"], name: "auditable_index"
    t.index ["created_at"], name: "index_audits_on_created_at"
    t.index ["request_uuid"], name: "index_audits_on_request_uuid"
    t.index ["user_id", "user_type"], name: "user_index"
  end

  create_table "bookmarks", force: :cascade do |t|
    t.string "bookmarkable_type"
    t.bigint "bookmarkable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "participant_id", null: false
    t.index ["bookmarkable_type", "bookmarkable_id"], name: "index_bookmarks_on_bookmarkable"
    t.index ["participant_id"], name: "index_bookmarks_on_participant_id"
  end

  create_table "classrooms", force: :cascade do |t|
    t.string "code"
    t.string "status"
    t.string "place"
    t.integer "quota", default: 20
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "school_id"
    t.string "graf_id"
    t.string "name"
    t.bigint "skill_block_id", null: false
    t.date "start_date"
    t.date "end_date"
    t.boolean "enabled", default: true, null: false
    t.index ["graf_id"], name: "index_classrooms_on_graf_id", unique: true
    t.index ["school_id"], name: "index_classrooms_on_school_id"
    t.index ["skill_block_id"], name: "index_classrooms_on_skill_block_id"
  end

  create_table "config_modules", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "title"
    t.string "icon"
    t.text "resume"
    t.boolean "active", default: false
    t.string "url"
    t.string "url_local"
    t.boolean "new_page", default: false
    t.boolean "beta", default: false
    t.string "font"
    t.integer "order", default: 0
    t.string "domain_datas"
    t.boolean "core"
    t.string "instance_datas"
    t.string "user_datas"
    t.string "color"
    t.boolean "data_loss_on_deactivation", default: true
    t.boolean "credential", default: false
  end

  create_table "config_modules_participants", id: false, force: :cascade do |t|
    t.bigint "participant_id", null: false
    t.bigint "config_module_id", null: false
    t.index ["config_module_id"], name: "index_config_modules_participants_on_config_module_id"
    t.index ["participant_id"], name: "index_config_modules_participants_on_participant_id"
  end

  create_table "domain_infos", id: :serial, force: :cascade do |t|
    t.integer "config_module_id"
    t.boolean "active", default: true
    t.integer "instance_id"
    t.bigint "classroom_id", null: false
    t.index ["classroom_id"], name: "index_domain_infos_on_classroom_id"
    t.index ["config_module_id"], name: "index_domain_infos_on_config_module_id"
    t.index ["instance_id"], name: "index_domain_infos_on_instance_id"
  end

  create_table "folders", force: :cascade do |t|
    t.string "name"
    t.text "permissions"
    t.integer "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "classroom_id", default: 171, null: false
    t.boolean "automatic", default: false
    t.bigint "teaching_unit_id"
    t.boolean "deleted", default: false
    t.bigint "participant_id"
    t.text "comment"
    t.index ["classroom_id"], name: "index_folders_on_classroom_id"
    t.index ["participant_id"], name: "index_folders_on_participant_id"
    t.index ["teaching_unit_id"], name: "index_folders_on_teaching_unit_id"
  end

  create_table "groups", force: :cascade do |t|
    t.bigint "classroom_id", null: false
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "automatic"
    t.index ["classroom_id"], name: "index_groups_on_classroom_id"
  end

  create_table "groups_participants", id: false, force: :cascade do |t|
    t.bigint "participant_id", null: false
    t.bigint "group_id", null: false
    t.index ["group_id"], name: "index_groups_participants_on_group_id"
    t.index ["participant_id"], name: "index_groups_participants_on_participant_id"
  end

  create_table "instances", id: :serial, force: :cascade do |t|
    t.integer "config_module_id"
    t.string "name"
    t.text "description"
    t.boolean "active", default: true
    t.string "datas"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "instance_state_id", default: 1
    t.string "url"
    t.string "credential_username"
    t.string "credential_password"
    t.index ["config_module_id"], name: "index_instances_on_config_module_id"
    t.index ["instance_state_id"], name: "index_instances_on_instance_state_id"
  end

  create_table "invitations", force: :cascade do |t|
    t.string "invitable_type"
    t.bigint "invitable_id"
    t.string "recipient_type"
    t.bigint "recipient_id"
    t.string "external_recipient"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["invitable_type", "invitable_id"], name: "index_invitations_on_invitable"
    t.index ["recipient_type", "recipient_id"], name: "index_invitations_on_recipient"
  end

  create_table "learnings", force: :cascade do |t|
    t.integer "status"
    t.integer "graf_id"
    t.bigint "skill_block_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["skill_block_id"], name: "index_learnings_on_skill_block_id"
  end

  create_table "learnings_tutorings", id: false, force: :cascade do |t|
    t.bigint "learning_id"
    t.bigint "tutoring_id"
    t.index ["learning_id"], name: "index_learnings_tutorings_on_learning_id"
    t.index ["tutoring_id"], name: "index_learnings_tutorings_on_tutoring_id"
  end

  create_table "links", id: :serial, force: :cascade do |t|
    t.string "title"
    t.text "resume"
    t.string "url"
    t.string "font"
    t.string "color"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "symbol"
    t.boolean "global", default: false
    t.integer "participant_id", null: false
    t.index ["participant_id"], name: "index_links_on_participant_id"
  end

  create_table "meetings", force: :cascade do |t|
    t.string "name"
    t.datetime "started_at"
    t.datetime "ended_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "meeting_id"
    t.text "description"
    t.bigint "participant_id", null: false
    t.index ["meeting_id"], name: "index_meetings_on_meeting_id"
    t.index ["participant_id"], name: "index_meetings_on_participant_id"
  end

  create_table "memos", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "comment"
    t.string "uuid"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "participant_id", null: false
    t.index ["participant_id"], name: "index_memos_on_participant_id"
  end

  create_table "messages", id: :serial, force: :cascade do |t|
    t.string "message", null: false
    t.string "user_id", null: false
    t.integer "module_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "module_tag"
    t.integer "event_id"
    t.index ["module_id"], name: "index_messages_on_module_id"
  end

  create_table "pads", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "comment"
    t.string "uuid"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "full_url"
    t.bigint "config_module_id", null: false
    t.bigint "participant_id", null: false
    t.index ["config_module_id"], name: "index_pads_on_config_module_id"
    t.index ["participant_id"], name: "index_pads_on_participant_id"
  end

  create_table "participants", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "classroom_id", null: false
    t.string "participable_type", null: false
    t.bigint "participable_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "enabled", default: true, null: false
    t.datetime "last_login"
    t.string "token"
    t.index ["classroom_id"], name: "index_participants_on_classroom_id"
    t.index ["participable_type", "participable_id"], name: "index_participants_on_participable"
    t.index ["user_id"], name: "index_participants_on_user_id"
  end

  create_table "schools", force: :cascade do |t|
    t.string "name"
    t.string "place"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "graf_id"
    t.index ["graf_id"], name: "index_schools_on_graf_id", unique: true
  end

  create_table "shuttle_forms", force: :cascade do |t|
    t.bigint "teaching_unit_id", null: false
    t.bigint "learning_id", null: false
    t.text "learner_theory1"
    t.text "learner_theory2"
    t.text "learner_theory3"
    t.text "learner_theory4"
    t.text "learner_theory5"
    t.datetime "learner_theory_signed_at"
    t.text "teacher_theory1"
    t.datetime "teacher_theory_signed_at"
    t.text "learner_training1"
    t.text "learner_training2"
    t.text "learner_training3"
    t.text "learner_training4"
    t.text "learner_training5"
    t.text "learner_training6"
    t.datetime "learner_training_signed_at"
    t.text "tutor_training1"
    t.text "tutor_training2"
    t.text "tutor_training3"
    t.text "tutor_training4"
    t.text "tutor_training5"
    t.text "tutor_training6"
    t.datetime "tutor_training_signed_at"
    t.text "teacher_training1"
    t.datetime "teacher_training_signed_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "teacher_theory_signed_by_id"
    t.bigint "teacher_training_signed_by_id"
    t.bigint "tutor_training_signed_by_id"
    t.integer "learner_status", default: 0
    t.integer "teacher_status", default: 0
    t.integer "tutor_status", default: 0
    t.boolean "learner_training_signed"
    t.boolean "learner_theory_signed"
    t.index ["learner_status"], name: "index_shuttle_forms_on_learner_status"
    t.index ["learning_id"], name: "index_shuttle_forms_on_learning_id"
    t.index ["teacher_status"], name: "index_shuttle_forms_on_teacher_status"
    t.index ["teacher_theory_signed_by_id"], name: "index_shuttle_forms_on_teacher_theory_signed_by_id"
    t.index ["teacher_training_signed_by_id"], name: "index_shuttle_forms_on_teacher_training_signed_by_id"
    t.index ["teaching_unit_id"], name: "index_shuttle_forms_on_teaching_unit_id"
    t.index ["tutor_status"], name: "index_shuttle_forms_on_tutor_status"
    t.index ["tutor_training_signed_by_id"], name: "index_shuttle_forms_on_tutor_training_signed_by_id"
  end

  create_table "skill_blocks", force: :cascade do |t|
    t.integer "graf_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["graf_id"], name: "index_skill_blocks_on_graf_id", unique: true
  end

  create_table "skill_units", force: :cascade do |t|
    t.string "code"
    t.bigint "classroom_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "skill_block_id", null: false
    t.integer "graf_id"
    t.index ["classroom_id"], name: "index_skill_units_on_classroom_id"
    t.index ["skill_block_id"], name: "index_skill_units_on_skill_block_id"
  end

  create_table "skill_weeks", force: :cascade do |t|
    t.integer "week"
    t.bigint "skill_unit_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["skill_unit_id"], name: "index_skill_weeks_on_skill_unit_id"
    t.index ["week", "skill_unit_id"], name: "index_skill_weeks_on_week_and_skill_unit_id", unique: true
  end

  create_table "survey_answers", id: :serial, force: :cascade do |t|
    t.integer "survey_option_id"
    t.string "uuid"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "survey_options", id: :serial, force: :cascade do |t|
    t.integer "survey_id"
    t.string "choice"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "surveys", id: :serial, force: :cascade do |t|
    t.datetime "ended_at", precision: nil
    t.string "question"
    t.string "comment"
    t.string "uuid"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "owner_id"
    t.boolean "allow_multiple"
    t.boolean "anonymous", default: false
    t.bigint "user_id", null: false
    t.index ["user_id"], name: "index_surveys_on_user_id"
  end

  create_table "teaching_sequences", force: :cascade do |t|
    t.bigint "skill_unit_id", null: false
    t.integer "graf_id"
    t.bigint "teaching_unit_id", null: false
    t.datetime "started_at"
    t.datetime "ended_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.text "description"
    t.index ["skill_unit_id"], name: "index_teaching_sequences_on_skill_unit_id"
    t.index ["teaching_unit_id"], name: "index_teaching_sequences_on_teaching_unit_id"
  end

  create_table "teaching_units", force: :cascade do |t|
    t.string "name"
    t.integer "graf_id"
    t.bigint "classroom_id", null: false
    t.date "start_date"
    t.date "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "training_end_date"
    t.index ["classroom_id"], name: "index_teaching_units_on_classroom_id"
  end

  create_table "teachings", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "responsible", default: false
  end

  create_table "tutorings", force: :cascade do |t|
    t.integer "graf_id"
    t.date "start_date"
    t.date "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "graf_id"
    t.string "first_name"
    t.string "last_name"
    t.string "graf_status"
    t.boolean "superadmin", default: false
    t.string "theme"
    t.boolean "dark_mode", default: false
    t.string "sso_clear_password"
    t.string "time_zone"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["graf_id"], name: "index_users_on_graf_id", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "visios", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "comment"
    t.datetime "started_at", precision: nil
    t.integer "duration"
    t.integer "number_of_participants"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "uuid"
    t.string "friendly_id"
    t.bigint "config_module_id", default: 10, null: false
    t.bigint "participant_id", null: false
    t.boolean "allow_recording"
    t.boolean "anyone_can_start"
    t.boolean "mute_on_start"
    t.boolean "anyone_join_as_moderator"
    t.boolean "moderator_approval"
    t.index ["config_module_id"], name: "index_visios_on_config_module_id"
    t.index ["participant_id"], name: "index_visios_on_participant_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_attachments", "classrooms"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "archives", "participants"
  add_foreign_key "bookmarks", "participants"
  add_foreign_key "classrooms", "schools"
  add_foreign_key "classrooms", "skill_blocks"
  add_foreign_key "config_modules_participants", "config_modules"
  add_foreign_key "config_modules_participants", "participants"
  add_foreign_key "domain_infos", "classrooms"
  add_foreign_key "domain_infos", "config_modules"
  add_foreign_key "domain_infos", "instances"
  add_foreign_key "folders", "classrooms"
  add_foreign_key "folders", "participants"
  add_foreign_key "folders", "teaching_units"
  add_foreign_key "groups", "classrooms"
  add_foreign_key "groups_participants", "groups"
  add_foreign_key "groups_participants", "participants"
  add_foreign_key "instances", "config_modules"
  add_foreign_key "learnings", "skill_blocks"
  add_foreign_key "links", "participants"
  add_foreign_key "meetings", "meetings"
  add_foreign_key "meetings", "participants"
  add_foreign_key "memos", "participants"
  add_foreign_key "pads", "config_modules"
  add_foreign_key "pads", "participants"
  add_foreign_key "participants", "classrooms"
  add_foreign_key "participants", "users"
  add_foreign_key "shuttle_forms", "learnings"
  add_foreign_key "shuttle_forms", "teaching_units"
  add_foreign_key "shuttle_forms", "users", column: "teacher_theory_signed_by_id"
  add_foreign_key "shuttle_forms", "users", column: "teacher_training_signed_by_id"
  add_foreign_key "shuttle_forms", "users", column: "tutor_training_signed_by_id"
  add_foreign_key "skill_units", "classrooms"
  add_foreign_key "skill_units", "skill_blocks"
  add_foreign_key "skill_weeks", "skill_units"
  add_foreign_key "surveys", "users"
  add_foreign_key "teaching_sequences", "skill_units"
  add_foreign_key "teaching_sequences", "teaching_units"
  add_foreign_key "teaching_units", "classrooms"
  add_foreign_key "visios", "config_modules"
  add_foreign_key "visios", "participants"
end
