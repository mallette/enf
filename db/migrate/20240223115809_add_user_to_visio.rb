class AddUserToVisio < ActiveRecord::Migration[7.0]
  def change
    add_reference :visios, :user, null: false, foreign_key: true
    remove_column :visios, :owner_id, type: 'string'
  end
end
