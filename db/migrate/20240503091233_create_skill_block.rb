class CreateSkillBlock < ActiveRecord::Migration[7.1]
  def change
    create_table :skill_blocks do |t|
      t.integer :graf_id

      t.timestamps
    end
  end
end
