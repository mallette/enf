class RenameArchivableModule < ActiveRecord::Migration[7.0]
  def change
    rename_table :archive_modules, :archives
  end
end
