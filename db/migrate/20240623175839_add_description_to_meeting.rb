class AddDescriptionToMeeting < ActiveRecord::Migration[7.1]
  def change
    add_column :meetings, :description, :text
  end
end
