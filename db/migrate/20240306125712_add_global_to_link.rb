class AddGlobalToLink < ActiveRecord::Migration[7.0]
  def change
    add_column :links, :global, :boolean, default: false
  end
end
