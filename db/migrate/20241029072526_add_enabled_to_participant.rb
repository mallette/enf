class AddEnabledToParticipant < ActiveRecord::Migration[7.2]
  def change
    add_column :participants, :enabled, :boolean, null: false, default: true
  end
end
