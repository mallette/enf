class AddClassroomToMemo < ActiveRecord::Migration[7.0]
  def change
    add_reference :memos, :classroom, null: false, foreign_key: true
  end
end
