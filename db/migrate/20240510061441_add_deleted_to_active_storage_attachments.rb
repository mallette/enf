class AddDeletedToActiveStorageAttachments < ActiveRecord::Migration[7.1]
  def change
    add_column :active_storage_attachments, :deleted, :boolean, default: false
    add_column :active_storage_attachments, :permissions, :string
    add_reference :active_storage_attachments, :classroom, null: true, foreign_key: true
  end
end
