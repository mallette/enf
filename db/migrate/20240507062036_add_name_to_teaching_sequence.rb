class AddNameToTeachingSequence < ActiveRecord::Migration[7.1]
  def change
    add_column :teaching_sequences, :name, :string
    add_column :teaching_sequences, :description, :text
  end
end
