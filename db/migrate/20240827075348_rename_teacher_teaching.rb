class RenameTeacherTeaching < ActiveRecord::Migration[7.2]
  def change
    rename_table :teachers, :teachings
  end
end
