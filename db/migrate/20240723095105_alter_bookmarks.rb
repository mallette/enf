class AlterBookmarks < ActiveRecord::Migration[7.1]
  def change
    add_reference :bookmarks, :participant, null: false, foreign_key: true
    remove_column :bookmarks, :user_id
  end
end
