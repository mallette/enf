class AddReferenceToInvitationUser < ActiveRecord::Migration[7.0]
  def change
    add_reference :invitation_users, :user, null: true, foreign_key: true
  end
end
