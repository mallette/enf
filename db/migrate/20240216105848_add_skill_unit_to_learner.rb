class AddSkillUnitToLearner < ActiveRecord::Migration[7.0]
  def change
    add_reference :learners, :skill_unit, null: false, foreign_key: true
  end
end
