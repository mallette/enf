class ChangePermissionStagiaireFolder < ActiveRecord::Migration[7.1]
  def change
    Classroom.all.each do |classroom|
      teachers_group_id = Group.where(classroom:, automatic: :teachers).map(&:id)
      learners_group_id = Group.where(classroom:, automatic: :learners).map(&:id)
      tutors_group_id = Group.where(classroom:, automatic: :tutors).map(&:id)

      classroom.folders.where(automatic: true, name: 'Espace individuel des stagiaires').each do |f|
        f.permissions[:r][:g] = f.permissions[:r][:g] + tutors_group_id
        f.save
      end

      classroom.folders.where(automatic: true, name: 'Notre formation').each do |f|
        f.permissions[:r] = { u: [], g: teachers_group_id + learners_group_id }
        f.save
      end
    end
  end
end
