class AddCredentialToInstances < ActiveRecord::Migration[7.0]
  def change
    add_column :instances, :url, :string
    add_column :instances, :credential_username, :string
    add_column :instances, :credential_password, :string
  end
end
