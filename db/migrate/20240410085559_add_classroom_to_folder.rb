class AddClassroomToFolder < ActiveRecord::Migration[7.1]
  def change
    add_reference :folders, :classroom, null: false, foreign_key: true
  end
end
