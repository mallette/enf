class AddCustomAuditedToActiveStorageAttachment < ActiveRecord::Migration[7.2]
  def change
    add_column :active_storage_attachments, :filename_audit, :string
    add_column :active_storage_attachments, :content_audit, :string
  end
end
