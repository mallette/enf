class CreateSkillUnit < ActiveRecord::Migration[7.0]
  def change
    create_table :skill_units do |t|
      t.string :code
      t.references :classroom, null: false, foreign_key: true

      t.timestamps
    end
  end
end
