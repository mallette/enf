class AddUserToMeeting < ActiveRecord::Migration[7.1]
  def change
    add_reference :meetings, :user, null: false, foreign_key: true, default: User.first&.id
  end
end
