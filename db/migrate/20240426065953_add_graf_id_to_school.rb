class AddGrafIdToSchool < ActiveRecord::Migration[7.1]
  def change
    add_column :schools, :graf_id, :integer
    add_index :schools, :graf_id, unique: true
  end
end
