class ChangeTerritorialAgenciesToSchools < ActiveRecord::Migration[7.0]
  def change
    rename_table :territorial_agencies, :schools
  end
end
