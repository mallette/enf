class AddDatesToClassroom < ActiveRecord::Migration[7.1]
  def change
    add_column :classrooms, :start_date, :date
    add_column :classrooms, :end_date, :date
  end
end
