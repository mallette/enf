class CreateConfigModuleParticipant < ActiveRecord::Migration[7.1]
  def change
    create_table :config_modules_participants, id: false do |t|
      t.references :participant, null: false, foreign_key: true
      t.references :config_module, null: false, foreign_key: true
    end
    modules = ConfigModule.where(active: true)
    Participant.all.each do |participant|
      participant.config_modules << modules
    end
    drop_table :classroom_user_config_modules
  end
end
