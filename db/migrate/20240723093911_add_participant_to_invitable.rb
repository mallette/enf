class AddParticipantToInvitable < ActiveRecord::Migration[7.1]
  def change
    %i[pads visios memos].each do |invitable|
      add_reference invitable, :participant, null: false, foreign_key: true
      remove_column invitable, :user_id
      remove_column invitable, :classroom_id
    end
  end
end
