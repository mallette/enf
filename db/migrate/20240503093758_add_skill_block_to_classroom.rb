class AddSkillBlockToClassroom < ActiveRecord::Migration[7.1]
  Classroom.destroy_all
  def change
    add_reference :classrooms, :skill_block, null: false, foreign_key: true
  end
end
