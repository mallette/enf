class AddSkillBlockToSkillUnit < ActiveRecord::Migration[7.1]
  SkillUnit.destroy_all
  def change
    add_reference :skill_units, :skill_block, null: false, foreign_key: true
  end
end
