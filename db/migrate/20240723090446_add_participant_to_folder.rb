class AddParticipantToFolder < ActiveRecord::Migration[7.1]
  LOGGER = SemanticLogger[self]
  def change
    add_reference :folders, :participant, null: true, foreign_key: true
    Folder.all.each do |folder|
      classroom = Classroom.find(folder.classroom_id)
      participant = Participant.find_by classroom:, user_id: folder.user_id
      folder.update(participant:)
      LOGGER.info "Change user to participant for folder <#{folder.name}>"
    end
    remove_column :folders, :user_id
  end
end
