class AddResponsibleToTeachings < ActiveRecord::Migration[7.2]
  def change
    add_column :teachings, :responsible, :boolean, default: false
  end
end
