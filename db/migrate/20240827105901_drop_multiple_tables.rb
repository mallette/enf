class DropMultipleTables < ActiveRecord::Migration[7.2]
  def change
    drop_table :domain_options
    drop_table :events
    drop_table :events_states
    drop_table :formlines
    drop_table :forms
    drop_table :instance_options
    drop_table :tickets
    drop_table :instance_states, force: :cascade
    drop_table :meta_options
    drop_table :ticket_categories
    drop_table :ticket_statuses
    drop_table :user_infos
    drop_table :user_themes
  end
end
