class AddParticipantToMeeting < ActiveRecord::Migration[7.1]
  def change
    Meeting.destroy_all
    remove_column :meetings, :user_id
    add_reference :meetings, :participant, null: false, foreign_key: true
  end
end
