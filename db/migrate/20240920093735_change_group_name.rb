class ChangeGroupName < ActiveRecord::Migration[7.2]
  def change
    Group.where(automatic: :teachers).each do |group|
      group.update name: 'Formateurs·rices'
    end
    Group.where(automatic: :tutors).each do |group|
      group.update name: 'Tuteurs·rices'
    end
  end
end
