class CreateTeacher < ActiveRecord::Migration[7.0]
  def change
    create_table :teachers do |t|
      t.references :classroom, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
