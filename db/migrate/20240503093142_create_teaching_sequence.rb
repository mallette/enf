class CreateTeachingSequence < ActiveRecord::Migration[7.1]
  def change
    create_table :teaching_sequences do |t|
      t.references :skill_unit, null: false, foreign_key: true
      t.integer :graf_id
      t.references :teaching_unit, null: false, foreign_key: true
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps
    end
  end
end
