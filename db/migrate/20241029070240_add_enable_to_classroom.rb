class AddEnableToClassroom < ActiveRecord::Migration[7.2]
  def change
    add_column :classrooms, :enabled, :boolean, null: false, default: true
  end
end
