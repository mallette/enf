class DropStatistic < ActiveRecord::Migration[7.1]
  def change
    drop_table :statistics
  end
end
