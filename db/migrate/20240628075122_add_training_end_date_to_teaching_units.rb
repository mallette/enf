class AddTrainingEndDateToTeachingUnits < ActiveRecord::Migration[7.1]
  def change
    add_column :teaching_units, :training_end_date, :date
  end
end
