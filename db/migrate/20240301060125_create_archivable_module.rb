class CreateArchivableModule < ActiveRecord::Migration[7.0]
  def change
    create_table :archive_modules do |t|
      t.references :archivable, polymorphic: true
      t.references :user, null: false, foreign_key: true
      t.timestamps
    end
  end
end
