class AddColumnsToUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :graf_id, :string
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :graf_status, :string
  end
end
