class ChangePermissionStagiaireRootFolder < ActiveRecord::Migration[7.1]
  def change
    Folder.where.not(user_id: nil).each do |f|
      tutors_ids = f.user.tutors(f.classroom).map(&:id)
      teachers_group = Group.find_by(classroom: f.classroom, automatic: :teachers)
      f.permissions[:r] = { u: [f.user.id] + tutors_ids, g: [teachers_group.id] }
      f.save
      f.children.where(name: 'Stagiaire-Formateurs-Tuteurs', automatic: true).each do |c|
        c.permissions[:r] = { u: [f.user.id] + tutors_ids, g: [teachers_group.id] }
        c.permissions[:w] = { u: [f.user.id] + tutors_ids, g: [teachers_group.id] }
        c.save
      end
    end
  end
end
