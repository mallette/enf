class AddSignedByToShuttleForm < ActiveRecord::Migration[7.1]
  def change
    add_reference :shuttle_forms, :learner_theory_signed_by, null: true, foreign_key: { to_table: 'users' }
    add_reference :shuttle_forms, :teacher_theory_signed_by, null: true, foreign_key: { to_table: 'users' }
    add_reference :shuttle_forms, :learner_training_signed_by, null: true, foreign_key: { to_table: 'users' }
    add_reference :shuttle_forms, :teacher_training_signed_by, null: true, foreign_key: { to_table: 'users' }
    add_reference :shuttle_forms, :tutor_training_signed_by, null: true, foreign_key: { to_table: 'users' }
  end
end
