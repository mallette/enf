class DeleteLearnerSignatureByFromShuttleForm < ActiveRecord::Migration[7.1]
  def change
    remove_column :shuttle_forms, :learner_theory_signed_by_id
    remove_column :shuttle_forms, :learner_training_signed_by_id
  end
end
