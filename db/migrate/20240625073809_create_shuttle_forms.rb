class CreateShuttleForms < ActiveRecord::Migration[7.1]
  def change
    create_table :shuttle_forms do |t|
      t.references :teaching_unit, null: false, foreign_key: true
      t.references :registration, null: false, foreign_key: true

      t.text :learner_theory1
      t.text :learner_theory2
      t.text :learner_theory3
      t.text :learner_theory4
      t.text :learner_theory5
      t.datetime :learner_theory_signature

      t.text :teacher_theory1
      t.datetime :teacher_theory_signature

      t.text :learner_training1
      t.text :learner_training2
      t.text :learner_training3
      t.text :learner_training4
      t.text :learner_training5
      t.text :learner_training6
      t.datetime :learner_training_signature

      t.text :tutor_training1
      t.text :tutor_training2
      t.text :tutor_training3
      t.text :tutor_training4
      t.text :tutor_training5
      t.text :tutor_training6
      t.datetime :tutor_training_signature

      t.text :teacher_training1
      t.datetime :teacher_training_signature

      t.timestamps
    end

    sf_cm = ConfigModule.create name: 'ShuttleForm', url_local: '/shuttle_forms', font: 'route', order: 70,
                                color: '#0cb75f', active: true
    instance = Instance.create(config_module: sf_cm, name: 'ShuttleForm', description: 'ShuttleForm', active: true)
    TeachingUnit.all.each do |tu|
      tu.classroom.registrations.each do |registration|
        ShuttleForm.create(registration:, teaching_unit: tu)
      end
    end
    Subtasks::Modules.perform_add_instance_to_all_classrooms(sf_cm.id, instance.id)
    Subtasks::Modules.perform_activate_module_for_all_users(sf_cm.id)
  end
end
