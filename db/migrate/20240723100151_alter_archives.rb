class AlterArchives < ActiveRecord::Migration[7.1]
  def change
    add_reference :archives, :participant, null: false, foreign_key: true
    remove_column :archives, :user_id
  end
end
