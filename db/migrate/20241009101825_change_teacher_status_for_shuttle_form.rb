class ChangeTeacherStatusForShuttleForm < ActiveRecord::Migration[7.2]
  def change
    ShuttleForm.where(learner_status: :learner_theory_signed, teacher_status: :teacher_inactive)
               .each(&:teacher_theory_update!)
  end
end
