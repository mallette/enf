class AddGrafIdToSkillUnit < ActiveRecord::Migration[7.1]
  def change
    add_column :skill_units, :graf_id, :integer
  end
end
