class DropOldArchiveTables < ActiveRecord::Migration[7.0]
  def change
    drop_table :visio_archives
    drop_table :memo_archives
    drop_table :pad_archives
  end
end
