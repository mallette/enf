class AddStatusToShuttleForm < ActiveRecord::Migration[7.1]
  def change
    add_column :shuttle_forms, :learner_status, :integer, default: 0
    add_column :shuttle_forms, :teacher_status, :integer, default: 0
    add_column :shuttle_forms, :tutor_status, :integer, default: 0
    add_index :shuttle_forms, :teacher_status
    add_index :shuttle_forms, :tutor_status
    add_index :shuttle_forms, :learner_status
  end
end
