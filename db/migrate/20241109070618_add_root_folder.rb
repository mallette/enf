class AddRootFolder < ActiveRecord::Migration[7.2]
  def change
    Classroom.all.each do |classroom|
      next if classroom.root_folder

      old_root_folders = classroom.folders.where(parent: nil, automatic: true).where.not(name: 'trash')

      logger.info "Création du dossier racine / de la promo #{classroom.code}"
      root_folder = classroom.folders.create name: '/',
                                             permissions: { r: :all },
                                             automatic: true

      old_root_folders.each do |folder|
        folder.update parent: root_folder
      end
    end
  end
end
