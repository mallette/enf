class AddCommentToFolder < ActiveRecord::Migration[7.2]
  def change
    add_column :folders, :comment, :text
  end
end
