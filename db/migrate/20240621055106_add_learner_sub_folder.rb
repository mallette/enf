class AddLearnerSubFolder < ActiveRecord::Migration[7.1]
  def change
    Classroom.all.each do |classroom|
      teachers_group_id = Group.where(classroom:, automatic: :teachers).map(&:id)
      classroom.learners.each do |learner|
        tutors_ids = learner.tutors(self).map(&:id)
        learner_folder = classroom.folders.find_by(user_id: learner.id)
        learner_folder.permissions[:r][:u] = [learner.id] + tutors_ids
        learner_folder.permissions[:w] = nil
        learner_folder.save
        next if learner_folder.children.where(automatic: true).count.positive?

        classroom.folders.create name: 'Stagiaire-Formateurs',
                                 automatic: true,
                                 parent_id: learner_folder.id,
                                 permissions: { r: { u: [learner.id], g: teachers_group_id },
                                                w: { u: [learner.id], g: teachers_group_id } }
        classroom.folders.create name: 'Stagiaire-Formateurs-Tuteurs',
                                 automatic: true,
                                 parent_id: learner_folder.id,
                                 permissions: { r: { u: [learner.id] + tutors_ids, g: teachers_group_id },
                                                w: {
                                                  u: [learner.id] + tutors_ids, g: teachers_group_id
                                                } }
      end
    end
  end
end
