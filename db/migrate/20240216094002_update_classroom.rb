class UpdateClassroom < ActiveRecord::Migration[7.0]
  def change
    change_table :classrooms do |t|
      t.rename :code_promo, :code
    end
  end
end
