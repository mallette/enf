class ChangeDataLossOnDeactivationForShuttleForm < ActiveRecord::Migration[7.1]
  def change
    cm = ConfigModule.find_by name: 'ShuttleForm'
    cm.update data_loss_on_deactivation: false
  end
end
