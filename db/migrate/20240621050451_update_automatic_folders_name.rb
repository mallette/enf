class UpdateAutomaticFoldersName < ActiveRecord::Migration[7.1]
  def change
    Folder.where(name: 'Consultation', automatic: true).each do |folder|
      folder.update name: 'Cadre général'
    end
    Folder.where(name: 'Collaboration', automatic: true).each do |folder|
      folder.update name: 'Notre formation'
    end
    Folder.where(name: 'Stagiaires', automatic: true).each do |folder|
      folder.update name: 'Espace individuel des stagiaires'
    end
  end
end
