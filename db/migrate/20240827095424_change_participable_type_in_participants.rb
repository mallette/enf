class ChangeParticipableTypeInParticipants < ActiveRecord::Migration[7.2]
  def change
    Participant.where(participable_type: 'Teacher').update(participable_type: 'Teaching')
    Participant.where(participable_type: 'Registration').update(participable_type: 'Learning')
  end
end
