class AddDeletedToFolder < ActiveRecord::Migration[7.1]
  def change
    add_column :folders, :deleted, :boolean, default: false
  end
end
