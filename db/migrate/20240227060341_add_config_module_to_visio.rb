class AddConfigModuleToVisio < ActiveRecord::Migration[7.0]
  def change
    cm = ConfigModule.where(name: 'BigBlueButton').first
    add_reference :visios, :config_module, null: false, foreign_key: true, default: cm.id
  end
end
