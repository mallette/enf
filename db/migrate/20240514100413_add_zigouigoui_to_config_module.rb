class AddZigouigouiToConfigModule < ActiveRecord::Migration[7.1]
  def change
    cm = ConfigModule.find_by(name: 'Zigouigoui')
    if cm
      cm.update name: 'Zigouigoui', url_local: '/folders', font: 'folder', order: 2, color: '#00FF48',
                credential: false, data_loss_on_deactivation: false, active: true
    else
      ConfigModule.create name: 'Zigouigoui', url_local: '/folders', font: 'folder', order: 2, color: '#00FF48',
                          credential: false, data_loss_on_deactivation: false, active: true
    end
  end
end
