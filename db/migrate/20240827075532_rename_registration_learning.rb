class RenameRegistrationLearning < ActiveRecord::Migration[7.2]
  def change
    rename_table :registrations, :learnings
    rename_table :registrations_tutorings, :learnings_tutorings
    rename_column :learnings_tutorings, :registration_id, :learning_id
  end
end
