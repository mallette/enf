class CreateClassroom < ActiveRecord::Migration[7.0]
  def change
    create_table :classrooms do |t|
      t.string :code_promo
      t.string :status
      t.date :date_start
      t.date :date_end
      t.string :place
      t.integer :quota
      t.references :territorial_agency, null: false, foreign_key: true

      t.timestamps
    end
  end
end
