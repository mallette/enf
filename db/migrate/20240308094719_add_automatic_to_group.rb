class AddAutomaticToGroup < ActiveRecord::Migration[7.0]
  def change
    add_column :groups, :automatic, :string
  end
end
