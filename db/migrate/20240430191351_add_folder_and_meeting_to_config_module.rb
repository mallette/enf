class AddFolderAndMeetingToConfigModule < ActiveRecord::Migration[7.1]
  def change
    ConfigModule.create name: 'Zigouigoui', url_local: '/folders', font: 'cloud', order: 10, color: '#e5a50a',
                        data_loss_on_deactivation: false, active: true
    cm = ConfigModule.find_by name: 'Nextcloud'
    cm.update active: false
  end
end
