class ConvertLinksToBelongsToParticipant < ActiveRecord::Migration[7.2]
  def change
    add_reference :links, :participant, null: true, foreign_key: true
    Link.all.each do |link|
      # ici, on va utiliser link[:user_id] car le belongs_to n'existe plus!
      participants = Participant.where(user_id: link[:user_id], classroom_id: link[:classroom_id])
      if participants.empty?
        logger.warn("bizarre, un vieux lien sans participant user_id=#{link[:user_id]} classroom_id=#{link[:classroom_id]}")
        next
      elsif participants.size > 1
        logger.warn("on a trouvé plus qu'un participant. on prend le premier", participants)
      end
      participant = participants.first
      link.update(participant:)
    end
    remove_reference :links, :user
    remove_reference :links, :classroom
    change_column :links, :participant_id, :integer, null: false
  end
end
