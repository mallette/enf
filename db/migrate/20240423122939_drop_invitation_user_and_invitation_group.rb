class DropInvitationUserAndInvitationGroup < ActiveRecord::Migration[7.1]
  def change
    drop_table :invitation_users
    drop_table :invitation_groups
  end
end
