class AddNextMeetingToMeeting < ActiveRecord::Migration[7.1]
  def change
    add_reference :meetings, :meeting, null: true, foreign_key: true
  end
end
