class AddTeachingUnitsFoldersPermissions < ActiveRecord::Migration[7.1]
  def change
    Folder.where.not(teaching_unit_id: nil).each do |f|
      teachers_group = Group.find_by(classroom: f.classroom, automatic: :teachers)
      learners_group = Group.find_by(classroom: f.classroom, automatic: :learners)
      f.update permissions: { r: { u: [], g: [teachers_group.id, learners_group.id] },
                              w: { u: [], g: [teachers_group.id, learners_group.id] } }
    end
  end
end
