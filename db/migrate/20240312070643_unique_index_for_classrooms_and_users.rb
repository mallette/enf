class UniqueIndexForClassroomsAndUsers < ActiveRecord::Migration[7.0]
  def change
    add_index :classrooms, :graf_id, unique: true
    add_index :users, :graf_id, unique: true
  end
end
