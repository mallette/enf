class AddIndexToSkillBlock < ActiveRecord::Migration[7.1]
  def change
    add_index :skill_blocks, :graf_id, unique: true
  end
end
