class AddAutomaticToFolder < ActiveRecord::Migration[7.1]
  def change
    add_column :folders, :automatic, :boolean, default: false
    add_reference :folders, :user, null: true, foreign_key: true
    add_reference :folders, :teaching_unit, null: true, foreign_key: true
  end
end
