class AddCredentialToConfigModules < ActiveRecord::Migration[7.0]
  def change
    add_column :config_modules, :credential, :boolean, default: false
  end
end
