class CreateTutoring < ActiveRecord::Migration[7.1]
  def change
    create_table :tutorings do |t|
      t.references :user, null: false, foreign_key: true
      t.references :registration, null: false, foreign_key: true
      t.integer :graf_id
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
