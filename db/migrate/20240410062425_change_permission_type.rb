class ChangePermissionType < ActiveRecord::Migration[7.1]
  def change
    change_column :folders, :permissions, :text
  end
end
