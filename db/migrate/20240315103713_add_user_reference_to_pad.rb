class AddUserReferenceToPad < ActiveRecord::Migration[7.0]
  def change
    add_reference :pads, :user, null: false, foreign_key: true
  end
end
