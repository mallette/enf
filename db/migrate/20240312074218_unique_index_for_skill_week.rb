class UniqueIndexForSkillWeek < ActiveRecord::Migration[7.0]
  def change
    add_index :skill_weeks, %i[week skill_unit_id], unique: true
  end
end
