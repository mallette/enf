class CreateClassroomUserConfigModule < ActiveRecord::Migration[7.0]
  def change
    create_table :classroom_user_config_modules do |t|
      t.references :user, null: false, foreign_key: true
      t.references :classroom, null: false, foreign_key: true
      t.references :config_module, null: false, foreign_key: true
    end
  end
end
