class ChangeSchoolInClassroom < ActiveRecord::Migration[7.0]
  def change
    change_table :classrooms do |t|
      t.remove :territorial_agency_id
      t.references :school, null: false, foreign_key: true
    end
  end
end
