class AddClassroomToLink < ActiveRecord::Migration[7.0]
  def change
    add_reference :links, :classroom, null: false, foreign_key: true
    remove_column :links, :domain_id, type: 'integer'
  end
end
