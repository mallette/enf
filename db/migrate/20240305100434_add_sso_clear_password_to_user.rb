class AddSsoClearPasswordToUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :sso_clear_password, :string
  end
end
