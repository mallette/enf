class CreateInvitations < ActiveRecord::Migration[7.1]
  def change
    create_table :invitations do |t|
      t.references :invitable, polymorphic: true
      t.references :recipient, polymorphic: true, null: true
      t.string :external_recipient
      t.timestamps
    end
  end
end
