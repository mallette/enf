class AlterTutoringsHabtm < ActiveRecord::Migration[7.1]
  def change
    create_table :registrations_tutorings, id: false do |t|
      t.belongs_to :registration
      t.belongs_to :tutoring
    end
    remove_column :tutorings, :registration_id
    add_reference :tutorings, :classroom, foreign_key: true
  end
end
