class RenameShuttleFormRegistrationToLearning < ActiveRecord::Migration[7.2]
  def change
    rename_column :shuttle_forms, :registration_id, :learning_id
  end
end
