class AddTrashFolder < ActiveRecord::Migration[7.1]
  def change
    Classroom.all.each do |classroom|
      classroom.folders.create name: 'trash', permissions: { r: :all }, automatic: true
    end
  end
end
