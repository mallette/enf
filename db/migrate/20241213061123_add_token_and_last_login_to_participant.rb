class AddTokenAndLastLoginToParticipant < ActiveRecord::Migration[7.2]
  def change
    add_column :participants, :last_login, :datetime
    add_column :participants, :token, :string
  end
end
