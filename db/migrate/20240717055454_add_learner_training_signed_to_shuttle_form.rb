class AddLearnerTrainingSignedToShuttleForm < ActiveRecord::Migration[7.1]
  def change
    add_column :shuttle_forms, :learner_training_signed, :boolean
    add_column :shuttle_forms, :learner_theory_signed, :boolean
  end
end
