class AddTransversalFolder < ActiveRecord::Migration[7.2]
  def change
    Classroom.all.each do |classroom|
      folder_transversal = classroom.folders.find_by(parent: nil, name: 'Transversal')
      next if folder_transversal

      classroom.folders.create name: 'Transversal',
                               permissions: { r: { u: [],
                                                   g: [classroom.teachers_group.id, classroom.learners_group.id] },
                                              w: { u: [],
                                                   g: [classroom.teachers_group.id, classroom.learners_group.id] } },
                               automatic: true
    end
  end
end
