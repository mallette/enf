class SetQuotaDefault20ToClassroom < ActiveRecord::Migration[7.0]
  def change
    change_column :classrooms, :quota, :integer, default: 20
  end
end
