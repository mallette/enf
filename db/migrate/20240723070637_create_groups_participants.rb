class CreateGroupsParticipants < ActiveRecord::Migration[7.1]
  LOGGER = SemanticLogger[self]

  def change
    create_table :groups_participants, id: false do |t|
      t.references :participant, null: false, foreign_key: true
      t.references :group, null: false, foreign_key: true
    end
    ActiveRecord::Base.connection.execute('SELECT * FROM groups_users').to_a.each do |gu|
      group = Group.find(gu['group_id'])
      participant = Participant.find_by classroom: group.classroom, user_id: gu['user_id']
      next unless participant

      group.participants << participant
      LOGGER.info "Add participant <#{participant.user.name}> to group <#{group.name}>"
    end

    drop_table :groups_users
  end
end
