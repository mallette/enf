class CreateSkillWeek < ActiveRecord::Migration[7.0]
  def change
    create_table :skill_weeks do |t|
      t.integer :week
      t.references :skill_unit, null: false, foreign_key: true

      t.timestamps
    end
  end
end
