class RenameDatetimeToAt < ActiveRecord::Migration[7.1]
  def change
    change_table :events do |t|
      t.rename :start_date, :started_at
      t.rename :end_date, :ended_at
    end

    change_table :meetings do |t|
      t.rename :start_time, :started_at
      t.rename :end_time, :ended_at
    end

    change_table :shuttle_forms do |t|
      t.rename :learner_theory_signature, :learner_theory_signed_at
      t.rename :learner_training_signature, :learner_training_signed_at
      t.rename :teacher_theory_signature, :teacher_theory_signed_at
      t.rename :teacher_training_signature, :teacher_training_signed_at
      t.rename :tutor_training_signature, :tutor_training_signed_at
    end

    change_table :surveys do |t|
      t.rename :end_date, :ended_at
    end

    change_table :teaching_sequences do |t|
      t.rename :start_date, :started_at
      t.rename :end_date, :ended_at
    end

    change_column :teaching_units, :start_date, :date
    change_column :teaching_units, :end_date, :date

    change_table :visios do |t|
      t.rename :date_start, :started_at
    end

    remove_column :classrooms, :date_start
    remove_column :classrooms, :date_end

    remove_column :registrations, :date
  end
end
