class CreateTeachingUnit < ActiveRecord::Migration[7.0]
  def change
    create_table :teaching_units do |t|
      t.string :name
      t.integer :graf_id
      t.references :classroom, null: false, foreign_key: true
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps
    end
  end
end
