class CreateParticipant < ActiveRecord::Migration[7.1]
  def change
    create_table :participants do |t|
      t.references :user, null: false, foreign_key: true
      t.references :classroom, null: false, foreign_key: true
      t.references :participable, polymorphic: true, null: false

      t.timestamps
    end

    remove_column :teachers, :user_id
    remove_column :teachers, :classroom_id
    remove_column :registrations, :user_id
    remove_column :registrations, :classroom_id
    remove_column :tutorings, :user_id
    remove_column :tutorings, :classroom_id
  end
end
