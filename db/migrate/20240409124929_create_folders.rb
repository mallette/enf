class CreateFolders < ActiveRecord::Migration[7.1]
  def change
    create_table :folders do |t|
      t.string :name
      t.string :permissions
      t.integer :parent_id

      t.timestamps
    end
  end
end
