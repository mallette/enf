class DeleteUuidToInvitationUsers < ActiveRecord::Migration[7.1]
  def change
    remove_column :invitation_users, :uuid
  end
end
