class CreateRegistration < ActiveRecord::Migration[7.1]
  def change
    drop_table :learners
    create_table :registrations do |t|
      t.references :user, null: false, foreign_key: true
      t.references :classroom, null: false, foreign_key: true
      t.integer :status
      t.integer :graf_id
      t.references :skill_block, null: false, foreign_key: true
      t.date :date

      t.timestamps
    end
  end
end
