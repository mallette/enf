class AddUpdatedAtToActiveStorageAttachments < ActiveRecord::Migration[7.1]
  def change
    add_column :active_storage_attachments, :updated_at, :datetime
    ActiveStorage::Attachment.where(updated_at: nil).each do |file|
      file.update updated_at: file.created_at
    end
  end
end
