class AddClassroomToPad < ActiveRecord::Migration[7.1]
  def change
    add_reference :pads, :classroom, null: false, foreign_key: true
    add_reference :pads, :config_module, null: false, foreign_key: true
    remove_column :pads, :owner_id
    remove_column :pads, :user_id
  end
end
