TODO
====

* [x] importation Graf
  * [x] pb de chevauchement des unités pédagogiques -> empeche le fonctionnement des fiches navettes avec fermeture auto
    * [x] voir classe AT11-B4E21AS1N

* [ ] les invitations
  * [x] améliorer le système de notification
  * [x] bouton FERMER qui finalise les invitations
  * [x] with_bookmarked(participant, filter: nil) # filter: true|false|nil
  * [x] with_invited(participant, filter: nil) # filter: true|false|nil
  * [x] with_owned(participant, filter_invited: nil, filter_owned: nil) # filter: true|false|nil

* [ ] FOLDERS
  * [ ] REGEX sur les noms de fichiers
    * [ ] Caractères invalides " * / : < > ? \ |
    * [ ] Espace en début/fin
    * [ ] Un fichier qui termine par un .
    * [ ] Doublon de casse (parce que windows n'aime pas ça). Ça il y a besoin de le faire à l'échelle d'un dossier, car il faut comparer avec les autres fichiers présents
    * [ ] https://stackoverflow.com/questions/1976007/what-characters-are-forbidden-in-windows-and-linux-directory-names
    * [ ] https://gitlab.cemea.org/-/snippets/3

Technical stuff
---------------

* [ ] système de notification global
* [x] ajout du nouveau système de notification le même que Zourit v2 (pb: raffraichissement turbo)
* [x] ajout du nouveau Logger basé sur EasyGoingRails
  * [x] prévoir possibilité de filtrer en mode DEV pour visualiser ActiveRecord, Mailer, ...
* [ ] virer scss et utilisation propre avec Tailwind (propshaft?)
* [ ] menu unifié
* [ ] empecher l'accès aux ActiveStroage sans être authentifié
  * [ ] + partage public
* [ ] corriger bug calendrier en mode responsive
  * [ ] liveserver pure tailwind
* [ ] créer des modèles intermédiaires type ParticipantCalendar, PartcipantVisio, ... pour simplifier les broadcasts
* [ ] Faire en sorte de tailwind fasse un rebuild lorsqu'on modifie un fichier css (ne fonctionne plus depuis qu'on a plus de procfile)

NEW NOTE
========
