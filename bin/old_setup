#!/bin/bash

## FUNCTIONS
## ---------

function askPassword {
    echo -n "$1: "
    read -s $2
    echo
}

function ask {
    echo -n "$1: "
    read $2
}

function postgres_newdb {
    sudo -iu postgres psql -c "CREATE USER $PROJECT WITH PASSWORD '$PROJECT'"
    sudo -iu postgres createdb $PROJECT -O $PROJECT
}

function postgres_change_hba {
    # FIXME: is it useful?
    sudo sed -i "s/host    all             all             127.0.0.1/32            peer/host    all             all             127.0.0.1/32            md5/" /etc/postgresql/15/main/pg_hba.conf
}

function setup_prod {
    [[ -d /etc/bichik ]] && echo "already done!" && echo "hint: \`sudo rm /etc/$PROJECT -rf\` and re-run to force setup" && exit 0

    sudo apt install -qy build-essential git ruby-dev libpq-dev nodejs
    sudo gem install debase -v 0.2.5.beta2
    sudo gem install bundler -v 2.4.14
    fetch_latest_version

    askPassword "SUPER-ADMIN password" "password"
    ask "SMTP host" "smtp_host"
    ask "SMTP port" "smtp_port"
    ask "SMTP sender from (email expected)" "smtp_sender_from"

    # build configuration files
    # -------------------------
    sudo mkdir -p /etc/$PROJECT

    # /etc/bichik/ldap.yml
    sudo tee /etc/$PROJECT/ldap.yml &>/dev/null <<EOF
host: 127.0.0.1
port: 389
admin: "cn=admin,dc=$PROJECT,dc=$TLD"
password: $password
base: "dc=$PROJECT,dc=$TLD"
EOF

    # /etc/bichik/database.yml
    sudo tee /etc/$PROJECT/database.yml &>/dev/null <<EOF
production: 
    adapter: postgresql
    host: 127.0.0.1
    port: 5432
    database: $PROJECT
    user: $PROJECT
    password: $PROJECT
EOF

    # /etc/bichik/smtp.yml
    sudo tee /etc/$PROJECT/smtp.yml &>/dev/null <<EOF
address: $smtp_host
port: $smtp_port
notification_sender:
    email: "$smtp_sender_from"
    name: "$PROJECT"
EOF

    # CHECK WHETHER service.conf exists
    if [[ ! -f "/etc/$PROJECT/service.conf" ]]; then
        sudo mkdir -p "/etc/$PROJECT"
        echo "generate new SECRET_KEY_BASE"
        SECRET_KEY_BASE=$( SIZE=32 ; head /dev/urandom | tr -dc A-Za-z0-9 | head -c $SIZE )
        echo "build service.conf"
        sudo tee "/etc/$PROJECT/service.conf" &>/dev/null <<EOF
RAILS_PORT=$PRODUCTION_PORT
SECRET_KEY_BASE=$SECRET_KEY_BASE
SEMANTIC_LOGGER_APP=$PROJECT
EOF
        sudo chmod 600 /etc/$PROJECT/service.conf
    fi

    # INSTALL NEW SYSTEMD SERVICE EACH TIME
    echo "installing systemd services"
    sudo tee /etc/systemd/system/$PROJECT.service &>/dev/null <<EOF
[Unit]
Description=$PROJECT
After=network.target

[Service]
Type=simple
Environment="RAILS_ENV=production"
EnvironmentFile=/etc/$PROJECT/service.conf
WorkingDirectory=$DEPLOY_FOLDER
SyslogIdentifier=$PROJECT
PermissionsStartOnly=true
ExecStart=/usr/local/bin/rails server --port \$RAILS_PORT

[Install]
WantedBy=multi-user.target
EOF

    sudo systemctl daemon-reload
    sudo systemctl enable $PROJECT.service

    sudo ln -s /etc/$PROJECT/{ldap,database,smtp}.yml /opt/$PROJECT/config/

    # setup LDAP
    cd /opt/$PROJECT
    sudo bundle install --without development test
    rails debconf:slapd["$password"]
    sudo apt install -qy slapd schema2ldif
    rails ldap:migrate

    # setup POSTGRESQL
    sudo apt install -qy postgresql
    #postgres_change_hba
    postgres_newdb

    secret=$(sudo grep SECRET_KEY_BASE /etc/$PROJECT/service.conf | cut -d '=' -f2)
    cd $DEPLOY_FOLDER
    bundle config set --local without 'development test'
    bundle install
    RAILS_ENV=production SECRET_KEY_BASE=$secret rails db:schema:load
    RAILS_ENV=production SECRET_KEY_BASE=$secret rails db:seed
    RAILS_ENV=production SECRET_KEY_BASE=$secret rails modules:populate
    RAILS_ENV=production SECRET_KEY_BASE=$secret rails assets:precompile

    systemd_restart

}

function setup_shared_dev {
    echo "TODO: to be implemented!"
    exit 2

    # CONSTANT DEFINITION
    SHARED_CONFIG_FOLDER="/var/lib/$PROJECT"

    # COPY configuration files from SHARED_CONFIG_FOLDER
    for i in database ldap smtp; do
        if [[ ! -f "/etc/$APP_NAME/$i.yml" ]]; then
            if [[ -f "$SHARED_CONFIG_FOLDER/$i.yml" ]]; then
                sudo cp "$SHARED_CONFIG_FOLDER/$i.yml" "/etc/$APP_NAME/$i.yml"
                sudo chmod 600 "/etc/$APP_NAME/$i.yml"
            else
            >&2 echo "$SHARED_CONFIG_FOLDER/$i.yml undefined, please provide your own config file and save /etc/$APP_NAME/$i.yml, aborted!"
            exit 20
            fi 
        fi
        sudo ln -sf "/etc/$APP_NAME/$i.yml" "$DEPLOY_FOLDER/config/$i.yml"
    done
}

## MAIN
## ----


set -Eeuo pipefail
eval "$(curl -s https://gitlab.cemea.org/mallette/enf/-/raw/main/bin/shared)"
check_sudo_user

setup_type="${1:-}"
case "$setup_type" in
prod)
    setup_prod ;;
shared-dev)
    setup_shared_dev ;;
*)
    echo "USAGE: $0 prod|shared_dev"
    exit 1
esac
