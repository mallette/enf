
function assert_hostname_ends_with {
  type=$1
  if !( hostname | grep -q "\-$type\$" ); then
    echo "current machine is: $(hostname), expected: bichik-$type" >&2 && exit 2
  fi
}

function assert_hostname_is_prod {
  if !( hostname | grep "\-(|dev|beta|preprod)\$" ); then
    echo "current machine is: $(hostname), expected: bichik" >&2 && exit 2
  fi
}

function assert_branch { 
  branch=$1
  current_branch=$(git rev-parse --abbrev-ref HEAD)
  if [[ "$current_branch" != "$branch" ]]; then
    echo "current branch is: $current_branch, expected: $b̂ranch" >&2 && exit 1
  fi
}

function goto_opt_bichik {
  if [[ ! -d /opt/bichik ]]; then
    echo '/opt/bichik is missing!' >&2 && exit 3
  else
    cd /opt/bichik
  fi
}

function assert_git_clean {
  if [[ -n "$(git status --porcelain)" ]]; then
    echo "dirty working directory!" >&2
    echo "maybe: " >&2
    echo "git reset --hard HEAD" >&2
    echo "git clean -fd #then, delete --force --directory" >&2
    exit 4
  fi
}

function deploy_branch {
  branch=$1
  [[ -z "$branch" ]] && echo 'empty branch' >&2 && exit 4

  goto_opt_bichik
  assert_branch "$branch"
  assert_git_clean
  git pull
  bundle install
  rails db:migrate
  rails assets:precompile
  systemctl restart bichik
}
