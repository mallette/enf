function sed_replace {
  REGEX=$1
  STRING=$2
  FILE=$3
  if ! grep -Eq "$REGEX" "$FILE"; then
    builtin echo -e "$STRING" >>"$FILE"
    echo 'appended'
  else
    sed -Ei "s|$REGEX|$STRING|g" "$FILE"
    echo 'replaced'
  fi
}

function bashrc_count_lines {
  wc -l "$HOME/.bashrc" | cut -d' ' -f1
}

function bashrc_watch_start {
  if [[ -z "${BASHRC_LINES+set}" ]]; then
    BASHRC_LINES=$(bashrc_count_lines)
  else
    BASHRC_EVOLVED=true
  fi
}

function bashrc_watch_end {
  bashrc_lines=$(bashrc_count_lines)
  if [[ "$BASHRC_LINES" -lt "$bashrc_lines" || -n "${BASHRC_EVOLVED+set}" ]]; then
    echo
    echo '*****************************'
    echo '* BASHRC has evolved!       *'
    echo '* please synchronize:       *'
    echo '*                           *'
    echo "*   source ~/.bashrc        *"
    echo '*                           *'
    echo '*****************************'
    echo
  fi
}

function _bashrc_env_with_prefix {
  arg1="$1"
  arg2="$2"
  prefix="$3"
  if printenv | grep -q "$arg1"; then
    real_value=$(eval "echo \$$arg1")
    echo "success for $arg1 = $real_value"
  else
    case "$prefix" in
    export)
      session_line="export ${arg1}=${arg2}"
      regex="$session_line"
      ;;
    eval)
      session_line="eval \"\$($arg2)\""
      regex="eval \"\\\$\\($arg2\\)\""
      ;;
    *) echo "unknown prefix $prefix" && exit 10 ;;
    esac
    sed_replace "$regex" "$session_line" "$HOME/.bashrc" &>/dev/null
    eval "$session_line"
    set +e
    trap - EXIT
    eval "BASHRC_LINES=$BASHRC_LINES $0"
    exit $?
  fi

}

function bashrc_export {
  _bashrc_env_with_prefix "$1" "$2" 'export'
}

function bashrc_eval {
  _bashrc_env_with_prefix "$1" "$2" 'eval'
}

function on_exit() {
  rv=$?
  # [[ $rv -eq 0 ]] && bashrc_watch_end
  bashrc_watch_end
  exit $rv
}

bashrc_watch_start
trap "on_exit" EXIT
