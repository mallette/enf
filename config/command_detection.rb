# useful methods for figuring out which kind of process is running
module CommandDetection
  def server? = Rails.const_defined?('Server')
  def console? = !server? && defined?(Rails::Console) == 'constant'
  def rake? = !server? && !console? && Rails.const_defined?('Rake')
  def tailwind_watcher? = rake_first_task?('tailwindcss:watch')

  def procfile_server?
    Rails.env.development? &&
      Rails.application.server? &&
      File.exist?(File.join(Rails.root, 'Procfile.dev'))
  end

  private

  def rake_first_task?(name) = Rake.const_defined?(:Application) && Rake.application.top_level_tasks&.first == name
end
