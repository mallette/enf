# tell the I18n library to load additional locales from app/lib/core
I18n.load_path += Dir[Rails.root.join('app', 'lib', 'core', '*', 'locales', '*.{rb,yml}')]
I18n.available_locales = %i[fr en es it]
I18n.default_locale = :fr

# Langue française par défaut
Rails.configuration.i18n.default_locale = :fr
Rails.configuration.i18n.fallbacks = [:fr]
