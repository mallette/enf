return if Rails.application.tailwind_watcher?

def build_instrumentation_config
  {
    action_controller: true,
    action_view: Live::Constants::ACTION_VIEW,
    active_record: Live::Constants::ACTIVE_RECORD
  }.map do |key, value|
    key if value
  end.compact
end

def reset_dev_logger
  time_format = '%H:%M:%S'
  formatter = Rails.configuration.x.semantic.formatter.constantize.new(time_format:)

  SemanticLogger.clear_appenders!
  SemanticLogger.add_appender(io: $stdout, formatter:)
  Semantic::Instrumentalizer.activate(*build_instrumentation_config)
  SemanticLogger.sync!
end

Rails.application.configure do
  config.x.action_controller.main_session_tag = 'graf_id'
  config.x.action_controller.main_session_tag_fixed_length = 8
  SemanticLogger.default_level = Rails.env.test? ? :warn : :debug
  RailsSemanticLogger::Rack::Logger.logger.level = :info # useful for remaining log like "[Rack::Log] Started..."
  ActionCable.server.config.logger.level = :warn
  Rails.logger.name = 'rails'
end

# FIXME: Logger should be available sooner!
Rails.configuration.after_initialize do
  if Rails.env.production?

    appender = :syslog
    formatter = Semantic::BasicFormatter.new
    SemanticLogger.add_appender(appender:, formatter:)
    Semantic::Instrumentalizer.activate(*build_instrumentation_config)

    if Rails.application.rake? || Rails.application.console?
      io_formatter = Semantic::BasicFormatter.new(time_format: '%H:%M:%S')
      SemanticLogger.add_appender(io: $stdout, formatter: io_formatter)
    end

  elsif Rails.application.server?
    reset_dev_logger

    ActiveSupport::Notifications.subscribe('rolling.live_constant') do |event|
      constants = event.payload[:changes].map { |change| change[:constant] }
      if constants.intersection(%w[ACTIVE_RECORD ACTION_VIEW]).any?
        Semantic::Instrumentalizer.activate(*build_instrumentation_config)
      end
    end

    Rails.autoloaders.main.on_load('ApplicationController') do
      reset_dev_logger
      Rails.logger.info ' ', dimensions: Semantic::FancyDimensions.new(rails: '╔═╗', before: 1)
      Rails.logger.fatal '🌟 Zeitwerk RELOAD!', dimensions: Semantic::FancyDimensions.new(rails: '╠█╣')
      Rails.logger.info ' ', dimensions: Semantic::FancyDimensions.new(rails: '╚═╝')
    end

  else
    SemanticLogger.add_appender(io: $stdout, formatter: Semantic::BasicFormatter.new(time_format: '%H:%M:%S'))
    Semantic::Instrumentalizer.activate(*build_instrumentation_config)
  end
end
