Audited.current_user_method = :current_participant

Rails.configuration.after_initialize do
  module Audited # rubocop:disable Lint/ConstantDefinitionInBlock
    class Audit
      alias participant user
      class << self
        alias as_participant as_user
      end
    end
  end
end
