return unless defined?(RailsLiveReload)

RailsLiveReload.configure do |config|
  # Tailwind CSS
  # First rule prevents reloading from asset files ending with '.tailwind.css'
  config.watch %r{(app|vendor)/(assets|javascript)/.+\.(css|js|html|png|jpg)(?<!tailwind\.css)$}, reload: :always
  config.watch %r{app/assets/builds/tailwind.css}, reload: :always

  # Default watched folders & files
  config.watch %r{app/views/.+\.(erb|haml|slim)$}
end
