def assets_path(path)= Rails.root.join('app', 'assets', path)
def config_path(path)= Rails.root.join('config', path)
def load_symbolized_yaml(path) = YAML.load_file(config_path(path)).deep_symbolize_keys
def git_local_branch = `git rev-parse --abbrev-ref HEAD`.chop
def git_current_version = `git describe --tags 2>/dev/null | cut -d'-' -f1`.chop
def git_dirty = `git status -s | wc -l`.chop.to_i
def git_ahead = `git log "#{git_current_version}"..HEAD --oneline | wc -l`.chop.to_i
def main_branch? = git_local_branch == 'main'

Rails.application.configure do
  config.x.options.tap do |options|
    options.max_threads = 20
    options.password_pattern = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/
    options.datetime_format = '%Y-%m-%dT%H:%M'

    options.repository_url = 'https://gitlab.cemea.org/mallette/enf'
    options.repository_branch = git_local_branch
    options.repository_version = git_current_version
    unless main_branch?
      options.repository_version += "⤽#{git_ahead}" if git_ahead.positive?
      options.repository_version += "…#{git_dirty}" if git_dirty.positive?
    end
    graf = load_symbolized_yaml 'graf.yml'
    options.graf_url = graf.fetch :url
    options.graf_username = graf.fetch :username
    options.graf_password = graf.fetch :password

    notification_sender = load_symbolized_yaml('smtp.yml').fetch(:notification_sender)
    options.from_email = notification_sender.fetch :email
    options.from_name = notification_sender.fetch :name
    options.attachment_logo = File.read(assets_path('images/commun/logo_bichik_email.png'))
  end

  # special trick to include `x_option` method into Rails.configuration
  # call sample: Rails.configuration.x_option(:graf,:url)
  def config.x_option(*keys)
    original_keys = keys.dup
    current = Rails.configuration.x.options
    current = current.fetch(keys.shift) while keys.any? && current.is_a?(Hash)
    raise "Rails.configuration.x.options.<#{original_keys.join('.')}> nil!" unless current
    raise "Rails.configuration.x.options.<#{original_keys.join('.')}> from #{keys} unrecognized!" unless keys.empty?

    current
  rescue StandardError
    raise "Rails.configuration.x.options.<#{original_keys.join('.')}> not found!"
  end
end
