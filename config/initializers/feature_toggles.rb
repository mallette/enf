return if Rails.application.tailwind_watcher?

Rails.configuration.after_initialize do
  Live::Constants
    .constants
    .map { |c| c.match(/^FEAT_.*/) if Live::Constants.const_get(c) }
    .compact
    .each { |flag| puts "🚧 feature toggle enabled... #{flag}" }
end
