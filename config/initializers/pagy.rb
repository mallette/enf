require 'pagy/extras/overflow'
require 'pagy/extras/array'

Pagy::DEFAULT[:overflow] = :last_page
Pagy::DEFAULT[:limit] = 10 # items per page

Pagy::I18n.load({ locale: 'fr', filepath: 'config/locales/pagy.fr.yml' },
                { locale: 'en' },
                { locale: 'es' },
                { locale: 'it' })
