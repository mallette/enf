return unless Rails.application.server?

Rails.application.configure do
  config.active_job.logger = nil
  config.after_initialize do
    DailyJob.new.reschedule(force: true, verbose: false)
  end
end
