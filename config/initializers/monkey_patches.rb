return unless Rails.application.server? || Rails.application.console? || Rails.env.test?

def grant(patch)
  case patch
  when /^action_dispatch/ then Rails.application.server?
  when /^action_view/ then Rails.application.server? || Rails.env.test?
  when /^active_record/ then Rails.application.console?
  when /^rails_live_reload/ then Rails.application.server? && Rails.env.development?
  when /^semantic/ then !Rails.env.test?
  end
end

Rails.configuration.before_initialize do
  patches = Dir.glob(Rails.root.join('lib', 'monkey_patches', '**', '*.rb'))
  patches.each do |file|
    patch = Pathname.new(file).relative_path_from(Rails.root.join('lib', 'monkey_patches')).to_s
    next unless grant patch

    puts "🐵 patching... #{patch}"
    require file
  end
end
