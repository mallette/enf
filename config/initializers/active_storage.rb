Rails.configuration.to_prepare do
  ActiveStorage::Attachment.include ActiveStorageAttachmentExtension
end

Rails.application.configure do
  config.active_record.default_column_serializer = YAML
end
