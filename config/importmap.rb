# Pin npm packages by running ./bin/importmap

pin 'application', preload: true
pin '@hotwired/turbo-rails', to: 'turbo.min.js', preload: true
pin '@hotwired/stimulus', to: 'stimulus.min.js'
pin '@hotwired/stimulus-loading', to: 'stimulus-loading.js'
pin_all_from 'app/javascript/controllers', under: 'controllers'

pin 'alpine-turbo-drive-adapter', to: 'https://ga.jspm.io/npm:alpine-turbo-drive-adapter@2.0.0/dist/alpine-turbo-drive-adapter.esm.js'
pin 'alpinejs', to: 'https://ga.jspm.io/npm:alpinejs@3.13.0/dist/module.esm.js'
pin 'chart.js', to: 'https://ga.jspm.io/npm:chart.js@4.4.0/dist/chart.js'
pin '@kurkle/color', to: 'https://ga.jspm.io/npm:@kurkle/color@0.3.2/dist/color.esm.js'
pin '@ryangjchandler/alpine-clipboard', to: 'https://ga.jspm.io/npm:@ryangjchandler/alpine-clipboard@2.3.0/src/index.js'
pin 'toastify-js' # @1.12.0
pin '@stimulus-components/password-visibility', to: '@stimulus-components--password-visibility.js' # @3.0.0
pin '@stimulus-components/clipboard', to: '@stimulus-components--clipboard.js' # @5.0.0
pin '@stimulus-components/notification', to: '@stimulus-components--notification.js' # @3.0.0
pin '@stimulus-components/checkbox-select-all', to: '@stimulus-components--checkbox-select-all.js' # @6.1.0
pin 'stimulus-use' # @0.52.3
