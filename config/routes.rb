Rails.application.routes.draw do
  root 'participants#index'

  resources :shuttle_forms, only: %i[index edit update]
  post 'shuttle_forms/menu/:id', to: 'shuttle_forms#turbo_menu', as: :shuttle_form_menu

  mount ActionCable.server => '/cable'
  resources :folders
  resources :meetings
  devise_for :users, controllers: { sessions: 'users/sessions', passwords: 'users/passwords' }
  resources :links, except: %i[show]

  resources :pads
  post 'pads/:id/toggle_archive', to: 'pads#toggle_archive', as: 'pad_toggle_archive'
  post 'pads/:id/toggle_bookmark', to: 'pads#toggle_bookmark', as: 'pad_toggle_bookmark'

  resources :visios
  post 'visios/:id/toggle_archive', to: 'visios#toggle_archive', as: 'visio_toggle_archive'
  post 'visios/:id/toggle_bookmark', to: 'visios#toggle_bookmark', as: 'visio_toggle_bookmark'
  get 'visios/:id/visio_url', to: 'visios#url', as: 'visio_url'
  get '/visios/:id/button_to_bbb', to: 'visios#button_to_bbb', as: 'button_to_bbb'
  get '/visios/:id/recordings', to: 'visio_recordings#index', as: 'visio_recordings'
  delete 'visios/:id/recordings/:record_id' => 'visio_recordings#destroy', as: 'visio_recordings_destroy'
  get '/visios/:id/recordings_list', to: 'visio_recordings#visio_recordings', as: 'visio_recordings_list'
  get 'visios/:id/recordings/after_destroy', to: 'visio_recordings#after_destroy', as: 'visio_recordings_after_destroy'
  get 'visios/:id/recordings/:record_id/download', to: 'visio_recordings#download', as: 'visio_recording_download'

  resources :memos
  post 'memos/:id/toggle_archive', to: 'memos#toggle_archive', as: 'memo_toggle_archive'
  post 'memos/:id/toggle_bookmark', to: 'memos#toggle_bookmark', as: 'memo_toggle_bookmark'

  resources :users, only: %i[edit update]

  resources :config_modules do
    resources :instances
  end

  resources :services, only: %i[index show]

  # Choix de la promo
  get 'participants', to: 'participants#index', as: 'participants'
  post 'participants/:id', to: 'participants#choose', as: 'participant'

  resources :classrooms
  get 'classrooms/:id/groups', to: 'groups#index', as: 'groups'
  post 'classrooms/:id/groups', to: 'groups#create'
  get 'classrooms/:id/groups/:group_id', to: 'groups#show', as: 'group'
  delete 'classrooms/:id/groups/:group_id', to: 'groups#destroy'
  patch 'classrooms/:id/groups/:group_id', to: 'groups#update', as: 'group_update'
  post 'classrooms/:id/participant/:participant_id/enable_module/:module_id', to: 'classrooms#enable_module_for_participant',
                                                                              as: 'enable_module_for_participant'
  post 'classrooms/:id/participant/:participant_id/disable_module/:module_id', to: 'classrooms#disable_module_for_participant',
                                                                               as: 'disable_module_for_participant'

  post 'login' => 'users#login'
  get 'logout' => 'users#logout', as: :logout

  get 'about' => 'about#index', as: 'about'

  get 'config_modules/:domain_id/infos' => 'config_modules#infos'

  get 'invitations/from/:invitable_type/:invitable_id', to: 'invitations#index', as: :invitations
  get 'invitations/from/:invitable_type/:invitable_id/groups', to: 'invitations#invite_per_groups',
                                                               as: :invite_per_groups
  post 'invitations/from/:invitable_type/:invitable_id/group/:id', to: 'invitations#invite_group_add',
                                                                   as: :invite_group_add
  delete 'invitations/from/:invitable_type/:invitable_id/group/:id', to: 'invitations#invite_group_del',
                                                                     as: :invite_group_del
  post 'invitations/from/:invitable_type/:invitable_id/participant/:id', to: 'invitations#invite_participant_add',
                                                                         as: :invite_participant_add
  delete 'invitations/from/:invitable_type/:invitable_id/participant/:id', to: 'invitations#invite_participant_del',
                                                                           as: :invite_participant_del
  get 'invitations/from/:invitable_type/:invitable_id/participants', to: 'invitations#invite_per_participants',
                                                                     as: :invite_per_participants
  post 'invitations/from/:invitable_type/:invitable_id/emails', to: 'invitations#invite_per_emails',
                                                                as: :invite_per_emails
  patch 'invitations/from/:invitable_type/:invitable_id/emails', to: 'invitations#invite_per_emails_submit',
                                                                 as: :invite_per_emails_submit
  post 'invitations/from/:invitable_type/:invitable_id/groups/:id', to: 'invitations#invite_add_group',
                                                                    as: :invite_one_group
  patch 'invitations/:id', to: 'invitations#resend', as: :resend_invitation
  delete 'invitations/:id', to: 'invitations#destroy', as: :destroy_invitation

  get 'folders/:id/files/:file_id', to: 'folders#file', as: 'file'
  patch 'folders/:id/files/:file_id', to: 'folders#rename_file', as: 'rename_file'
  delete 'folders/:id/files/:file_id', to: 'folders#delete_file', as: 'delete_file'
  patch 'folders/:id/files/:file_id/move_or_copy', to: 'folders#move_or_copy_file', as: 'move_or_copy_file'
  get 'folders/:id/bookmark', to: 'folders#bookmark', as: 'bookmark_folder'
  get 'folders/:id/file/:file_id/bookmark', to: 'folders#bookmark_file', as: 'bookmark_file'
  delete 'folders/:id/elements', to: 'folders#delete_elements', as: 'folder_delete_elements'
  post 'folders/:id/files', to: 'folders#create_blank_file'
  patch 'folders/:id/move_or_copy', to: 'folders#move_or_copy_folder', as: 'move_or_copy_folder'
  get 'folders/:id/details', to: 'folders#details', as: 'details_folder'
  get 'folders/:id/files/:file_id/details', to: 'folders#file_details', as: 'details_file'
  get 'folders_search', to: 'folders#search'
  get 'folders_tree', to: 'folders#tree', as: :tree_folder
  get 'folders/:id/zip', to: 'folders#zip', as: :folder_zip

  get 'wopi/files/:id/contents', to: 'wopi#filecontent', as: 'wopi_filecontent'
  post 'wopi/files/:id/contents', to: 'wopi#update_filecontent'
  get 'wopi/files/:id', to: 'wopi#file', as: 'wopi_file'

  get 'end_visio', to: 'external_pages#end_visio', as: 'end_visio'
end
