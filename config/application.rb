require_relative 'boot'
require_relative 'command_detection'
require 'rails/all'

Bundler.require(*Rails.groups)

module Bichik
  class Application < Rails::Application
    include CommandDetection

    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.2

    # ignoring monkey_patches because MonkeyPatcher would carry out
    config.autoload_lib(ignore: %w[assets tasks monkey_patches])
  end
end
