import { fontFamily as _fontFamily } from 'tailwindcss/defaultTheme'

export const darkMode = 'class'
export const content = [
  './public/*.html',
  './app/helpers/**/*.rb',
  './app/javascript/**/*.js',
  './app/views/**/*.{erb,haml,html,slim}'
]
export const safelist = [
  'sm:col-start-1',
  'sm:col-start-2',
  'sm:col-start-3',
  'sm:col-start-4',
  'sm:col-start-5',
  'sm:col-start-6',
  'sm:col-start-7',
]
export const theme = {
  extend: {
    fontFamily: {
      sans: ['Inter var', ..._fontFamily.sans],
    },
    colors: {
      zourit: {
        1: '#00a19a', // blue_bichik
        2: '#535786', // special violet
        3: '#9ca3af', // special gray for background (old: #374151)
        4: '#03807a', // darker blue_bichik
        5: '#43476f', // darker special violet
        6: '#80cccb', // charte blue
        7: '#007570', // blue_bichik_darker
        8: '#00b5ad', // blue_bichik_lighter
      },
      bichik: {
        80: '#88bb91',
        90: '#00b5ad',
        100: '#00a19a',
        110: '#007570',
        180: '#8a70ab',
        190: '#6b70ab',
        200: '#535786',
        210: '#494c71',
      }
    },
    backgroundImage: {
      'fond': "url('commun/cheap_diagonal_fabric.png');",
      'fond-dark': "url('commun/black-woven-fabric-pattern.jpg');"
    },
    keyframes: {
      "fade-in": {
        '0%': { opacity: '0%' },
        '100%': { opacity: '100%' },
      }
    },
    animation: {
      "fade-in": 'fade-in 0.2s ease-in-out',
    }
  },
}
export const variants = {
  extend: {
    visibility: ["group-hover"],
  },
}
export const plugins = [
  require('@tailwindcss/forms'),
  require('@tailwindcss/aspect-ratio'),
  require('@tailwindcss/typography'),
  require('@tailwindcss/container-queries'),
]
