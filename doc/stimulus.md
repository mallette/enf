Stimulus
========

pour utiliser les composants de https://www.stimulus-components.com
et plutôt que d'utiliser ~~`yarn`~~, préférez `importmap`


> par exemple si le composant s'appelle '@stimulus-components/checkbox-select-all'

```bash
./bin/importmap pin @stimulus-components/checkbox-select-all
```

> @jnoel me signale que avec importmap, cela pose problème notamment cela ne charge pas 
>
> > ni les librairies javascript dépendantes
> 
> > ni les librairies css


register
--------

### either manually

```javascript
import PasswordVisibility from '@stimulus-components/password-visibility'
application.register('password-visibility', PasswordVisibility)
```

### or automatically via a custom stimulus controller

in app/javascript/controllers/**checkbox_select_all**_controller.js
```javascript
import CheckboxSelectAll from "@stimulus-components/checkbox-select-all"

export default class extends CheckboxSelectAll {
  ...
}
```