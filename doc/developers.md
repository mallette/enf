DEV
===


* page dédiée aux [tests](./test.md)
* [stimulus](stimulus.md)

LANCER DES TACHES MANUELLEMENT
------------------------------

* rails -T # pour avoir une description courte
* rails -D # pour avoir la description complète (utile notamment pour graf:import)

* rails graf:import # importation de toutes les classes, sans notification par mail
* rails "graf:import[18587]" # importation de la seule classe 18587 (rapide)
* rails "graf:import[18587, true]" # importation de la seule classe 18587 mais avec notification
* rails "daily:job" # execute toutes les sous-tâches (scheduler + graf import) de manière synchrone, sans notification par email
* rails "daily:job[true]" # execute toutes les sous-tâches (scheduler + graf import) de manière synchrone, et avec notification

SSO_CLEAR_PASSWORD
------------------

#FIXME: to remove!
`EMAIL=paon_adeline@yahoo.fr ; rails r "puts User.find_by(email:'$EMAIL').sso_clear_password"`

Normalisation des champs date
-----------------------------

* <field>_at : pour un champ Datetime
* <field>_date : pour un champ Date

Rq: ne pas utiliser l'inverse en préfixant par date_ ou at_ !

INIT from empty database
------------------------

* # SCHEMA and SEEDS
  * rails db:schema:load
  * rails db:seed
  * rails db:migrate
  * rails graf:import
* # MODULES
  * rails modules:add_instance_to_all_classrooms (run once per module)
  * rails modules:activate_module_for_all_users (run once per module)


Environnement de développement
------------------------------

* GNU/Linux (au choix)
  * [Debian](#debian-first-install) 
  * [Arch](#arch-first-install)
* IDE (au choix)
  * VsCodium 
  * Neovim
* Ruby
  * Rails 7
* Postgres SQL

REUSE SSH CONTAINER
-------------------

* # First: ASK FOR a ssh GRANT ACCESS!!!
* cp .env.sample .env # then, change RAILS_PORT (default: 3000) and RDEBUG_PORT (default: 30000) in case you run multiple instances

TAILWIND TRICKS
---------------

* bg-zourit-1 bg-zourit-5 # the bigger always wins! -> bg-zourit-5
* bg-zourit-5 bg-zourit-1 # the bigger always wins! -> bg-zourit-5


