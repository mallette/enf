Zigouigoui
==========

```mermaid
classDiagram
    Folder *-- File : ActiveStorageAttachmentExtension
    File --> ActiveStorageBlob
    File --> ActiveStorageAttachment
    File --> ActiveStorageVariantRecord
```