Participant
===========

> FIXME: Attention OBSOLÈTE
> 
> > Teacher => Teaching
> 
> > Registration => Learning

```mermaid
classDiagram

    class Classroom{
        +String code
        +Enum status
        +Date date_end
        +Date date_start
        +Géo place
        +Int quota
    }

    class User{
        +Int graf_id
        +String last_name
        +String first_name
        +String email
        +Encrypted password
        +Enum status
    }
    
    class SkillUnit{
        +String code
    }

    class TeachingUnit{
        +String name
        +DateTime start_date
        +DateTime end_date
    }
    
    class School {
        +String name
        +Géo place
    }

    Teacher --|> Participant
    Registration --|> Participant
    Tutoring --|> Participant
    Participant --> User
    Participant --> Classroom
    Tutoring o--o Registration

    Classroom --> School
    Group o--o Participant
    Group --> Classroom : est attaché à une seule

    Classroom o-- SkillUnit : Ruban pédagogique

    Registration o-- SkillUnit
    TeachingUnit --> Classroom
```
