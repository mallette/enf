WorkFlow style Flowchart
-----

```mermaid
flowchart TD
    A1:::action
    A2:::action
    A3:::action
    A7:::action
    A9:::action
    S1:::scheduler
    S2:::scheduler
    S8:::scheduler
    S4:::scheduler
    S1:::scheduler
    S9:::scheduler
    E1:::etat
    E2:::etat
    E3:::etat
    E4:::etat
    E5:::etat
    E6:::etat
    E7:::etat
    E8:::etat
    E9:::etat
    E10:::etat
    classDef action stroke:#f00
    classDef scheduler stroke:#0f0
    classDef etat stroke:#00f
    B{tu.start_date?} -->|Oui| E1[LearnerTheoryUpdate]
    B -->|Non| S1(Schedule1)
    S1 --> B
    E1 --> A1(Modification)
    A1 --> E{signé?}
    E -->|non| A1
    E -->|oui| E2[LearnerTheorySigned]
    E -->|oui| E3
    E2 --> D3
    S2(Schedule2) --> D3{tu.end_date+2d ?}
    D3 -->|oui| E5[LearnerTrainingUpdate]
    D3 -->|oui| E3[TeacherTheoryUpdate]
    D3 -->|oui| E7[TutorUpdate]
    D3 -->|non| S2
    E5 --> A2(modification)
    A2 --> D4{Signé?}
    D4 -->|oui| E6[LearnerTrainingSigned]
    D4 -->|non| A2

    E3 --> A3(modification)
    A3 --> D5{Signé?}
    D5 -->|oui| E4[TeacherTheorySigned]
    D5 -->|non| A3

    E7 --> A7(modification)
    A7 --> D7{Signé?}
    D7 -->|oui| E8[TutorSigned]
    D7 -->|non| A7
    S8(Schedule4) --> D8{next_tu.start_date?}
    D8 -->|oui| E8
    D8 -->|oui| E6
    D8 -->|non| S8

    S4(Schedule3) --> D9{tu.end_date+7d ?}
    D9 -->|oui| E4
    D9 -->|non| S4

    E4 --> D12
    E6 --> D12
    E8 --> D12
    D12{status_semaphor?} -->|oui| E9[TeacherTrainingUpdate]
    E9 --> A9(modification)
    A9 --> D11{signé?}
    D11 -->|oui| E10[TeacherTrainingSigned]
    D11 -->|non| A9

    S9(Schedule5) --> D10{next_tu.start_date+1d?}
    D10 -->|oui| E10
    D10 -->|non| S9
 ```
 