Modules Métier
==============

```mermaid
classDiagram

    class Classroom{
        +String code
        +Enum status
        +Date date_end
        +Date date_start
        +Géo place
        +Int quota
    }

    class User{
        +Int graf_id
        +String last_name
        +String first_name
        +String email
        +Encrypted password
        +Enum status
    }
    
    class SkillUnit{
        +String code
    }

    class SkillWeek{
        +Int week
    }

    class TeachingUnit{
        +String name
        +DateTime start_date
        +DateTime end_date
    }
    
    class School {
        +String name
        +Géo place
    }

    Teacher --> User
    Registration --> User
    Tutoring --> User
    Tutoring --> Registration
    Tutoring --> Classroom

    Classroom --> School
    Group o--o User
    Group --> Classroom : est attaché à une seule

    Classroom o-- SkillUnit : Ruban pédagogique
    Classroom --> Teacher : responsable
    Classroom o-- Teacher

    Teacher o-- Classroom
    Registration o-- SkillUnit
    SkillWeek --> SkillUnit
    TeachingUnit --> Classroom
```


Diagramme vers les services 
----------------------------

```mermaid
classDiagram

    Invitation --> User
    Invitation --> Group
    Group o--o User
    Group --> Classroom
    Visio --> Classroom
    Visio --> User
    Memo --> Classroom
    Memo --> User
    Pad --> Classroom
    Pad --> User
    Survey --> Classroom
    Survey --> User

    Visio --|> Module
    Pad --|> Module
    Survey --|> Module
    Memo --|> Module
    
```


Diagramme spécifique pour Visio (mais s'applique aux autres modules, memo, pads, survey...)
----------------------------

```mermaid
classDiagram
    Invitation .. User
    Invitation .. Group
    Invitation --> Visio
    note for Invitation "polymorphic (User or Group)"
    Group o--o User
    Group --> Classroom
    Visio --> Classroom
    Visio --> User
```

```mermaid
classDiagram
    class TrainingCourse{
        +Int study_director(BasePartner)
    }

    class TrainingIndividualRegistration{
        +Int attendee(BasePartner)
        +Int SkillsBlock
    }
    
    TrainingCourse --o BaseCompany
    TrainingCourse --> BasePartner : Responsable
    TrainingIndividualRegistration --> BasePartner
    TrainingIndividualRegistration --o TrainingCourse
    TrainingIndividualRegistration --> TrainingSkillsBlock
    TrainingCredit --o TrainingSkillsBlock
    TrainingCredit o-- Lecture
    TrainingTeachingUnit o-- Lecture
    TrainingCourse o-- Lecture
    TrainingCourse o-- TrainingTeachingUnit
    TrainingCourse --> TrainingSkillsBlock

```




Diagramme des modules
---------------------

```mermaid
classDiagram
    
    domain_info --> classroom
    domain_info --> config_module
    domain_info --> instance

```

Diagramme entre les promos (*classroom*) et les services (*ConfigModule*)
---------------------------------------------------------

```mermaid
classDiagram
    
    classroom_user_config_module --> classroom
    classroom_user_config_module --> user
    classroom_user_config_module --> config_module
```







