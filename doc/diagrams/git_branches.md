## Git Branches & Servers

```mermaid
gitGraph
        commit
        branch dev
        checkout main
        commit tag:"v0.0.1"
        checkout dev
        commit 
        commit
        commit
        checkout main
        merge dev id:"MERGE 1"
        commit tag:"v0.0.2"
        checkout dev
        commit
        checkout main
        commit id:"bichik-prod" tag:"v0.0.3" type: HIGHLIGHT
        checkout dev
        commit
        checkout main
        merge dev id:"MERGE 2"
        checkout main
        commit id:"bichik-preprod" type: HIGHLIGHT
        checkout dev
        commit id:"bichik-beta" type: HIGHLIGHT
```
