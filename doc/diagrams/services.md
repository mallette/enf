Services
========

```mermaid
classDiagram
    Invitation --> Participant
    Invitation --> Group
    Group --> Participant
    Visio --> Participant
    Memo --> Participant
    Pad --> Participant
    Survey --> Participant
```
