
# ShuttleForm Workflow

```mermaid
stateDiagram-v2
    classDef Learner fill:green, color:black
    classDef Teacher fill:red, color:black
    classDef Tutor fill:orange, color:black

    class LearnerTheoryUpdate Learner
    class LearnerTheorySigned Learner
    class LearnerTrainingUpdate Learner
    class LearnerTrainingSigned Learner
    class TeacherTheoryUpdate Teacher
    class TeacherTheorySigned Teacher
    class TeacherTrainingUpdate Teacher
    class TeacherTrainingSigned Teacher
    class TutorUpdate Tutor
    class TutorSigned Tutor

    state join_state1 <<join>>
    state join_state2 <<join>>
    state join_state3 <<join>>
    state join_state4 <<join>>
    state join_state5 <<join>>
    state join_state6 <<join>>

    LearnerTheoryUpdate: 1 = LearnerTheoryUpdate
    LearnerTheorySigned: 2 = LearnerTheorySigned
    TeacherTheoryUpdate: 3 = TeacherTheoryUpdate
    TeacherTheorySigned: 4 = TeacherTheorySigned
    LearnerTrainingUpdate: 5 = LearnerTrainingUpdate
    LearnerTrainingSigned: 6 = LearnerTrainingSigned
    TutorUpdate: 7 = TutorUpdate
    TutorSigned: 8 = TutorSigned
    TeacherTrainingUpdate: 9 = TeacherTrainingUpdate
    TeacherTrainingSigned: 10 = TeacherTrainingSigned

    [*] --> LearnerTheoryUpdate: Job#schedule1
    LearnerTheoryUpdate --> LearnerTheoryUpdate
    LearnerTheoryUpdate --> join_state1
    LearnerTheoryUpdate --> join_state2: Job#schedule2
    join_state1 --> LearnerTheorySigned
    join_state1 --> TeacherTheoryUpdate
    LearnerTheorySigned --> join_state2: Job#schedule2
    join_state2 --> LearnerTrainingUpdate
    join_state2 --> TeacherTheoryUpdate
    LearnerTrainingUpdate --> LearnerTrainingUpdate
    LearnerTrainingUpdate --> join_state5: Job#schedule4
    join_state5 --> LearnerTrainingSigned
    join_state5 --> TutorSigned
    join_state2 --> TutorUpdate
    TutorUpdate --> TutorUpdate
    TutorUpdate --> TutorSigned
    TutorUpdate --> join_state5: Job#schedule4
    TeacherTheoryUpdate --> TeacherTheoryUpdate
    TeacherTheoryUpdate --> join_state4: Job#schedule3
    join_state4 --> TeacherTheorySigned
    TeacherTheoryUpdate --> TeacherTheorySigned
    LearnerTrainingUpdate --> LearnerTrainingSigned
    TeacherTheorySigned --> join_state3: Model#status_semaphor
    LearnerTrainingSigned --> join_state3: Model#status_semaphor
    TutorSigned --> join_state3: Model#status_semaphor
    join_state3 --> TeacherTrainingUpdate
    TeacherTrainingUpdate --> TeacherTrainingUpdate
    TeacherTrainingUpdate --> TeacherTrainingSigned
    TeacherTrainingUpdate --> join_state6: Job#schedule5
    join_state6 --> TeacherTrainingSigned
```
