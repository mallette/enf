DOCUMENTATION (french only)
===========================

DEPLOIEMENT
-----------

### Pré-requis

```
sudo apt install -qy build-essential git ruby-dev libpq-dev mupdf-tools ffmpeg redis libvips-tools
```

### vers BETA (tout développeur ayant accès à beta)

* # depuis machine de dev
* ## commit + push sur la branche `dev`
* ssh bichik-beta opt/bichik/bin/beta

### vers PROD ou PREPROD (uniquement mainteneurs du projet!)

* # depuis machine de dev
* ### dépot git propre sur la branche `dev`!
* ./bin/semver_prod

> va demander un nouveau numéro de version
> créé un tag sur la branche `main` si vous avez les droits mainteneurs du projet!
> puis fusionne en retour sur la branche `dev`

* # vers preprod, déploie la dernière version de la branche `main`
* ssh bichik-preprod /opt/bichik/bin/preprod

* # vers prod, déploie la dernière version de la branche `main`
* ssh bichik-prod /opt/bichik/bin/prod

AUTRES
------

* [developers](./developers.md)
* [graf](./graf.md)
* diagrammes
  * [participant](./diagrams/participant.md)
  * fiche navette
    * [workflow](./diagrams/fiche_navette.md)
    * [scheduler](./diagrams/scheduler.md)
  * [zioguigoui](./diagrams/zigouigoui.md)
  * [les services](./diagrams/services.md)
  * [autres (dépréciés)](./diagrams/_deprecated.md)
