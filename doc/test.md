TEST
====


LOG (debug instead of warn)
==========================

```ruby
setup do
  SemanticLogger.default_level = :debug
  [...]
end
```


DEBUG
-----

* change [launch.json](../.vscode/launch.json) like so:
```
"program": "${workspaceFolder}/bin/rails test",
```
```
"program": "${workspaceFolder}/bin/rails test test/controllers/folder_controller_test.rb:19",
```


TEST with advanced fixture (useful for complex join)
----------------------------------------------------

* see [config_modules_participants](../test/fixtures/config_modules_participants.yml)
* https://api.rubyonrails.org/classes/ActiveRecord/FixtureSet.html#class-ActiveRecord::FixtureSet-label-Fixture+label+interpolation
* To test the entire 'test/system' folder, you need to run "rails test test/system" in the console. 
* Running just rails test Won't include 'test/system' in the testing process.
* for test system you can use "make_screenshot" function to see the current state of the web page