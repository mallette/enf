# Graf


Corespondances des termes utilisés dans Graf(en), Bichik(en) et les Céméa(fr)
-----------------------------------------------------------------


| Graf                           | Bichik           | CEMEA                    |
| ------------------------------ | ---------------- | ------------------------ |
| BaseCompany                    | School           | Agence Territoriale (AT) |
| TrainingCourse                 | Classroom        | Promo[*tion*]            |
| TrainingCredit                 | SkillUnit        | Unité de compétence      |
| TrainingSkillsBlock            | SkillsBlock      | Ruban pédagogique        |
| TeachingUnits                  | TeachingUnits    | Unité pédagogique        |
| Lecture                        | TeachingSequence | Séquence pédagogique     |
| TrainingIndividualRegistration | Registration     | Inscription              |
| BasePartner                    | User             | Utilisateur              |


## Mise à jour depuis *Graf*, répercussion dans *Bichik*

### Promo

* nom de la promo
  * maj nom => maj du nom dans bichik
* code
  * maj **possible dans graf ?** si oui => maj dans bichik
    * Cemea : la mise à jour du code de la promo est impossible. Ex : AT30-B5E23RDPR ne changera jamais
* statut
  * maj : **que faire dans Bichik ?** 
    * exemple: la promo repasse en brouillon, que faire ? 
    * la promo passe au statut fermée dans graf, que faire ? => on supprime tout ?
      * Cemea : impossible de repasser dans un mode antérieur : Brouillon, Production, Validé, Cloturé, Annulé (status = 1 pour Brouillon, 2 pour En production, 3 pour Validé, 4 pour Clôturé et 5 pour Annulé)
* date début et fin
  * est-ce qu'elle peuvent **changer** dans graf ? 
    * si oui => maj dans bichik
  * **que se passe-t-il quand la date de fin est dépassée ?**
    * Cemea : on ne change rien. C'est le statut qui définit l'état de la promo
* AT
  * **est-ce qu'une promo peut changer d'AT en cours de route ?**
* graf_id : ne doit jamais changer
* Suppression : **est-ce que vous supprimez les promos de graf ? restent-t-elles archivées ?**
  * Cemea : Elles ne sont jamais supprimés. Toujours archivées (au moins 5 ans)


### Stagiaire
* nom et prénom
  * maj : maj dans bichik + renommage du dossier  stagiaire  
* email
  * maj :** maj dans bichik + envoi d'un email de confirmation ?**
  * **est-ce qu'il y a une gestion des doublons d'adresse mail dans Graf ?**
    * Cemea : Techniquement, il peut y avoir une adresse e-mail utilisée par plusieurs personne. Mais très improbable dans Bichik.
* graf_id : ne doit jamais changer
* graf_status
  * **maj : que faire dans bichik ? (le stagiaire est radié dans graf, on supprime le stagiaire dans bichik ? on le désactive ? si on supprime, on garde ses documents ? ses pads ? ect...)**
    * Cemea : A TRAITER
* Suppression : **est-ce que vous supprimez les stagiaires de graf ? restent-t-ils archivés ?**
    * Cemea:  pas de suppression : archivé ou anonymisé (si demande d'application du RGPD / données personnelles)
    * Cemea : A TRAITER

### Formateur
* nom et prénom
  * maj => maj dans bichik 
* email
  * maj => **maj dans bichik + envoi d'un email de confirmation ?**
  * **est-ce qu'il y a une gestion des doublons d'adresse mail dans Graf ?**
    * Cemea : Techniquement, il peut y avoir une adresse e-mail utilisée par plusieurs personne. Mais très improbable dans Bichik.
* graf_id : ne doit jamais changer
* graf_status
  * **maj => que faire dans bichik ?** 
    * exemple: le formateur est radié dans graf, on supprime le stagiaire dans bichik ? on le désactive ? 
      * si on supprime, on garde ses documents ? ses pads ? etc...
* **Suppression : est-ce que vous supprimez les formateurs de graf ? restent-t-ils archivés ?**
  * Cemea:  pas de suppression : archivé ou anonymisé (si demande d'application du RGPD / données personnelles)
  * Cemea : A TRAITER

### unités pédagogique
* date de début et de fin
  * maj => maj dans bichik
  * **peut-il y avoir des superpositions de dates entre 2 unités pédagogiques pour une même promo ?**
    * Cemea : en théorie, pas de chevauchement. A VALIDER
* graf_id, ne doit jamais changer !
* nom
  * maj => maj dans bichik + changement du dossier appartenant à cette unité pédagogique
* **suppression : que fait-on ? on supprime dans bichik ? avec tous les dossiers associés ? on déplace les documents dans un dossier d'archives ?**
  * Cemea : on supprime le dossier de l'unité péda. C'est une information qu'ils auront en amont. A elles·eux de sauvegarder le contenu avant.

### AT
* nom :
  * maj => maj dans bichik
  
### unité de compétence

> pas utilisé dans bichik pour l'instant

### bloc de compétence 

> pas utilisé dans bichik pour l'instant