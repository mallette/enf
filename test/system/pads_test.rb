require 'application_system_test_case'

class PadTest < ApplicationSystemTestCase
  include Devise::Test::IntegrationHelpers
  include ActionView::Helpers::TranslationHelper

  setup do
    do_connect :one
  end

  test 'verifie si il redirige vers la page souhaité' do
    visit pads_url
    assert_selector 'h1', text: ts('modules.etherpad.padlist')
  end

  test "création d'un nouveau pad" do
    visit "#{pads_url}/new"
    @name = 'test2'
    fill_in 'pad_name', with: @name
    fill_in 'pad_comment', with: 'test1'
    click_button ts('actions.validate')
    assert_text I18n.t('modules.etherpad.noticecreate', pad_name: @name)

    # sleep 5
    # make_screenshot
  end
end
