require 'application_system_test_case'

class SessionTest < ApplicationSystemTestCase
  include Devise::Test::IntegrationHelpers
  include ActionView::Helpers::TranslationHelper

  setup do
    visit root_url
  end

  test "connection avec un utilisateur qui n'a qu'un participant" do
    user_one = users(:one)
    assert_equal(user_one.participants.size, 1)

    fill_in 'user_login', with: user_one.email
    fill_in 'user_password', with: user_one.sso_clear_password
    click_on 'user_submit'
    assert_text ts('participants.welcome_aboard', classroom: user_one.participants.first.classroom.name)
  end

  test 'connection avec aux deuxieme utilisateur qui possède plusieurs participant' do
    user_two = users(:two)
    assert_operator user_two.participants.size, :>, 1

    fill_in 'user_login', with: user_two.email
    fill_in 'user_password', with: user_two.sso_clear_password
    click_on 'user_submit'

    assert_selector 'h1', text: ts('participants.index.choose')
  end
end
