require 'application_system_test_case'

class MemosTest < ApplicationSystemTestCase
  test 'creating a new memo' do
    I18n.locale = :en # FIXME: il faudrait trouver de forcé la page web à chargé les page sur la langue choisi, biensur pour les tests
    do_connect :one
    visit new_memo_path

    assert_selector 'h1', text: I18n.t('modules.framemo.addmemo')

    fill_in 'memo_name', with: 'Test Memo'
    fill_in 'memo_comment', with: 'This is a test description.'

    click_button 'Valider'
    assert_text I18n.t('modules.framemo.noticecreate')
  end
end
