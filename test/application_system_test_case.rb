require 'test_helper'

TIMESTAMP_FORMAT = '%Y-%m-%d__%H_%M_%S'.freeze

Capybara.register_driver :selenium_chrome do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome,
                                      options: Selenium::WebDriver::Chrome::Options.new(args: ['headless']))
end

Capybara.default_driver = :selenium_chrome
Capybara.javascript_driver = :selenium_chrome

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :selenium, using: :headless_chrome, screen_size: [1400, 1400]
end

def make_screenshot
  filename = "#{Time.current.strftime(TIMESTAMP_FORMAT)}.png"
  page.save_screenshot(filename) # rubocop:disable Lint/Debugger
  logger.warn "[Screenshot Image]: ./tmp/capybara/#{filename}"
end

def do_connect(fixture_name)
  visit root_url
  user = users(fixture_name)
  fill_in 'user_login', with: user.email
  fill_in 'user_password', with: Rails.configuration.x.plain_password
  click_on 'user_submit'

  assert_selector 'h1', text: 'Welcome' # Condition qui verifie si "Welcome" se trouve sur la page d'acceuil
  # Rq: {assert_selector} est nécessaire pour faire une "pause conditionnel",
  # sinon la connexion n'aura pas le temps de ce faire sur le navigateur Virtuel
end
