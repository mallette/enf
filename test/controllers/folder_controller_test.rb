require_relative 'abstract/auth_participant_integration_test'
class FolderControllerTest < AuthParticipantIntegrationTest
  setup { authenticate_user_participant(:one, :one) }

  test 'should get root folder page due to teaching participant' do
    assert participants(:one).teaching?
    get folders_path
    assert_response :success
  end

  test 'should get transversal folder' do
    get folder_path(folders(:transversal_one))
    assert_response :success
  end

  test 'should be able to create new directory in transversal' do
    transversal_id = folders(:transversal_one).id
    post folders_path, params: { folder: { name: 'new folder', parent_id: transversal_id } }
    assert_redirected_to folder_path(transversal_id)
  end

  test 'should be able to zip when empty' do
    get folder_zip_path(folders(:transversal_one).id)
    assert_response :success
    assert_equal 'application/zip', @response.media_type
  end

  test 'should be able to zip when contains some directories' do
    transversal_id = folders(:transversal_one).id
    post folders_path, params: { folder: { name: 'new folder1', parent_id: transversal_id } }
    post folders_path, params: { folder: { name: 'new folder2', parent_id: transversal_id } }
    get folder_zip_path(transversal_id)
    assert_response :success
    assert_equal 'application/zip', @response.media_type
  end
end
