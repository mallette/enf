require 'test_helper'

class ParticipantsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = users(:one)
    @user2 = users(:two)
  end

  test 'should get index if the user has more than one participant available' do
    sign_in @user2
    get participants_path
    assert_response :success
  end

  test 'should directly redirect to services' do
    sign_in @user
    get participants_path
    assert_redirected_to services_path
  end

  test 'should enrolls into participant' do
    sign_in @user
    post participant_path(@user)
    assert_redirected_to services_path
  end
end
