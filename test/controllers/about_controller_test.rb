require_relative 'abstract/auth_participant_integration_test'

class AboutControllerTest < AuthParticipantIntegrationTest
  setup { authenticate_user_participant(:one, :one) }

  test 'should get about page' do
    get about_path
    assert_response :success
  end
end
