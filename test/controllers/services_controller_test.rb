require_relative 'abstract/auth_participant_integration_test'

class ServicesControllerTest < AuthParticipantIntegrationTest
  setup { authenticate_user_participant(:one, :one) }

  test 'should get index' do
    get services_path
    assert_response :success
  end

  test 'should get show' do
    get services_path(@user)
    assert_response :success
  end

  # TODO: test 'should be redirected if not allowed to see the service'
end
