require_relative 'abstract/auth_participant_integration_test'
class UsersControllerTest < AuthParticipantIntegrationTest
  setup { authenticate_user_participant(:one, :one) }

  # Helper method for testing invalid password updates
  def patch_user_with_invalid_password_test(password, password_confirmation, error_message)
    patch user_path(@user),
          params: { user: { email: @user.email, password:, password_confirmation:,
                            first_name: 'Test' } }
    assert_redirected_to user_path(@user)
    assert_equal error_message, flash[:alert]
    refute_equal 'Test', User.find(@user.id).first_name
  end

  test 'should get edit' do
    get edit_user_path(@user)
    assert_response :success
  end

  test 'should update user with valid data' do
    patch user_path(@user),
          params: { user: { email: @user.email,
                            password: 'TestMotDePasse123',
                            password_confirmation: 'TestMotDePasse123',
                            first_name: 'Test' } }
    assert_redirected_to services_path
    assert_equal I18n.t('users.noticeupdate'), flash[:notice]
    assert_equal 'Test', User.find(@user.id).first_name
  end

  test 'should not update user if the password is too short' do
    patch_user_with_invalid_password_test('Test1', 'Test1', I18n.t('users.problem_password_pattern'))
  end

  test 'should not update user if the password doesn\'t contain at least one digit' do
    patch_user_with_invalid_password_test('Testtesttest', 'Testtesttest', I18n.t('users.problem_password_pattern'))
  end

  test 'should not update user if the password doesn\'t contain at least one char' do
    patch_user_with_invalid_password_test('2347289346729', '2347289346729', I18n.t('users.problem_password_pattern'))
  end

  test 'should not update user if the password doesn\'t contain at least one uppercased char' do
    patch_user_with_invalid_password_test('salutsalutsalut9', 'salutsalutsalut9',
                                          I18n.t('users.problem_password_pattern'))
  end

  test 'should not update user if the password doesn\'t contain at least one lowercased char' do
    patch_user_with_invalid_password_test('SALUTSALUT9', 'SALUTSALUT9', I18n.t('users.problem_password_pattern'))
  end

  test 'should not update user if the password/confirm password don\'t match' do
    patch_user_with_invalid_password_test('SALUT', 'TEST', I18n.t('users.problem_password'))
  end

  test 'should not update user if no changes are made' do
    patch user_path(@user),
          params: { user: { email: @user.email, password: '', password_confirmation: '',
                            first_name: @user.first_name } }
    assert_redirected_to services_path
    assert_equal I18n.t('users.noticeupdate'), flash[:notice]
    assert_equal @user.first_name, User.find(@user.id).first_name
  end

  test 'should not allow another user to edit profile' do
    @other_user = users(:two)
    sign_in @other_user
    patch user_path(@user),
          params: { user: { email: 'newemail@test.com' } }
    assert_redirected_to services_path
    assert_equal I18n.t('application.law'), flash[:notice]
    refute_equal 'newemail@test.com', User.find(@user.id).email
  end
end
