require_relative 'abstract/auth_participant_integration_test'
class VisiosControllerTest < AuthParticipantIntegrationTest
  setup { authenticate_user_participant(:one, :one) }

  test 'should get index' do
    get visios_path
    assert_response :success
  end

  test 'should get show' do
    get visio_path(visios(:one))
    assert_response :success
  end
end
