require 'test_helper'

class AuthParticipantIntegrationTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  # sign in as user, then choose participant
  def authenticate_user_participant(user_fixture_name, participant_fixture_name)
    @user = users(user_fixture_name)
    @participant = participants(participant_fixture_name)
    sign_in @user
    post participant_path(@user, @participant)
  end
end
