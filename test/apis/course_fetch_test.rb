require 'test_helper'
require 'webmock/minitest'

class ApiFetchCourseTest < ActionDispatch::IntegrationTest
  setup do
    @response = File.read(Rails.root.join('test', 'stubs', 'responses', 'course_fetch.json'))

    stub_request(:post, %r{https://project-cemea.axelor.com/cemea/ws/rest/com.axelor.apps.training.db.Course/\d+/fetch})
      .to_return(status: 300, body: '{"warning": "Redirected: Missing cookie, redirected to login"}', headers: {})

    stub_request(:post, %r{https://project-cemea.axelor.com/cemea/ws/rest/com.axelor.apps.training.db.Course/\d+/fetch})
      .with(headers: { 'Cookie' => /JSESSIONID=[^;]+/ })
      .to_return(status: 200, body: @response, headers: {})
  end

  test 'should return 200 with cookie' do
    response = RestClient.post('https://project-cemea.axelor.com/cemea/ws/rest/com.axelor.apps.training.db.Course/1/fetch',
                               {},
                               { cookies: { JSESSIONID: 'test' } })
    assert_equal 200, response.code
    assert_equal 'CEMÉA Picardie', JSON.parse(response.body)['company']['name']
  end

  test 'should return 300 without cookie' do
    RestClient.post('https://project-cemea.axelor.com/cemea/ws/rest/com.axelor.apps.training.db.Course/1/fetch', {})
  rescue RestClient::ExceptionWithResponse => e
    response = e.response

    assert_equal 300, response.code
    assert_equal 'Redirected: Missing cookie, redirected to login', JSON.parse(response.body)['warning']
  end
end
