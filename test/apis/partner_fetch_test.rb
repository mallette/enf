require 'test_helper'
require 'webmock/minitest'

class ApiFetchPartnerTest < ActionDispatch::IntegrationTest
  setup do
    @response = File.read(Rails.root.join('test', 'stubs', 'responses', 'partner_fetch.json'))
    stub_request(:post, %r{https://project-cemea.axelor.com/cemea/ws/com.axelor.apps.base.db.Partner/\d+/fetch})
      .to_return(status: 200, body: @response, headers: {})
  end
  test 'should respond with a user' do
    response = RestClient.post 'https://project-cemea.axelor.com/cemea/ws/com.axelor.apps.base.db.Partner/2637/fetch', {}
    assert_equal 200, response.code
    assert_equal 'François', JSON.parse(response.body)['firstName']
  end
end
