require 'test_helper'
require 'webmock/minitest'

class ApiSearchCourseTest < ActionDispatch::IntegrationTest
  setup do
    @response = File.read(Rails.root.join('test', 'stubs', 'responses', 'course_search.json'))
    @param = File.read(Rails.root.join('test', 'stubs', 'params', 'course_search.json'))

    stub_request(:post, 'https://project-cemea.axelor.com/cemea/ws/rest/com.axelor.apps.training.db.Course/search')
      .with(headers: { 'Cookie' => /JSESSIONID=[^;]+/ }, body: @param)
      .to_return(status: 200, body: @response, headers: {})
  end

  test 'should return a list of courses' do
    response = RestClient.post(
      'https://project-cemea.axelor.com/cemea/ws/rest/com.axelor.apps.training.db.Course/search', @param, { cookies: { JSESSIONID: 'testcookie' } }
    )

    assert_equal 200, response.code
    assert_equal '24ème journée de psychiatrie d\'Abbeville "Ryhtmes"', JSON.parse(response.body)[0]['name']
  end
end
