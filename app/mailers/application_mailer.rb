class ApplicationMailer < ActionMailer::Base
  layout 'mailer'
  default from: "#{Rails.configuration.x_option(:from_name)} <#{Rails.configuration.x_option(:from_email)}>"
  before_action :add_logos
  before_deliver :filter_delivery

  def mail(headers = {}, &)
    headers[:subject] = "ENF: #{headers[:subject]}"
    headers = cheat_destination(headers)
    logger.info("email queued!\nfrom: #{headers[:from]}\nto: #{headers[:to]}\nsubject: #{headers[:subject]}") do
      super
    rescue StandardError => e
      logger.error 'unable to send email', exception: e
    end

    logger.debug("⇤ app/mailers/#{headers[:template_path]&.first}.rb") if headers[:template_path]&.any?
  end

  private

  def filter_delivery
    return if Rails.configuration.action_mailer.perform_deliveries

    logger.warn('email will not be delivered due to <Rails.configuration.action_mailer.perform_deliveries>')
    throw :abort
  end

  # should occur for beta or preprod only!
  def cheat_destination(headers)
    cheat_to = Rails.configuration.action_mailer.smtp_settings.dig(:cheat, :to)
    return headers unless cheat_to

    old_to = headers[:to]
    headers[:to] = cheat_to
    headers[:subject] = "[ORIGINAL_TO: #{old_to}] #{headers[:subject]}"
    logger.warn("cheating the TO: destination original was <#{old_to}> replaced by <#{headers[:to]}>")
    headers
  end

  def add_logos
    attachments.inline['logo_bichik_complet.png'] = File.read(
      Rails.root.join('app', 'assets', 'images', 'commun', 'logo_bichik_complet.png')
    )
    attachments.inline['cemea-logo-nuage-asso.png'] = File.read(
      Rails.root.join('app', 'assets', 'images', 'commun', 'cemea-logo-nuage-asso.png')
    )
  end
end
