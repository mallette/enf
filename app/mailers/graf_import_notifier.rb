class GrafImportNotifier < ApplicationMailer
  def emit(to, responsible, classroom, messages)
    subject = I18n.t('graf.import.notifier.subject', classroom_code: classroom.code)
    attachments.inline['logo_bichik.png'] = Rails.configuration.x_option(:attachment_logo)

    @messages = messages
    @responsible = responsible
    @classroom_code = classroom.code

    mail(to:, subject:)
  end
end
