class PadUserNotifier < ApplicationMailer
  def invit_email(user, to, pad, url, host)
    @from = user
    @pad = pad
    @url = url
    @host = host
    mail(reply_to: "#{@from.name} <#{@from.email}>",
         bcc: to,
         subject: 'Invitation à un pad')
  end
end
