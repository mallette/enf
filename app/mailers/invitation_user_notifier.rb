class InvitationUserNotifier < ApplicationMailer
  def invit_email(user, to, invitation, url, host, tag)
    @tag = tag
    @from = user
    @invitation = invitation
    @url = url
    @host = host

    attachments.inline['logo_bichik_complet.png'] =
      File.read("#{Rails.root}/app/assets/images/commun/logo_bichik_complet.png")

    attachments.inline['cemea-logo-nuage-asso.png'] =
      File.read("#{Rails.root}/app/assets/images/commun/cemea-logo-nuage-asso.png")

    reply_to = "#{@from.name} <#{@from.email}>"
    subject = I18n.t "invitation.notifier.#{@tag}.subjet"

    mail(reply_to:, to:, subject:)
  end

  def meeting_invit_email(user, to, meeting)
    require 'icalendar/tzinfo'
    @tag = 'meeting'
    @from = user
    @meeting = meeting
    @to = to

    cal = Icalendar::Calendar.new
    tzid = 'UTC'
    tz = TZInfo::Timezone.get tzid
    timezone = tz.ical_timezone meeting.started_at
    cal.add_timezone timezone
    cal.event do |e|
      e.dtstart = Icalendar::Values::DateTime.new(meeting.started_at, tzid:)
      e.dtend = Icalendar::Values::DateTime.new(meeting.ended_at, tzid:)
      e.summary = meeting.name
      e.description = meeting.description
      # e.organizer = Icalendar::Values::CalAddress.new("mailto:#{meeting.participant.user.email}", cn: meeting.participant.name)
    end

    attachments['rendez-vous.ics'] = { mime_types: 'text/calendar', content: cal.to_ical }

    reply_to = "#{@from.name} <#{@from.email}>"
    subject = I18n.t 'invitation.notifier.meeting.subjet'

    mail(reply_to:, to:, subject:)
  end
end
