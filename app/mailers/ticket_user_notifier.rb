class TicketUserNotifier < ApplicationMailer
  def email_creation_ticket(user, to, ticket, url, _messages)
    @from = user
    @ticket = ticket
    @url = url
    mail(reply_to: "#{@from.name} <#{@from.email}>",
         bcc: to,
         subject: I18n.t('tickets.create_message', id: @ticket.id, title: @ticket.title))
  end

  def email_notif_ticket(user, to, ticket, url, messages)
    @from = user
    @ticket = ticket
    @url = url
    @messages = messages
    mail(reply_to: "#{@from.name} <#{@from.email}>",
         bcc: to,
         subject: I18n.t('tickets.email_notification', id: @ticket.id, from: @from.name))
  end
end
