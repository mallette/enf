class SkillUnit < ApplicationRecord
  belongs_to :classroom
  belongs_to :skill_block
  has_many :teaching_sequences, dependent: :destroy
end
