class Meeting < ApplicationRecord
  include ParticipantFeature

  belongs_to :participant, touch: true
  has_one :next_meeting, class_name: 'Meeting'

  before_destroy :untie_next_meeting

  def create_weekly(recurring_end_date, recurrence = 1)
    return if next_meeting

    meeting = self
    start = meeting.start_time + recurrence.week
    stop = meeting.end_time + recurrence.week
    while start < recurring_end_date
      meeting = meeting.next_meeting = Meeting.create(name:, start_time: start, end_time: stop, user_id:)
      start = meeting.start_time + recurrence.week
      stop = meeting.end_time + recurrence.week
    end
  end

  def destroy_recurring
    next_meeting&.destroy_recurring
    destroy
  end

  private

  def untie_next_meeting
    meeting = Meeting.find_by(meeting_id: id)
    meeting&.update meeting_id: nil
  end
end
