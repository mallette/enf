class Participant < ApplicationRecord
  validates :token, allow_nil: true, uniqueness: true

  belongs_to :user
  belongs_to :classroom

  has_and_belongs_to_many :groups
  has_and_belongs_to_many :config_modules

  has_many :folders, dependent: :destroy
  has_many :visios, dependent: :destroy
  has_many :pads, dependent: :destroy
  has_many :memos, dependent: :destroy
  has_many :meetings, dependent: :destroy
  has_many :links, dependent: :destroy
  has_many :bookmarks, dependent: :destroy
  has_many :archives, dependent: :destroy

  delegated_type :participable, types: %w[Teaching Learning Tutoring], dependent: :destroy
  delegate :involved_shuttle_forms, to: :participable
  delegate :involved_participants, to: :participable
  delegate :participant_icon, to: :participable
  delegate :name, to: :user
  delegate :email, to: :user
  delegate :shuttle_form_permissions, to: :participable

  after_create :enable_all_modules
  after_create :populate_automatic_group
  after_create :create_folder, if: -> { learning? }
  after_create :create_shuttle_forms, if: -> { learning? }
  before_destroy :remove_from_automatic_group
  before_destroy :disable_all_modules

  def to_s = "#{participable_type}#{self.class.name}##{id}"
  def self.ransackable_associations(_) = %w[user classroom archives folders groups memos pads teachings visios]

  def self.ransackable_attributes(_) = %w[user_id classroom_id created_at id id_value participable_id participable_type
                                          updated_at]

  # FIXME: should use class instead!
  def config_module_by_name_enabled?(name) = config_modules.exists?(name:)

  # FIXME: still useful? rather use method above!
  def enabled?(mod)
    return false unless mod

    config_modules.include?(mod)
  end

  def enable_all_modules
    config_modules << ConfigModule.all
  end

  def modules_enabled
    config_modules
  end

  def enable!(mod)
    return if enabled?(mod)

    config_modules << mod
  end

  def disable!(mod)
    return unless enabled?(mod)

    Visio.where(participant: self).destroy_all if mod.name == 'BigBlueButton'
    Memo.where(participant: self).destroy_all if mod.name == 'Framemo'
    config_modules.delete(mod)
  end

  def disable_all_modules
    config_modules.destroy_all
  end

  # FIXME: reutiliser particpable_type dans le champ automatic en respectant le nom du type de participant plutôt que des Strings (:learners, ...)
  def group_automatic_name
    case participable_type
    when 'Learning' then 'learners'
    when 'Tutoring' then 'tutors'
    else 'teachers'
    end
  end

  def remove_from_automatic_group
    group = groups.find_by(automatic: group_automatic_name.to_sym)
    return unless group

    logger.info("Retrait du participant <#{name}> au groupe <#{group.name}>")
    group.participants.delete(self)
  end

  def populate_automatic_group
    logger.info('populate_automatic_group')
    group = Group.find_by(automatic: group_automatic_name.to_sym, classroom:)
    return unless group

    logger.info("Ajout du participant <#{name}> au groupe <#{group.name}>")
    group.participants << self unless group.participants.include?(self)
  end

  def generate_or_get_token
    update token: UUIDTools::UUID.random_create.to_s unless token
    token
  end

  private

  def create_folder
    # TODO: le champ automatic devrait contenir un type au lieu d'un boolean, on filtrerai sur automatic au lieu de name
    parent = Folder.find_by(name: 'Espace individuel des stagiaires', automatic: true, classroom:)
    return unless parent

    teachers_group = Group.find_by(automatic: :teachers, classroom:)
    tutors_ids = participable.tutors.map(&:id)
    permissions = { r: { u: [id], g: [teachers_group.id] } }
    logger.info "Create learner folder <#{user.name}>"
    learner_folder = Folder.create name: user.name, permissions:, parent_id: parent.id, classroom:, automatic: true,
                                   participant_id: id
    parent.update(comment: nil)

    Folder.create name: 'Stagiaire-Formateurs',
                  automatic: true,
                  classroom:,
                  parent_id: learner_folder.id,
                  permissions: { r: { u: [id], g: [teachers_group.id] },
                                 w: { u: [id], g: [teachers_group.id] } }
    Folder.create name: 'Stagiaire-Formateurs-Tuteurs',
                  automatic: true,
                  classroom:,
                  parent_id: learner_folder.id,
                  permissions: { r: { u: [id] + tutors_ids, g: [teachers_group.id] },
                                 w: {
                                   u: [id] + tutors_ids, g: [teachers_group.id]
                                 } }
  end

  def create_shuttle_forms
    logger.debug("creation de toutes ses fiches navettes (#{classroom.teaching_units.size}) pour le stagiaire <#{user.name}>")
    classroom.teaching_units.each do |tu|
      ShuttleForm.create(learning: participable, teaching_unit: tu)
    end
  end
end
