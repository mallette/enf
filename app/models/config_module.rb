# Pour gérer les modules
class ConfigModule < ApplicationRecord
  has_and_belongs_to_many :participants

  has_many :domain_infos, dependent: :destroy
  has_many :meta_options, dependent: :destroy
  has_many :instances, dependent: :destroy
  has_many :visios, dependent: :destroy
  has_many :pads, dependent: :destroy

  def instance_options
    filter_meta_options :instance
  end

  def domain_options
    filter_meta_options :domain
  end

  def user_options
    filter_meta_options :user
  end

  def user_options_enabled
    filter_meta_options(:user).select { |mo| mo.key.to_sym == MetaOption::ENABLED }.first
  end

  def user_options_quota
    filter_meta_options(:user).select { |mo| mo.key == 'quota' }.first
  end

  def instance_for_classroom(classroom)
    DomainInfo.where(classroom:, config_module_id: id)&.first&.instance
  end

  private

  def filter_meta_options(symbol)
    meta_options.select { |mo| mo.field == symbol.to_s }
  end
end
