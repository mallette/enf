class Teaching < ApplicationRecord
  include Participable

  has_one :user, through: :participant

  # retourne tous les stagiaires + formateurs + tuteurs - lui-même
  def involved_participants
    ids = participant.classroom.participants.pluck(:id)
    ids.uniq!
    ids.delete(participant)
    Participant.where(id: ids)
  end

  def involved_shuttle_forms
    participant.classroom.shuttle_forms.joins(learning: :user)
               .joins(:teaching_unit).order('teaching_unit.start_date DESC',
                                            'users.last_name', 'users.first_name')
  end

  def participant_icon = 'chalkboard-teacher'

  def shuttle_form_permissions(shuttle_form)
    permissions = {}
    permissions[:learner_theory] = { r: shuttle_form.learner_status_index > 1,
                                     w: false }
    permissions[:teacher_theory] = { r: shuttle_form.teacher_status_index.positive?,
                                     w: shuttle_form.teacher_theory_update? }
    permissions[:learner_training] = { r: shuttle_form.learner_status_index > 5,
                                       w: false }
    permissions[:tutor_training] = { r: shuttle_form.tutor_status_index > 7,
                                     w: false }
    permissions[:teacher_training] = { r: shuttle_form.teacher_status_index >= 9,
                                       w: shuttle_form.teacher_status_index == 9 }
    permissions
  end
end
