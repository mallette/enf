class Folder < ApplicationRecord
  include Bookmarkable
  audited only: %i[name parent_id]
  broadcasts_refreshes

  belongs_to :classroom
  belongs_to :participant, optional: true
  belongs_to :teaching_unit, optional: true
  belongs_to :parent, class_name: 'Folder', optional: true, touch: true
  has_many :children, class_name: 'Folder', foreign_key: 'parent_id', dependent: :destroy
  has_many_attached :files do |attachable|
    # logger.warn 'Variant generating...'
    # attachable.variant :thumb, resize_to_limit: [100, 100], preprocessed: true
    # logger.warn 'Variant generated'
  end
  serialize :permissions, type: Hash

  validate :unrecursive_parent
  after_save :check_name
  after_update :check_filenames

  def read?(participant)
    rpermissions = recursive_permissions
    return false if rpermissions.empty?

    return true if rpermissions[:r] == :all

    return false unless rpermissions[:r]

    return true if rpermissions[:r][:u].include?(participant.id)

    participant_groups_ids = participant.groups.where(classroom_id:).map(&:id)
    (participant_groups_ids & rpermissions[:r][:g]).count.positive?
  end

  def write?(participant)
    rpermissions = recursive_permissions
    return false if rpermissions.empty?
    return false unless rpermissions[:w]
    return true if rpermissions[:w] == :all
    return true if rpermissions[:w][:u].include?(participant.id)

    participant_groups_ids = participant.groups.where(classroom:).map(&:id)
    (participant_groups_ids & rpermissions[:w][:g]).count.positive?
  end

  def recursive_permissions
    return permissions unless permissions.empty?

    return {} unless parent_id

    parent.recursive_permissions
  end

  def root
    return parent.root if parent

    self
  end

  def trash? = self == classroom.trash_folder
  def root? = self == classroom.root_folder
  def recursive_size = size + children.map(&:recursive_size).reduce(:+).to_i
  def size = files.where(deleted: false).map(&:byte_size).reduce(:+).to_i
  def destroyable?(participant) = write?(participant) && !automatic && participant_id.nil? && teaching_unit_id.nil?
  def in_trash? = root.id == classroom.trash_folder.id
  def count_label = "#{children.count} dossier(s) et #{files.where(deleted: false).count} fichier(s)"

  def check_name
    name.gsub!('/', '_')
    return unless parent
    return unless parent.children.where(name:).count > 1

    self.name += '_copie'
    save
  end

  def check_filenames
    filenames = files.reject(&:deleted).map(&:filename)
    duplicates = filenames.detect { |e| filenames.count(e) > 1 }
    return unless duplicates

    file = files.reject(&:deleted).select { |f| f.filename == duplicates.to_s }.last
    file.update filename: "#{file.filename.base}_copie.#{file.filename.extension}"
    check_filenames
  end

  def tree(participant)
    children_array = []
    children.order(:name).each do |child|
      children_array << { id: child.id,
                          name: child.name,
                          write: child.write?(participant),
                          children: child.children.order(:name).map do |c|
                            c.tree(participant)
                          end }
    end
    { id:, name:, write: write?(participant), children: children_array }
  end

  def self.tree(classroom, participant)
    tree = []
    Folder.where(parent: classroom.root_folder, classroom:).order(:name).each do |folder|
      tree << folder.tree(participant)
    end
    tree
  end

  def path = [parent&.path, self].flatten.compact

  def recursive_children
    folders = []
    folders << children
    folders << children.map(&:recursive_children)
    folders.flatten
  end

  def recursive_files
    rfiles = []
    rfiles << files
    rfiles << children.map(&:recursive_files)
    rfiles.flatten
  end

  def search_children(search_query, participant = nil)
    recursive_children.select do |child|
      I18n.transliterate(child.name).downcase.include?(I18n.transliterate(search_query.downcase)) && child.read?(participant)
    end
  end

  def search_files(search_query, participant = nil)
    recursive_files.select do |file|
      I18n.transliterate(file.blob.filename.to_s).downcase.include?(I18n.transliterate(search_query).downcase) && file.read?(participant)
    end
  end

  def copy_to(new_parent)
    new_folder = Folder.create name:, permissions:, classroom:, parent: new_parent
    files.each do |file|
      new_folder.files.attach(io: StringIO.new(file.download),
                              filename: file.filename,
                              content_type: file.content_type)
    end
    children.each { |child| child.copy_to(new_folder) }
  end

  # Rk: room for improvement -> estimate final size to improve user experience
  # https://github.com/julik/zip_kit#writing-zip-files-using-the-streamer-bypass
  def generate_zip(stream, folder_name = name)
    files.each do |blob|
      filename = join_name(folder_name, blob.filename)
      stream.write_file(filename) do |sink|
        blob.download do |chunk|
          sink.write chunk
        end
      end
    end
    children.each do |child|
      dirname = join_name(folder_name, child.name)
      stream.add_empty_directory(dirname:)
      child.generate_zip(stream, dirname)
    end
  end

  private

  def join_name(folder, file) = "#{folder}#{File::SEPARATOR}#{file}"

  def unrecursive_parent
    return if parent_id.nil?
    return unless parent_id == id || recursive_children.map(&:id).include?(parent_id)

    errors.add(:parent, 'unresolved recursivity')
  end
end
