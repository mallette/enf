class SkillBlock < ApplicationRecord
  has_many :learnings
  has_many :skill_units
  has_one :classroom
end
