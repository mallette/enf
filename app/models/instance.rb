class Instance < ApplicationRecord
  belongs_to :config_module
  belongs_to :instance_state
  has_many :domain_infos, dependent: :destroy
  has_many :instance_options, dependent: :destroy
  has_many :events, dependent: :destroy

  def option_by_name(name)
    mo = MetaOption.instance_option(config_module, name)
    io = instance_options.where(instance_id: self, meta_option_id: mo).first
    if io.nil?
      raise ArgumentError,
            "Instance <#{id}>, Argument <#{name}> n'existe pas pour le config_module <#{config_module.id}>"
    end

    io.value
  end

  def name_with_module
    "#{config_module.title} (#{name})"
  end

  def users_count
    domain_infos.map { |domain_info| domain_info&.domain&.users&.count.to_i }.inject(0, &:+)
  end

  def last_event
    Event.where(instance: self).order(:created_at).last
  end
end
