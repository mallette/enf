class Group < ApplicationRecord
  has_and_belongs_to_many :participants
  belongs_to :classroom

  def automatic? = !automatic.nil?
  def users = participants.map(&:user)
  def self.ransackable_attributes(_ = nil)= ['name']

  def create_automatic_services
    # TODO: vérifier si le service est bien actif avant de lancer la création
    logger.info "create_automatic_services for group <#{name}>"
    visio = Visio.create(name:, started_at: DateTime.now, user: classroom.teachings.first, classroom_id: classroom.id)
    Invitation.create(invitable: visio, recipient: self)
    memo = Memo.create(name:, user: classroom.teachings.first, classroom_id: classroom.id)
    Invitation.create(invitable: memo, recipient: self)
    pad = Pad.create(name:, user: classroom.teachings.first, classroom_id: classroom.id)
    Invitation.create(invitable: pad, recipient: self)
  end
end
