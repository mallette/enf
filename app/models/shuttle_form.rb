# Fiche Navette
# -------------------
# mieux comprendre l'aspect métier grace au
# [doc/diagrams/fiche_navette.md](diagramme)
# -------------------
class ShuttleForm < ApplicationRecord
  enum :learner_status, learner_inactive: 0,
                        learner_theory_update: 1,
                        learner_theory_signed: 2,
                        learner_training_update: 5,
                        learner_training_signed: 6
  enum :teacher_status, teacher_inactive: 0,
                        teacher_theory_update: 3,
                        teacher_theory_signed: 4,
                        teacher_training_update: 9,
                        teacher_training_signed: 10
  enum :tutor_status, tutor_inactive: 0,
                      tutor_update: 7,
                      tutor_signed: 8

  belongs_to :teaching_unit
  belongs_to :learning
  has_one :classroom, through: :learning
  belongs_to :teacher_theory_signed_by, class_name: 'User', optional: true
  belongs_to :teacher_training_signed_by, class_name: 'User', optional: true
  belongs_to :tutor_training_signed_by, class_name: 'User', optional: true

  # FIXME: le status_changed? n'a pas l'air de fonctionner
  after_update :check_status_change # , if: :status_changed?

  broadcasts_refreshes

  # FIXME: prefer using TeachingUnit#training_end_date for speed!
  def next
    ShuttleForm.joins(:teaching_unit)
               .where(teaching_units: { classroom: })
               .where('teaching_units.start_date > ?', teaching_unit.start_date)
               .order('teaching_units.start_date')
               .first
  end

  def status_changed?
    learner_status_changed? || teacher_status_changed? || tutor_status_changed?
  end

  def check_status_change
    teacher_theory_update! if learner_theory_signed? && teacher_inactive?
    status_semaphor
  end

  def status_semaphor
    return unless teacher_theory_signed? && learner_training_signed? && tutor_signed?

    teacher_training_update!
    logger.info("status_semaphor updates teacher_status to teacher_training_update for shuttleForm <#{id}>")
  end

  def learner_status_index = ShuttleForm.learner_statuses[learner_status]
  def teacher_status_index = ShuttleForm.teacher_statuses[teacher_status]
  def tutor_status_index = ShuttleForm.tutor_statuses[tutor_status]

  private

  def read_learner_theory(user)
    !learner_inactive?
  end

  def write_learner_theory(user)
    learner_theory_update? && learning.user == user
  end

  def read_teacher_theory(user)
    !teacher_inactive?
  end

  def write_teacher_theory(user)
    teacher_theory_update? && shuttle_form_teacher?(user)
  end

  def read_learner_training(user)
    ShuttleForm.learner_statuses[learner_status] >= ShuttleForm.learner_statuses[:learner_training_update]
  end

  def write_learner_training(user)
    learner_training_update? && learning.user == user
  end

  def read_tutor_training(user)
    ShuttleForm.tutor_statuses[tutor_status] >= ShuttleForm.tutor_statuses[:tutor_update]
  end

  def write_tutor_training(user)
    tutor_update? && shuttle_form_tutor?(user)
  end

  def read_teacher_training(user)
    ShuttleForm.teacher_statuses[teacher_status] >= ShuttleForm.teacher_statuses[:teacher_training_update]
  end

  def write_teacher_training(user)
    teacher_training_update? && shuttle_form_teacher?(user)
  end

  def shuttle_form_learner?(user)
    user.id == learning.user_id
  end

  def shuttle_form_teacher?(user)
    learning.classroom.teacher?(user)
  end

  def shuttle_form_tutor?(user)
    learning.tutors.include?(user)
  end

  def learner_theory_date_reached?
    Date.today >= (teaching_unit.end_date + 2.day)
  end

  def teacher_theory_date_reached?
    Date.today >= (teaching_unit.end_date + 7.day)
  end

  def training_end_date_reached?
    Date.today >= (teaching_unit.training_end_date || learning.classroom.end_date)
  end

  def training_end_date_plus_7_reached?
    Date.today >= (teaching_unit.training_end_date || learning.classroom.end_date) + 7.day
  end
end
