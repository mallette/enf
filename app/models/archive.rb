class Archive < ApplicationRecord
  include Loggable::Lifetime

  belongs_to :archivable, polymorphic: true
  belongs_to :participant
end
