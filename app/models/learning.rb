class Learning < ApplicationRecord
  include Participable

  belongs_to :skill_block
  has_and_belongs_to_many :tutorings
  has_one :user, through: :participant
  has_one :classroom, through: :participant
  has_many :tutors, through: :tutorings
  has_many :shuttle_forms, dependent: :destroy

  def involved_participants
    # FIXME: utiliser une seule requête avec condition OR
    participants = participant.classroom.teachings.map(&:participant) +
                   participant.classroom.learnings.map(&:participant) +
                   tutorings.map(&:participant)
    participants.delete(participant) # FIXME: where.not(participant:)
    Participant.where(id: participants.map(&:id))
  end

  def involved_shuttle_forms = shuttle_forms.joins(:teaching_unit).order('teaching_units.start_date DESC')
  def participant_icon = 'user-graduate'

  def shuttle_form_permissions(shuttle_form)
    permissions = {}
    permissions[:learner_theory] = { r: shuttle_form.learner_status_index.positive?,
                                     w: shuttle_form.learner_theory_update? }
    permissions[:teacher_theory] = { r: shuttle_form.teacher_status_index > 3,
                                     w: false }
    permissions[:learner_training] = { r: shuttle_form.learner_status_index >= 5,
                                       w: shuttle_form.learner_training_update? }
    permissions[:tutor_training] = { r: shuttle_form.tutor_status_index > 7,
                                     w: false }
    permissions[:teacher_training] = { r: shuttle_form.teacher_status_index > 9,
                                       w: false }
    permissions
  end
end
