class TeachingSequence < ApplicationRecord
  belongs_to :skill_unit
  belongs_to :teaching_unit
end
