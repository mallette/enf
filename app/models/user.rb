class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable

  before_create :set_sso_clear_password
  before_update :set_sso_clear_password
  after_update :update_folders

  has_many :participants, dependent: :destroy
  has_many :folders, through: :participants

  validates :graf_id, presence: true, uniqueness: { case_sensitive: false }
  validates :email, presence: true, uniqueness: { case_sensitive: false }

  broadcasts_refreshes

  attr_writer :login

  def self.ransackable_attributes(_auth_object = nil)
    %w[email first_name graf_id last_name]
  end

  def self.ransackable_associations(auth_object = nil)
    %w[archives classroom_user_config_modules folders groups learners memos pads teachings
       visios]
  end

  def login
    @login || graf_id || email
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if (login = conditions.delete(:login))
      where(conditions.to_h).where(['lower(graf_id) = :value OR lower(email) = :value',
                                    { value: login.downcase }]).first
    elsif conditions.key?(:graf_id) || conditions.key?(:email)
      where(conditions.to_h).first
    end
  end

  def admin = false
  def name = "#{first_name} #{last_name.upcase}"
  def learner_classrooms = Classroom.joins(:learnings).where('learnings.id': learnings).distinct
  def teacher_classrooms = Classroom.joins(:teachings).where('teachings.id': teachings).distinct
  def tutor_classrooms = Classroom.where(id: tutorings.map(&:classroom).map(&:id).uniq)

  def modules(classroom)
    participant = participant(classroom)
    ConfigModule.select { |mod| participant.enabled?(mod) }
  end

  def orphan?
    participants.count.zero?
  end

  def last_login
    participants.map(&:last_login).compact.max
  end

  def self.destroy_orphans
    logger.info 'Suppresion de tous les utilisateurs orphelins'
    logger.measure_info 'finalisation de la suppresion de tous les utilisateurs orphelins' do
      Parallel.each(User.all.select(&:orphan?), in_threads: Rails.configuration.x_option(:max_threads)) do |u|
        u.destroy
      end
    end
  end

  private

  # FIXME: on stocke le mot de passe en clair en attendant d'avoir un SSO
  def set_sso_clear_password
    self.sso_clear_password = password if password && !password.empty?
  end

  def update_folders
    folders.each do |folder|
      next if folder.name == name

      folder.update name:
    end
  end
end
