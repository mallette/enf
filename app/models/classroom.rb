class Classroom < ApplicationRecord
  validates :graf_id, presence: true, uniqueness: { case_sensitive: false }
  validates :code, presence: true

  belongs_to :school
  belongs_to :skill_block

  has_many :participants, dependent: :destroy
  has_many :skill_units, dependent: :destroy
  has_many :groups, dependent: :destroy
  has_many :domain_infos, dependent: :destroy
  has_many :teaching_units, dependent: :destroy
  has_many :teaching_sequences, through: :skill_units
  has_many :folders, dependent: :destroy
  has_many :shuttle_forms, through: :teaching_units

  after_create :link_with_all_modules
  after_create :create_automatic_groups
  after_create :create_root_folders

  def teachings = participants.where(participable_type: 'Teaching').map(&:participable)
  def learnings = participants.where(participable_type: 'Learning').map(&:participable)
  def tutorings = participants.where(participable_type: 'Tutoring').map(&:participable)
  def tutors = tutorings.map(&:tutor)
  def learners = learnings.map(&:user)
  def config_modules = domain_infos.map(&:config_module)

  def link_with_all_modules
    ConfigModule.where(active: true).each do |cm|
      instance = Instance.where(config_module_id: cm.id).last
      DomainInfo.create classroom: self, config_module: cm, instance: instance if instance
    end
  end

  def activate_all_modules(participant)
    config_modules.each do |cm|
      logger.info "Activation de #{cm.name} pour l'utilisateur <#{participant.user.graf_id}>"
      participant.enable!(cm)
    end
  end

  def trash_folder = folders.find_by(name: 'trash', automatic: true, parent: nil)
  def root_folder = folders.find_by(name: '/', automatic: true, parent: nil)
  def teachers_group = groups.find_by(automatic: :teachers)
  def tutors_group = groups.find_by(automatic: :tutors)
  def learners_group = groups.find_by(automatic: :learners)

  private

  def create_root_folders
    root_folder = folders.create name: '/',
                                 permissions: { r: :all },
                                 automatic: true
    folders.create! name: 'Notre formation',
                    permissions: { r: { u: [], g: [teachers_group.id, learners_group.id] } },
                    automatic: true,
                    parent: root_folder,
                    comment: "Il n'y a aucune unité pédagogique dans cette promo !"
    folders.create! name: 'Cadre général',
                    permissions: { r: :all, w: { u: [], g: [teachers_group.id] } },
                    automatic: true,
                    parent: root_folder
    folders.create! name: 'Espace individuel des stagiaires',
                    permissions: { r: { u: [],
                                       g: [teachers_group.id, tutors_group.id] } },
                    automatic: true,
                    parent: root_folder,
                    comment: "Il n'y a pas de stagiaire dans cette promo !"
    folders.create! name: 'Transversal',
                    permissions: { r: { u: [], g: [teachers_group.id, learners_group.id] },
                                  w: { u: [], g: [teachers_group.id, learners_group.id] } },
                    parent: root_folder,
                    automatic: true
    folders.create! name: 'trash', permissions: { r: :all }, automatic: true
  end

  def create_automatic_groups
    # TODO: i18n Formateurs et Stagiaires et Tuteurs
    groups.create name: 'Formateurs·rices', automatic: :teachers
    groups.create name: 'Stagiaires', automatic: :learners
    groups.create name: 'Tuteurs·rices', automatic: :tutors
  end
end
