# invitation between invitable(memo, visio, ...) and recipient (participant, group) or external_recipient (email)
class Invitation < ApplicationRecord
  belongs_to :invitable, polymorphic: true
  belongs_to :recipient, polymorphic: true, optional: true

  def recipient_human_name
    return external_recipient unless recipient_type

    recipient.name
  end

  def recipient_human_type
    return 'external' unless recipient_type

    recipient_type
  end

  def participant_invited?(participant)
    if recipient_type == 'Group'
      recipient.participants.include?(participant)
    elsif recipient_type == 'Participant'
      recipient == participant
    else
      false
    end
  end
end
