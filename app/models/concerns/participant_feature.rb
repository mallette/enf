module ParticipantFeature
  extend ActiveSupport::Concern

  include Invitable
  include Shareable
  include Ownable
  include Archivable
  include Bookmarkable
  include ParticipantUserIncludable
  include Sortable

  included do
    belongs_to :participant
    has_many :invitations, as: :invitable, dependent: :destroy

    scope :with_full, lambda { |participant, filtering: {}|
      with_invited(participant, filtering[:invited])
        .with_shared(participant, filtering[:shared])
        .with_owned(participant, filtering[:owned])
        .with_bookmarked(participant, filtering[:bookmarked])
        .with_archived(participant, filtering[:archived])
        .participant_user_included
    }
  end

  class_methods do
    def find_full(participant, id) = with_full(participant).find(id)
  end
end
