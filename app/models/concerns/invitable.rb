# This concern provides the scope `with_invited_shared_owned`
#   - a `participant`` is required
#   - optionally pass extra argument `filtering`` which may contain [true|false|nil]
module Invitable
  extend ActiveSupport::Concern

  included do
    ransacker :invited, &Arel::Factory.virtual_column(:invited)

    scope :with_invited, lambda { |participant, filtering = nil|
      invitable = arel_table
      invitation = Invitation.arel_table

      invited_by_participant = Arel::Nodes::Grouping.new(
        invitation[:recipient_type].eq(Participant.model_name.name)
                                        .and(invitation[:recipient_id].eq(participant.id))
      )

      invited_by_group = Arel::Nodes::Grouping.new(
        invitation[:recipient_type].eq(Group.model_name.name)
                                        .and(Arel::Nodes::In.new(
                                               invitation[:recipient_id],
                                               Group.select(:id)
                                               .joins(:groups_participants)
                                               .where(groups_participants: { participant: })
                                               .arel
                                             ))
      )

      invited_join = Arel::Nodes::OuterJoin.new(
        invitation,
        Arel::Nodes::On.new(invitation[:invitable_type].eq(name.classify))
        .and(invitation[:invitable_id].eq(invitable[:id]))
        .and(invited_by_participant.or(invited_by_group))
      )
      invited_or_mine = invited_arel_column.eq(true).or(invitable[:participant_id].eq(participant.id))

      query = select(invitable[Arel.star],
                     # TODO: try appending participant:user in order to sort by :participant_user_last_name
                     # see: [doc/ideas.md](idée pour corriger le pb de tri sur :participant_user_last_name)
                     invited_arel_column.as(:invited.to_s)) # virtual column entitled 'invited',
              .distinct
              .joins(invited_join)
              .where(invited_or_mine)

      filtering.nil? ? query : query.where(invited_arel_column.eq(filtering))
    }

    def invitable_index_i18n_key = "#{self.class.to_s.downcase.pluralize}.index_back"

    def invitable_index_path
      path_method = "#{self.class.to_s.downcase.pluralize}_path"
      eval("Rails.application.routes.url_helpers.#{path_method}", TOPLEVEL_BINDING, __FILE__, __LINE__) # rubocop:disable Security/Eval
    rescue StandardError
      raise "cannot infer route helper from a model which includes the Invitable module, to fix this issue either:
      - define a route called <#{path_method}> or
      - override a method named <invitable_index_path> in model: <#{self.class}>"
    end
  end

  class_methods do
    # virtual column useful for performing `where` conditions
    def invited_arel_column
      Arel::Nodes::Case.new
                       .when(Invitation.arel_table[:id].eq(nil))
                       .then(false)
                       .else(true)
    end
  end
end
