# This concern provides the scope `with_owned`
#   - a `participant`` is required
#   - optionally pass extra argument `filtering`` which may contain [true|false|nil]
module Ownable
  extend ActiveSupport::Concern

  included do
    belongs_to :participant

    ransacker :owned, &Arel::Factory.virtual_column(:owned)

    scope :with_owned, lambda { |participant, filtering = nil|
      invitable = arel_table
      query = select(invitable[Arel.star], owned_arel_column(participant.id).as(:owned.to_s))
      filtering.nil? ? query : query.where(owned_arel_column(participant.id).eq(filtering))
    }
  end

  class_methods do
    def owned_arel_column(participant_id)
      Arel::Nodes::Case.new
                       .when(arel_table[:participant_id].eq(participant_id))
                       .then(true)
                       .else(false)
    end
  end
end
