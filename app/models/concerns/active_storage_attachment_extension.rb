module ActiveStorageAttachmentExtension
  extend ActiveSupport::Concern
  include Bookmarkable
  included do
    alias_method :folder, :record
    audited only: %i[filename_audit content_audit record_id]
    serialize :permissions, type: Hash

    before_update :update_date

    def update_date = self.updated_at = DateTime.now

    def read?(participant)
      return folder.read?(participant) if permissions.empty?
      return true if permissions[:r] == :all
      return false unless permissions[:r]
      return true if permissions[:r][:u].include?(participant.id)

      groups_ids = participant.groups.where(classroom_id:).map(&:id)
      (groups_ids & permissions[:r][:g]).count.positive?
    end

    def write?(participant)
      return folder.write?(participant) if permissions.empty?
      return true if permissions[:w] == :all
      return true if permissions[:w][:u].include?(participant.id)

      groups_ids = participant.groups.where(classroom_id:).map(&:id)
      (groups_ids & permissions[:w][:g]).count.positive?
    end
  end
end
