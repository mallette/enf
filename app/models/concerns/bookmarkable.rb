# This concern provides the scope `with_bookmarked`
#   - a **participant** is required
#   - optionally pass an extra argument *filtering:* which may contain `[true|false|nil]`
module Bookmarkable
  extend ActiveSupport::Concern

  included do
    has_many :bookmarks, as: :bookmarkable, dependent: :destroy

    ransacker :bookmarked do
      Arel.sql(:bookmarked.to_s) # virtual column for performing `order` predicate
    end

    scope :with_bookmarked, lambda { |participant, filtering = nil|
      bookmarkable = arel_table
      bookmark = Bookmark.arel_table

      outer_join = Arel::Nodes::OuterJoin.new(
        bookmark,
        Arel::Nodes::On.new(bookmark[:bookmarkable_type].eq(name.classify))
        .and(bookmark[:bookmarkable_id].eq(bookmarkable[:id]))
        .and(bookmark[:participant_id].eq(participant.id))
      )

      query = joins(outer_join)
              .select(
                bookmarkable[Arel.star],
                bookmarked_arel_column.as(:bookmarked.to_s) # virtual column entitled 'bookmarked'
              )

      filtering.nil? ? query : query.where(bookmarked_arel_column.eq(filtering))
    }

    def bookmarked?(participant) = bookmarks.exists?(participant:)
    def toggle_bookmark(participant) = bookmarked?(participant) ? unbookmark(participant) : bookmark(participant)
    def bookmark(participant) = bookmarks.where(participant:).first_or_create
    def unbookmark(participant) = bookmarks.destroy_by(participant:)
  end

  class_methods do
    # virtual column useful for performing `where` conditions
    def bookmarked_arel_column
      Arel::Nodes::Case.new
                       .when(Bookmark.arel_table[:participant_id].eq(nil))
                       .then(false)
                       .else(true)
    end
  end
end
