# This concern provides the scope `with_shared`
#   - a `participant`` is required
#   - optionally pass extra argument `filtering`` which may contain [true|false|nil]
module Shareable
  extend ActiveSupport::Concern

  included do
    belongs_to :participant
    has_many :invitations, as: :invitable, dependent: :destroy

    ransacker :shared, &Arel::Factory.virtual_column(:shared)

    scope :with_shared, lambda { |participant, filtering = nil|
      invitable = arel_table
      invitation = Invitation.arel_table.alias

      shared_join = Arel::Nodes::OuterJoin.new(
        invitation,
        Arel::Nodes::On.new(invitation[:invitable_type].eq(name.classify))
        .and(invitation[:invitable_id].eq(invitable[:id]))
        .and(invitable[:participant_id].eq(participant.id))
      )

      query = joins(shared_join)
              .distinct
              .select(invitable[Arel.star],
                      shared_arel_column.as(:shared.to_s)) # virtual column entitled 'shared',

      filtering.nil? ? query : query.where(shared_arel_column.eq(filtering))
    }
  end

  class_methods do
    def shared_arel_column
      Arel::Nodes::Case.new
                       .when(Invitation.arel_table.alias[:id].eq(nil))
                       .then(false)
                       .else(true)
    end
  end
end
