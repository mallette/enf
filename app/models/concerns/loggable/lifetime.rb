# this concerns logs the lifetime (:create, :destroy) of this current ActiveRecord
module Loggable
  module Lifetime
    extend ActiveSupport::Concern

    included do
      after_create :log_lifetime_create
      after_destroy :log_lifetime_destroy

      private

      def log_lifetime_create = related_info :create
      def log_lifetime_destroy = related_info :destroy
      def related_info(action) = logger.info("#{action} <#{self}> related to #{belongings}")
      def belongings = _reflections.keys.map { |belonged| "<#{public_send(belonged)}>" }.join(' and ')
    end
  end
end
