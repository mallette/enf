# By including `Sortable`, you should probably declare a class variable `@default_sort`
#   which provides the default column to order by.
#
# For instance
# ```
#   class MyModel < ApplicationRecord
#     include Sortable
#     @default_sort='name' # or 'created_at desc', ...
# ```
module Sortable
  extend ActiveSupport::Concern

  DEFAULT_SORT_VARIABLE_NAME = '@default_sort'.freeze

  # FIXME: might be smarter with a **Functor**: Sortable(default_sort:'name')
  # instead of expecting

  included do
    def self.ransackable_attributes(_ = nil) = authorizable_ransackable_attributes
    def self.ransackable_associations(_ = nil) = authorizable_ransackable_associations
  end
end
