module Participable
  extend ActiveSupport::Concern
  included do
    has_one :participant, as: :participable, autosave: true
    def to_s = participant.to_s
  end
end
