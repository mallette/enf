module Archivable
  extend ActiveSupport::Concern
  included do
    has_many :archives, as: :archivable, dependent: :destroy

    ransacker :archived do
      Arel.sql(:archived.to_s) # virtual column for performing `order` predicate
    end

    scope :archived_yet, lambda { |archived|
      archived ? joins(:archives) : left_joins(:archives).where.missing(:archives)
    }

    scope :with_archived, lambda { |participant, filtering = nil|
      archivable = arel_table
      archive = Archive.arel_table

      outer_join = Arel::Nodes::OuterJoin.new(
        archive,
        Arel::Nodes::On.new(archive[:archivable_type].eq(name.classify))
        .and(archive[:archivable_id].eq(archivable[:id]))
        .and(archive[:participant_id].eq(participant.id))
      )

      query = joins(outer_join)
              .select(
                archivable[Arel.star],
                archived_arel_column.as(:archived.to_s) # virtual column entitled 'archived'
              )

      filtering.nil? ? query : query.where(archived_arel_column.eq(filtering))
    }

    def archived?(participant) = archives.exists?(participant:)
    def toggle_archive(participant) = archived?(participant) ? unarchive(participant) : archive(participant)
    def archive(participant) = archives.where(participant:).first_or_create
    def unarchive(participant)= archives.destroy_by(participant:)
  end

  class_methods do
    # virtual column useful for performing `where` conditions
    def archived_arel_column
      Arel::Nodes::Case.new
                       .when(Archive.arel_table[:participant_id].eq(nil))
                       .then(false)
                       .else(true)
    end
  end
end
