# This concern provides ordering facility through participant_user_...
module ParticipantUserIncludable
  extend ActiveSupport::Concern
  included do
    scope :participant_user_included, -> { joins(participant: :user).includes(participant: :user) }
  end
end
