class SkillWeek < ApplicationRecord
  belongs_to :skill_unit

  validates :week, uniqueness: { scope: :skill_unit }
end
