class Visio < ApplicationRecord
  include ParticipantFeature

  belongs_to :config_module

  before_create :prepare_greenligth_room

  validates :started_at, presence: true
  validates :name, presence: true

  @default_sort = 'name'

  # def self.ransackable_attributes(auth_object = nil)
  #   %w[name comment started_at number_of_participants duration]
  # end

  # def self.ransackable_associations(auth_object = nil)
  #   %w[participant]
  # end

  def prepare_greenligth_room
    logger.warn 'prepare_greenligth_room'
    session = get_session
    created_room = create_room(session, name)
    self.uuid = created_room['id']
    self.friendly_id = created_room['friendly_id']
  end

  def update(session, params)
    edit_room(session, friendly_id, params[:name]) if params[:name] != name
    super(params)
    send_configuration(session)
  end

  def destroy
    begin
      delete_room(get_session, friendly_id)
    rescue RestClient::NotFound, RestClient::NotAcceptable
      logger.error "Erreur lors de la suppression de la visio ayant pour friendly_id #{friendly_id}. Elle n'existe pas sur BBB."
    end
    super
  end

  def owner = user

  def domain_info
    DomainInfo.find_by(classroom: participant.classroom, config_module:)
  end

  # url de BigBlueButton
  def get_login_url
    domain_info.instance.url
  end

  def get_session
    signin_page = RestClient.get(
      "#{get_login_url}/signin",
      {
        user_agent: 'Zourit'
      }
    )
    csrf_token = Nokogiri::HTML(signin_page).at('meta[name="csrf-token"]')['content']
    cookie = signin_page.headers[:set_cookie][0]

    session = RestClient.post(
      "#{get_login_url}/api/v1/sessions.json",
      {
        session: {
          email: domain_info.instance.credential_username,
          password: domain_info.instance.credential_password,
          extend_session: false
        },
        token: ''
      }.to_json,
      {
        accept: 'application/json',
        content_type: 'application/json',
        cookie:,
        user_agent: 'Zourit',
        X_CSRF_TOKEN: csrf_token
      }
    )
    {
      cookie: session.headers[:set_cookie][0],
      csrf_token:,
      user_id: JSON.parse(session.body)['data']['id']
    }
  end

  def get_rooms_list(session)
    rooms_list = RestClient.get(
      "#{get_login_url}/api/v1/rooms.json",
      {
        accept: 'application/json',
        content_type: 'application/json',
        user_agent: 'Zourit',
        cookie: session[:cookie],
        X_CSRF_TOKEN: session[:csrf_token]
      }
    )
    JSON.parse(rooms_list.body)['data']
  end

  def create_room(session, room_name)
    created_room = RestClient.post(
      "#{get_login_url}/api/v1/rooms.json",
      {
        room: {
          name: room_name,
          user_id: session[:user_id]
        },
        user_id: session[:user_id]
      }.to_json,
      {
        accept: 'application/json',
        content_type: 'application/json',
        user_agent: 'Zourit',
        cookie: session[:cookie],
        X_CSRF_TOKEN: session[:csrf_token]
      }
    )
    JSON.parse(created_room.body)['data']
  end

  def edit_room(session, friendly_id, room_name)
    edited_room = RestClient.patch(
      "#{get_login_url}/api/v1/rooms/#{friendly_id}.json",
      {
        room: {
          name: room_name,
          user_id: session[:user_id]
        },
        user_id: session[:user_id]
      }.to_json,
      {
        accept: 'application/json',
        content_type: 'application/json',
        user_agent: 'Zourit',
        cookie: session[:cookie],
        X_CSRF_TOKEN: session[:csrf_token]
      }
    )
    JSON.parse(edited_room.body)['data']
  end

  def start_meeting(session, friendly_id, display_username)
    start_url = RestClient.post(
      "#{get_login_url}/api/v1/meetings/#{friendly_id}/start.json",
      { name: display_username }.to_json,
      {
        accept: 'application/json',
        content_type: 'application/json',
        referer: "http://#{Rails.application.routes.default_url_options[:host]}:#{Rails.application.routes.default_url_options[:port]}/end_visio",
        user_agent: 'Zourit',
        cookie: session[:cookie],
        X_CSRF_TOKEN: session[:csrf_token]
      }
    )
    start_url = JSON.parse(start_url.body)['data']
    # Return the URL after the first redirection if BBB is behind a Scalelite as a proxy
    begin
      RestClient::Request.execute(method: :get, url: start_url, max_redirects: 0)
    rescue RestClient::ExceptionWithResponse => e
      start_url = e.response.headers[:location]
    end
    start_url
  end

  def delete_room(session, friendly_id)
    RestClient.delete(
      "#{get_login_url}/api/v1/rooms/#{friendly_id}.json",
      {
        accept: 'application/json',
        content_type: 'application/json',
        user_agent: 'Zourit',
        cookie: session[:cookie],
        X_CSRF_TOKEN: session[:csrf_token]
      }
    )
  end

  def destroy_user(user, _meta_option)
    [Visio.where(user_id: user.id).destroy_all, '', '']
  end

  def full_url
    "#{get_login_url}/rooms/#{friendly_id}/join"
  end

  def get_room_settings(session, friendly_id)
    settings_data = RestClient.get(
      "#{get_login_url}/api/v1/room_settings/#{friendly_id}.json",
      {
        accept: 'application/json',
        content_type: 'application/json',
        user_agent: 'Zourit',
        cookie: session[:cookie],
        X_CSRF_TOKEN: session[:csrf_token]
      }
    )
    JSON.parse(settings_data.body)['data']
  end

  def update_room_settings(session, friendly_id, setting_name, setting_value)
    settings_data = RestClient.patch(
      "#{get_login_url}/api/v1/room_settings/#{friendly_id}.json",
      {
        settingName: setting_name,
        settingValue: setting_value
      }.to_json,
      {
        accept: 'application/json',
        content_type: 'application/json',
        user_agent: 'Zourit',
        cookie: session[:cookie],
        X_CSRF_TOKEN: session[:csrf_token]
      }
    )
    JSON.parse(settings_data.body)['data']
  end

  def send_configuration(session)
    if moderator_approval_previously_changed?
      logger.info "moderator_approval change to #{moderator_approval}"
      update_room_settings(session, friendly_id, 'guestPolicy',
                           moderator_approval)
    end
    if anyone_can_start_previously_changed?
      logger.info "anyone_can_start change to #{anyone_can_start}"
      update_room_settings(session, friendly_id, 'glAnyoneCanStart',
                           anyone_can_start)
    end
    if anyone_join_as_moderator_previously_changed?
      logger.info "anyone_join_as_moderator change to #{anyone_join_as_moderator}"
      update_room_settings(session, friendly_id, 'glAnyoneJoinAsModerator',
                           anyone_join_as_moderator)
    end
    if mute_on_start_previously_changed?
      logger.info "mute_on_start change to #{mute_on_start}"
      update_room_settings(session, friendly_id, 'muteOnStart',
                           mute_on_start)
    end
    if allow_recording_previously_changed?
      logger.info "allow_recording change to #{allow_recording}"
      update_room_settings(session, friendly_id, 'record',
                           allow_recording)
    end

    true
  end

  def get_room_recordings(session, friendly_id)
    recordings_data = RestClient.get(
      "#{get_login_url}/api/v1/rooms/#{friendly_id}/recordings.json",
      {
        accept: 'application/json',
        content_type: 'application/json',
        user_agent: 'Zourit',
        cookie: session[:cookie],
        X_CSRF_TOKEN: session[:csrf_token]
      }
    )
    JSON.parse(recordings_data.body)['data']
  end

  def delete_room_recording(session, record_id)
    RestClient.delete(
      "#{get_login_url}/api/v1/recordings/#{record_id}.json",
      {
        accept: 'application/json',
        content_type: 'application/json',
        user_agent: 'Zourit',
        cookie: session[:cookie],
        X_CSRF_TOKEN: session[:csrf_token]
      }
    )
  end

  def get_room_recordings_processing(session, friendly_id)
    recordings_data = RestClient.get(
      "#{get_login_url}/api/v1/rooms/#{friendly_id}/recordings_processing.json",
      {
        accept: 'application/json',
        content_type: 'application/json',
        user_agent: 'Zourit',
        cookie: session[:cookie],
        X_CSRF_TOKEN: session[:csrf_token]
      }
    )
    JSON.parse(recordings_data.body)['data']
  end
end
