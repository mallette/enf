class Link < ApplicationRecord
  include ParticipantUserIncludable
  include Sortable

  belongs_to :participant
  has_one :classroom, through: :participant

  validates :url, format: { with: URI::DEFAULT_PARSER.make_regexp(%w[http https ftp ftps]) }
  validates :title, presence: true
  validates :url, presence: true
  validates :color, presence: true
  validates :symbol, presence: true

  @default_sort = 'title'
end
