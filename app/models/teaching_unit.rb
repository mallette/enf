class TeachingUnit < ApplicationRecord
  belongs_to :classroom
  has_one :folder, dependent: :destroy
  has_many :shuttle_forms, dependent: :destroy
  has_many :teaching_sequences, dependent: :destroy

  validates :graf_id, presence: true, uniqueness: true
  after_update :update_folder
  after_create :create_folder
  after_create :create_shuttle_forms

  def update_folder
    folder.update name:
  end

  def create_folder
    parent = Folder.find_by(name: 'Notre formation', classroom_id:, automatic: true)
    teachers_group = Group.find_by(classroom:, automatic: :teachers)
    learners_group = Group.find_by(classroom:, automatic: :learners)
    if Folder.create name:, classroom_id: classroom.id, teaching_unit_id: id, parent_id: parent.id,
                     permissions: { r: { u: [], g: [teachers_group.id, learners_group.id] },
                                    w: { u: [], g: [teachers_group.id, learners_group.id] } }
      parent.update comment: nil
    end
  end

  def create_shuttle_forms
    classroom.learnings.each do |learning|
      logger.debug("creation d'une fiche navette pour le stagiaire <#{learning.user.name}>")
      ShuttleForm.create(learning:, teaching_unit: self)
    end
  end
end
