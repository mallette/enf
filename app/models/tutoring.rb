class Tutoring < ApplicationRecord
  include Participable

  has_and_belongs_to_many :learnings
  has_one :tutor, through: :participant, source: :user

  # retourne tous les formateurs + SON stagiaire - lui-même
  def involved_participants
    ids = participant.classroom.teachings.pluck(:participant) << learnings.map(&:participant)
    ids.uniq!
    ids.delete(participant)
    Participant.where(id: ids)
  end

  def involved_shuttle_forms
    participant.classroom.shuttle_forms
               .joins(learning: { participant: :user })
               .joins(:teaching_unit)
               .where('learning.participant': involved_participants)
               .order('teaching_unit.start_date DESC', 'users.last_name', 'users.first_name')
  end

  def participant_icon = 'people-roof'

  def fix_folders_permissions
    learnings.each do |learning|
      fixed_learner_folder = fix_learner_folder_permissions(learning)
      fix_learner_tutor_folder_permissions(fixed_learner_folder)
    end
  end

  def shuttle_form_permissions(shuttle_form)
    permissions = {}
    permissions[:learner_theory] = { r: shuttle_form.learner_status_index > 1,
                                     w: false }
    permissions[:teacher_theory] = { r: shuttle_form.teacher_status_index > 3,
                                     w: false }
    permissions[:learner_training] = { r: shuttle_form.learner_status_index > 5,
                                       w: false }
    permissions[:tutor_training] = { r: shuttle_form.tutor_status_index.positive?,
                                     w: shuttle_form.tutor_status_index == 7 }
    permissions[:teacher_training] = { r: shuttle_form.teacher_status_index > 9,
                                       w: false }
    permissions
  end

  private

  def fix_learner_folder_permissions(learning)
    return unless participant

    learner_folder = Folder.find_by(participant: learning.participant, classroom: participant.classroom,
                                    automatic: true)
    new_permissions = learner_folder.permissions
    new_permissions[:r][:u] << participant.id unless new_permissions[:r][:u].include?(participant.id)
    learner_folder.update permissions: new_permissions
    learner_folder
  end

  def fix_learner_tutor_folder_permissions(learner_folder)
    learner_tutor_folder = Folder.find_by(name: 'Stagiaire-Formateurs-Tuteurs', parent: learner_folder)
    return unless learner_tutor_folder

    new_permissions = learner_tutor_folder.permissions
    new_permissions[:r][:u] << participant.id unless new_permissions[:r][:u].include?(participant.id)
    new_permissions[:w][:u] << participant.id unless new_permissions[:w][:u].include?(participant.id)
    learner_tutor_folder.update permissions: new_permissions
  end
end
