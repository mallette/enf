class DomainInfo < ApplicationRecord
  belongs_to :config_module
  belongs_to :instance
  belongs_to :classroom
end
