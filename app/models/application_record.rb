class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  # returns a pretty name like: "MyModel#1"
  def to_s = "#{self.class.name}##{id}"
end
