class Pad < ApplicationRecord
  include ParticipantFeature

  belongs_to :config_module
  broadcasts_refreshes
  validates :name, presence: true

  @default_sort = 'name'
end
