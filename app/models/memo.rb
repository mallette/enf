class Memo < ApplicationRecord
  include ParticipantFeature

  before_create :add_uuid
  validates_presence_of :name

  @default_sort = 'name'

  def owner = user

  def external_url(classroom)
    mod = ConfigModule.where(name: 'Framemo').first
    url_server = mod.instance_for_classroom(classroom).url
    url_embedded = url_server
    url_embedded += '/' unless url_server[-1] == '/'
    url_embedded += uuid
    url_embedded
  end

  private

  def add_uuid = self.uuid = UUIDTools::UUID.random_create.to_s
end
