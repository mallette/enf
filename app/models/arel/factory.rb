module Arel
  module Factory
    # build a lamba virtual column, specially useful for one-line ransacker
    def self.virtual_column(symb) = ->(_) { Arel.sql(symb.to_s) }
  end
end
