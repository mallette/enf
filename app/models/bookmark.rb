class Bookmark < ApplicationRecord
  include Loggable::Lifetime

  belongs_to :bookmarkable, polymorphic: true, touch: true
  belongs_to :participant
end
