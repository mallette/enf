// Js Controllers
import "controllers"

// Turbo
import "@hotwired/turbo-rails"
Turbo.config.drive.progressBarDelay = 300
Turbo.config.forms.confirm = (title, element) => {
    let dialog = document.getElementById("turbo-confirm")
    dialog.querySelector("h2").textContent = title
    dialog.showModal()

    return new Promise((resolve, reject) => {
        dialog.addEventListener("close", () => {
            //if (dialog.returnValue == "confirm") { loading('Enregistrement en cours ...') }
            resolve(dialog.returnValue == "confirm")
        }, { once: true })
    })
}

// AlpineJs + turbo drive adapter
import 'alpine-turbo-drive-adapter'
import Alpine from 'alpinejs'
import Clipboard from "@ryangjchandler/alpine-clipboard"
Alpine.plugin(Clipboard)
window.Alpine = Alpine
Alpine.start()

// Chart.js with registerables
import { Chart, registerables } from 'chart.js'
Chart.register(...registerables)
window.Chart = Chart



