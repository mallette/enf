import { Application } from "@hotwired/stimulus"
const application = Application.start()

import PasswordVisibility from '@stimulus-components/password-visibility'
import Clipboard from '@stimulus-components/clipboard'
import Notification from '@stimulus-components/notification'
application.register('password-visibility', PasswordVisibility)
application.register('clipboard', Clipboard)
application.register('notification', Notification)

// Configure Stimulus development experience
const stimulusDebug = document.head.querySelector("meta[name=stimulus-debug]")
application.debug = stimulusDebug ? stimulusDebug.content === 'true' : false

window.Stimulus = application
export { application }
