module FontAwesomeHelper
  def icon(fa_style, fa_icon, **options) = fa_icon(fa_icon, fa_style, **options)

  def fa_icon(fa_icon, fa_style = 'fa-solid', **options)
    options[:class] = "fa-#{fa_icon} #{fa_style} #{options[:class]}".rstrip
    tag.i(**options)
  end
end
