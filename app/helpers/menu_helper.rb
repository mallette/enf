module MenuHelper
  def build_dropdown(toggle_name, label = '', icon = nil, origin = 'right', &)
    content = "
        <div class=\"relative inline-block text-left pr-3 -mr-3 \">
            <button type='button'
                    class='inline-flex h-full justify-center items-center gap-x-1.5 px-3 -mx-3 py-2 text-sm
                            font-semibold rounded-t-md text-gray-200 hover:text-white
                            border-gray-800 border-8 border-b-0 border-t-0'
                    @mouseover='#{toggle_name} = true'>
                <i class='fa fa-chevron-down transition' x-bind:class='#{toggle_name} ? \"rotate-180\" : \"\"'></i>
                #{icon('fa-solid', icon, class: 'text-2xl') if icon}
                <span class='text-md -pr-4'>#{label}</span>
            </button>
            <div
                x-show='#{toggle_name}'
                x-transition:enter='transition ease-out duration-100'
                x-transition:enter-start='transform opacity-0 scale-95'
                x-transition:enter-end='transform opacity-100 scale-100'
                x-transition:leave='transition ease-in duration-75'
                x-transition:leave-start='transform opacity-100 scale-100'
                x-transition:leave-end='transform opacity-0 scale-95'
                class='absolute #{origin}-0 z-10 w-56 origin-top-#{origin} divide-y-4 divide-gray-800
                    rounded-b-md text-gray-200 bg-gray-600
                    shadow-lg focus:outline-none border-gray-800 border-8 border-t-0'
                tabindex='-1'
                style='display:none;'>
                <div></div>
                #{capture(&)}
            </div>
        </div>
    "
    content_tag(:div,
                content.html_safe,
                class: 'flex items-center [&>div>button]:hover:bg-gray-900 h-full',
                '@mouseleave': "#{toggle_name} = false")
  end

  def build_dropdown_item(href, label, options = {})
    data = ''
    data = { turbo_method: options[:method] } if options[:method] != :get
    link_to href, data:,
                  class: 'block px-4 py-1 text-sm hover:bg-gray-800 hover:text-zourit-1',
                  target: options[:target] do
      content = ''
      content << "#{icon('fa-solid', options[:icon], class: 'fa-fw mr-1')} " if options[:icon]
      content << label
      content.html_safe
    end
  end

  def current_classrole_title
    if current_school_role
      classrole_title(current_school_role.role,
                      current_school_role.classroom.code)
    else
      ''
    end
  end

  def classrole_title(role, classroom_code)
    "#{t("shared.#{role}")} - #{classroom_code}"
  end
end
