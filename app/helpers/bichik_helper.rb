module BichikHelper
  def current_participant = Participant.find(session[:participant_id])
  def current_classroom = current_participant.classroom
  def current_timezone = current_user.time_zone || 'Paris'
  def string_to_bool(string) = string&.upcase == 'TRUE'
  def localtime(datetime) = I18n.l(datetime.in_time_zone(current_timezone))

  # when update form model fails, reuse its partial view and sibling turbo-frame
  # ```html.erb
  # <%= turbo_frame_tag :model_form, target: '_top' do %>
  #   <%= form_for(@model) do |f| %>
  #     ...
  #   <% end %>
  # <%end%>
  # ```
  def render_turbo_error(model, turbo_frame:, partial:)
    render turbo_stream: turbo_stream.replace(turbo_frame, partial:)
    logger.warn "Model <#{model}> contains #{model.errors.size} error(s)",
                model.errors.details.ai(ruby19_syntax: true, plain: true, multiline: false)
  end
end
