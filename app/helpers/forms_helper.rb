module FormsHelper
  EMAIL_LOCAL_PART_PATTERN = '^([\w+\-]\.?)+'.freeze
  PAGINATION_STEPS = [10, 25, 50, 100].freeze
  FIELD_STYLECLASS = 'bg-gray-100 border border-zourit-1 text-gray-800 text-sm rounded-r-lg
                                                   w-full pl-5 p-2 focus:border-zourit-1 focus:ring-1
                                                   focus:ring-zourit-1'.freeze
  ICON_STYLECLASS = 'h-auto rounded-l-lg bg-zourit-1 text-gray-100 text-lg p-2 w-8'.freeze

  def build_text_area(form, attribute, icon = 'align-left', **options)
    options_classize(options)
    placeholder = options_placeholderize(options, form, attribute)
    options[:rows] ||= 2
    icon_tag = build_field_icon(icon, title: placeholder)
    textarea = form.text_area(attribute, options)
    tag.div("#{icon_tag} #{textarea}".html_safe, class: "flex flex-row w-full #{options[:div_class]}")
  end

  def build_text_field(form, attribute, icon = 'pencil-alt', **options)
    options_classize(options)
    options_autofocusize(options)
    placeholder = options_placeholderize(options, form, attribute)
    icon_html = build_field_icon(icon, title: placeholder)
    textfield = form.text_field(attribute, options)
    tag.div("#{icon_html} #{textfield}".html_safe, class: 'flex flex-row')
  end

  # add extra javascript to put the caret at the end of the value
  def options_autofocusize(options)
    return unless options.include? :autofocus

    options[:onfocus] ||= 'this.setSelectionRange(this.value.length,this.value.length);'
  end

  def options_classize(options)
    options[:class] = "#{FIELD_STYLECLASS} #{options[:class]}"
  end

  def options_placeholderize(opt, form, attr) = opt[:placeholder] ||= form.object.class.human_attribute_name(attr)
  def build_field_icon(icon, title: nil) = icon('fa-solid', icon, title:, class: ICON_STYLECLASS)
  def email_local_part_pattern = EMAIL_LOCAL_PART_PATTERN

  ### -----------------------------------------
  ### TODO: OLD STUFF, need some refactoring...
  ### -----------------------------------------

  def build_th_order(column, label, previous_order)
    previous_column, previous_direction = previous_order&.split(',')
    if previous_column == column && previous_direction == 'DESC'
      direction = 'ASC'
      arrow = 'up'
    else
      direction = 'DESC'
      arrow = 'down'
    end
    taint = previous_column == column ? 'text-gray-100 dark:text-gray-800' : 'text-gray-400 dark:text-gray-600'
    "<span @click=$refs.orderInput.value='#{column},#{direction}';refresh() class='cursor-pointer #{taint} hover:text-gray-100'}>
    #{label}
    #{icon('fa-solid',
           "caret-#{arrow}",
           class: "cursor-pointer pl-1 #{taint}")}</span>".html_safe
  end

  def build_text_field_tag(input_name, value, input_placeholder, icon, required: false, readonly: false)
    "<div class='flex flex-row'>
      #{icon('fa-solid', icon, class: 'h-auto rounded-l-lg bg-zourit-1 text-gray-100 text-lg p-2 w-8',
                               title: input_placeholder)}
      #{text_field_tag input_name.to_sym, value, placeholder: input_placeholder,
                                                 required:,
                                                 readonly:,
                                                 class: "bg-gray-100 border border-zourit-1 text-gray-800 text-sm
                                                   rounded-r-lg w-full pl-5 p-2 focus:border-zourit-1 focus:ring-1
                                                   focus:ring-zourit-1"}
    </div>".html_safe
  end

  def build_complete_email_field(form, input_name, _value, input_placeholder, icon, required: false, readonly: false)
    "<div class='flex flex-row'>
      #{icon('fa-solid', icon, class: 'h-auto rounded-l-lg bg-zourit-1 text-gray-100 text-lg p-2 w-8',
                               title: input_placeholder)}
      #{form.email_field input_name.to_sym, placeholder: input_placeholder,
                                            required:,
                                            readonly:,
                                            class: "bg-gray-100 border border-zourit-1 text-gray-800 text-sm
                                                   rounded-r-lg w-full pl-5 p-2 focus:border-zourit-1 focus:ring-1
                                                   focus:ring-zourit-1"}
    </div>".html_safe
  end

  def build_complete_email_field_tag(input_name, value, input_placeholder, icon, required: false, readonly: false)
    "<div class='flex flex-row'>
      #{icon('fa-solid', icon, class: 'h-auto rounded-l-lg bg-zourit-1 text-gray-100 text-lg p-2 w-8',
                               title: input_placeholder)}
      #{email_field_tag input_name.to_sym, value, placeholder: input_placeholder,
                                                  required:,
                                                  readonly:,
                                                  class: "bg-gray-100 border border-zourit-1 text-gray-800 text-sm
                                                   rounded-r-lg w-full pl-5 p-2 focus:border-zourit-1 focus:ring-1
                                                   focus:ring-zourit-1"}
    </div>".html_safe
  end

  def build_email_field_tag(input_name, value, input_placeholder, icon, required, domain, options = {})
    "<div class='flex flex-row'>
      #{icon('fa-solid', icon, class: 'h-auto rounded-l-lg bg-zourit-1 text-gray-100 text-lg p-2 w-8')}
      #{text_field_tag input_name.to_sym, value, placeholder: input_placeholder,
                                                 required:,
                                                 autocapitalize: 'none',
                                                 autocomplete: options[:autocomplete],
                                                 pattern: '[a-z0-9._%+\-]{2,}$',
                                                 title: ts('shared.email_policy'),
                                                 class: "bg-gray-100 border border-zourit-1 text-gray-800 text-sm
                                                   w-full pl-5 p-2 focus:border-zourit-1 focus:ring-1
                                                   focus:ring-zourit-1"}
      <div class='rounded-r-lg bg-zourit-1 text-gray-100 text-md p-2 whitespace-nowrap'>@#{domain}</div>
    </div>".html_safe
  end

  def build_email_field(form, input_name, input_placeholder, icon, required, domain)
    "<div class='flex flex-row'>
      #{icon('fa-solid', icon, class: 'h-auto rounded-l-lg bg-zourit-1 text-gray-100 text-lg p-2 w-8')}
      #{form.text_field input_name.to_sym, placeholder: input_placeholder,
                                           required:,
                                           autocapitalize: 'none',
                                           pattern: '[a-z0-9._%+\-]{2,}$',
                                           title: ts('shared.email_policy'),
                                           class: "bg-gray-100 border border-zourit-1 text-gray-800 text-sm
                                                   w-full pl-5 p-2 focus:border-zourit-1 focus:ring-1
                                                   focus:ring-zourit-1"}
      <div class='rounded-r-lg bg-zourit-1 text-gray-100 text-md p-2 whitespace-nowrap'>@#{domain}</div>
    </div>".html_safe
  end

  def build_number_field(form, input_name, input_placeholder, icon, min = 0, max = nil, required: false)
    "<div class='flex flex-row'>
      #{icon('fa-solid', icon, class: 'h-auto rounded-l-lg bg-zourit-1 text-gray-100 text-lg p-2 w-8')}
      #{form.number_field input_name.to_sym, placeholder: input_placeholder,
                                             min:,
                                             max:,
                                             required:,
                                             class: 'bg-gray-100 border border-zourit-1 text-gray-800 text-sm
                                                     rounded-r-lg w-full pl-5 p-2 focus:border-zourit-1 focus:ring-1
                                                     focus:ring-zourit-1'}
    </div>".html_safe
  end

  def build_select_tag(name, value, collection, options = {})
    options[:class] ||=
      'pr-8 text-sm bg-gray-800 dark:bg-gray-200 text-gray-400 dark:text-gray-800 rounded-r-lg border-zourit-1 focus:border-zourit-1 focus:ring-1 focus:ring-zourit-1'
    options['@change'] = options.delete :on_change
    select_tag(name, options_for_select(collection, value), options)
  end

  def build_items_per_page_collection(max_items)
    collection = []
    PAGINATION_STEPS.each do |step|
      collection << [step.to_s, step.to_s] if step < max_items
    end
    collection << [I18n.t('pagination.all'), max_items.to_s]
    collection
  end

  def build_number_field_tag(input_name, input_placeholder, icon, options = {})
    "<div class='flex flex-row'>
      #{icon('fa-solid', icon, class: 'h-auto rounded-l-lg bg-zourit-1 text-gray-100 text-lg p-2 w-8')}
      #{number_field_tag input_name,
                         options[:value],
                         placeholder: input_placeholder,
                         min: options[:min],
                         max: options[:max],
                         required: options[:required],
                         '@input': options[:on_input],
                         data: options[:data],
                         class: 'bg-gray-100 border border-zourit-1 text-gray-800 text-sm
                                rounded-r-lg w-full pl-5 p-2 focus:border-zourit-1 focus:ring-1
                                focus:ring-zourit-1 ' + options[:class]}
    </div>".html_safe
  end

  def build_text_area_tag(text_area_name, content, icon, placeholder: nil, required: false)
    "<div class='flex flex-row w-full'>
      #{icon('fa-solid', icon, class: 'h-auto rounded-l-lg bg-zourit-1 text-gray-100 text-lg p-2 w-8')}
      #{text_area_tag text_area_name.to_sym, content,
                      placeholder:,
                      required:,
                      class: "bg-gray-100 border border-zourit-1 text-gray-800 text-sm
                                                      rounded-r-lg w-full pl-5 p-2 focus:border-zourit-1 focus:ring-1
                                                      focus:ring-zourit-1"}
    </div>".html_safe
  end

  def build_password_field(form, input_name, input_placeholder, icon, options = {})
    body = <<-BODY
    <div class="flex flex-col">
      <div x-data='{ show: false }' class='flex flex-row' data-controller="password-visibility">
        <div class='flex flex-row items-center h-auto rounded-l-lg bg-zourit-1 text-gray-100 text-lg p-2'>
          #{icon('fa-solid', icon, class: '')}
        </div>
        #{form.password_field input_name.to_sym, { placeholder: input_placeholder,
                                                   required: options[:required],
                                                   autocomplete: options[:autocomplete],
                                                   pattern: options[:no_policy] ? nil : Rails.configuration.x_option(:password_pattern),
                                                   title: ts('shared.password_policy'),
                                                   'data-password-visibility-target': 'input',
                                                   spellcheck: 'false',
                                                   class: 'bg-gray-100 border border-zourit-1 text-gray-800 text-sm
                                                      w-full pl-5 p-2 focus:border-zourit-1 focus:ring-1
                                                      focus:ring-zourit-1',
                                                   'x-bind:type': 'show ? \'text\' : \'password\'' }
        }
        <div class='flex flex-row1'>
          <button type="button" data-action="password-visibility#toggle" class='h-auto rounded-r-lg bg-zourit-1 text-gray-100 text-lg p-2 w-10' title='#{t('users.show_hide_password')}'>
            <span data-password-visibility-target="icon">#{fa_icon('eye')}</span>
            <span data-password-visibility-target="icon" class="hidden">#{fa_icon('eye-slash')}</span>
          </button>
        </div>
      </div>
        <div class='text-xs text-gray-600 dark:text-gray-200'>#{unless options[:no_policy]
                                                                  ts('shared.password_policy')
                                                                end}</div>
    </div>
    BODY

    body.html_safe
  end

  def build_password_field_tag(input_name, input_placeholder, icon, options = {})
    # options[:no_policy] = true for disable password policy, for login
    body = <<-BODY
    <div class="flex flex-col">
      <div x-data='{ show: false }' class='flex flex-row'>
        #{icon('fa-solid', icon, class: 'h-auto rounded-l-lg bg-zourit-1 text-gray-100 text-lg p-2 w-8')}
        #{password_field_tag input_name.to_sym, options[:value], { placeholder: input_placeholder,
                                                                   required: options[:required],
                                                                   autocomplete: options[:autocomplete],
                                                                   pattern: options[:no_policy] ? nil : Rails.configuration.x_option(:password_pattern),
                                                                   title: ts('shared.password_policy'),
                                                                   class: "bg-gray-100 border border-zourit-1 text-gray-800 text-sm
                                                                          w-full pl-5 p-2 focus:border-zourit-1 focus:ring-1
                                                                          focus:ring-zourit-1",
                                                                   'x-bind:type': 'show ? \'text\' : \'password\'' }
        }
        <div class='flex flex-row'>
          <div @click='show = !show' class='h-auto rounded-r-lg bg-zourit-1 text-gray-100 text-lg p-2 w-10'#{' '}
          title='#{t('users.show_hide_password')}' >
            <i x-show='!show' class='fa fa-eye' aria-hidden='true'></i>
            <i x-show='show' class='fa fa-eye-slash' aria-hidden='true'></i>
          </div>
        </div>
      </div>
      <div class='text-xs text-gray-600 dark:text-gray-200'>#{unless options[:no_policy]
                                                                ts('shared.password_policy')
                                                              end}</div>
    </div>
    BODY

    body.html_safe
  end

  def build_color_field(form, name, placeholder, icon, required: false)
    "<div class='flex flex-row h-12 items-center'>
      #{icon('fa-solid', icon, class: 'rounded-l-lg bg-zourit-1 text-gray-100 text-lg p-2 w-8 h-12 pt-3',
                               title: placeholder)}
      #{form.color_field name.to_sym, placeholder:,
                                      required:,
                                      class: 'bg-gray-100 border border-zourit-1 text-gray-800 text-sm rounded-r-lg
                                              w-full pl-5 p-2 focus:border-zourit-1 focus:ring-1
                                              focus:ring-zourit-1 h-12'}
    </div>".html_safe
  end

  def build_text_info(label, value)
    "<div class='flex flex-row'>
      <span class='h-auto rounded-l-lg bg-zourit-1 text-gray-100 text-sm p-2 whitespace-nowrap'>#{label}</span>
      <span class='flex bg-gray-100 border border-zourit-1 text-gray-800 text-sm rounded-r-lg w-full pl-5 p-2 justify-end'>
        #{value}
      </span>
    </div>".html_safe
  end

  def build_datetime_field(form_name, datetime_name, value, icon, required: false)
    "<div class='flex flex-row'>
      #{icon('fa-solid', icon, class: 'h-auto rounded-l-lg bg-zourit-1 text-gray-100 text-lg p-2 w-8')}
      <input type='datetime-local' id='#{form_name}_#{datetime_name}' name='#{form_name}[#{datetime_name}]'
             value='#{value&.strftime(Rails.configuration.x_option(:datetime_format))}'
             class='bg-gray-100 border border-zourit-1 text-gray-800 text-sm rounded-r-lg w-full pl-5
                    p-2 focus:border-zourit-1 focus:ring-1 focus:ring-zourit-1'
            #{'required' if required}/>
    </div>".html_safe
  end

  def build_datetime_field_with_label(form_name, datetime_name, value, options)
    body = <<-BODY
      <div class='flex flex-col'>
        <div><span class='bg-bichik-100 rounded-t-lg p-1 text-sm text-gray-100'>#{options[:label]}</span></div>
        <div class='flex flex-row'>
          #{icon('fa-solid', options[:icon], class: 'h-auto rounded-bl-lg bg-zourit-1 text-gray-100 text-lg p-2 w-8')}
          <input type='datetime-local' id='#{form_name}_#{datetime_name}' name='#{form_name}[#{datetime_name}]'
                value='#{value&.strftime(Rails.configuration.x_option(:datetime_format))}'
                class='bg-gray-100 border border-zourit-1 text-gray-800 text-sm rounded-r-lg w-full pl-5
                        p-2 focus:border-zourit-1 focus:ring-1 focus:ring-zourit-1'
                #{'required' if options[:required]} #{'readonly' if options[:readonly]}/>
        </div>
      </div>
    BODY

    body.html_safe
  end

  def build_datetime_field_tag(datetime_name, value, icon, required: false)
    "<div class='flex flex-row'>
      #{icon('fa-solid', icon, class: 'h-auto rounded-l-lg bg-zourit-1 text-gray-100 text-lg p-2 w-8')}
      <input type='datetime-local' id='#{datetime_name}' name='#{datetime_name}'
             value='#{value&.strftime(Rails.configuration.x_option(:datetime_format))}'
             class='bg-gray-100 border border-zourit-1 text-gray-800 text-sm rounded-r-lg w-full pl-5
                    p-2 focus:border-zourit-1 focus:ring-1 focus:ring-zourit-1'
            #{'required' if required}/>
    </div>".html_safe
  end

  def build_toggle_field_tag(toggle_name, label, default_checked, options = {})
    "<div>
      <label class='relative flex items-center cursor-pointer'>
        #{check_box_tag toggle_name,
                        default_checked,
                        default_checked,
                        class: 'sr-only peer pt-4',
                        onchange: "event.target.value = event.target.checked; #{options[:on_change]}",
                        data: options[:data],
                        'x-ref': options['x-ref'],
                        required: options[:required],
                        value: options[:value] || default_checked
        }
        <div class='w-11 h-6 #{options[:dark] ? 'bg-gray-600' : 'bg-gray-200'} peer-focus:outline-none rounded-full peer
            peer-checked:after:translate-x-full
            peer-checked:after:border-white after:content-[\"\"] after:absolute after:top-[2px]
            after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full
            after:h-5 after:w-5 after:transition-all peer-checked:bg-zourit-1'></div>
        <span class='ml-3 text-sm font-medium text-gray-900 dark:text-gray-200 whitespace-nowrap'>
          #{block_given? ? yield.html_safe : label}
        </span>
      </label>
    </div>".html_safe
  end

  def build_toggle_field(form, toggle_name, label, options = {})
    "<div class='items-end #{options[:class]}'>
      <label class='flex'>
        <div class='w-11 relative flex cursor-pointer'>
          #{form.check_box toggle_name,
                           class: 'sr-only peer pt-4',
                           '@change': options[:on_change],
                           data: options[:data]}
          <div class='w-11 h-6 #{options[:dark] ? 'bg-gray-600' : 'bg-gray-200'} peer-focus:outline-none rounded-full peer
              peer-checked:after:translate-x-full
              peer-checked:after:border-white after:content-[\"\"] after:absolute after:top-[2px]
              after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full
              after:h-5 after:w-5 after:transition-all peer-checked:bg-zourit-1'></div>
        </div>
        <span class='ml-3 text-sm font-medium text-gray-900 dark:text-gray-200'>#{label}</span>
      </label>
    </div>".html_safe
  end

  def build_form_submit(form, options)
    body = <<-BUTTON
    #{form.submit options[:text],
                  class: "rounded-lg bg-gray-800 text-gray-100 hover:bg-zourit-1 [&>i]:hover:text-gray-100
                          text-sm px-5 py-2 cursor-pointer transition transform ease-in-out duration-300 #{options[:class]}"}
    BUTTON

    body.html_safe
  end

  def build_button(options)
    body = <<-BODY
      <i class='fa fa-#{options[:icon]} mr-2' aria-hidden='true'></i>
      #{options[:text]}
    BODY

    opts = {}
    opts[:class] = 'rounded-lg bg-zourit-1 text-gray-100 hover:bg-zourit-2
    [&>i]:hover:text-gray-100 text-sm px-3 py-2 cursor-pointer transition transform ease-in-out duration-300'
    opts[:href] = options[:link] if options[:link]
    opts['x-on:click'] = options[:click] if options[:click]

    content_tag(options[:link] ? :a : :span, body.html_safe, opts)
  end

  def build_cancel_button(options)
    body = <<-BODY
      #{t('actions.cancel')}
    BODY

    opts = {}
    opts[:class] = 'rounded-lg bg-gray-200 text-gray-800 hover:bg-zourit-2
    hover:text-gray-100 text-sm px-5 py-2 cursor-pointer transition transform ease-in-out duration-300'
    opts[:href] = options[:link] if options[:link]
    opts['x-on:click'] = options[:click] if options[:click]

    content_tag(options[:link] ? :a : :span, body.html_safe, opts)
  end
end
