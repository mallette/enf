module ApplicationHelper
  include Pagy::Frontend

  PAGE_TITLE_HASH = {
    I18n.t('modules.jitsimeet.visiolist') => 'services.jitsimeet',
    I18n.t('modules.survey.title') => 'services.survey',
    I18n.t('modules.etherpad.padlist') => 'services.etherpad',
    I18n.t('modules.framemo.memolist') => 'services.framemo',
    I18n.t('modules.bigbluebutton.visiolist') => 'services.bigbluebutton'
  }.freeze

  def page_title(title) = PAGE_TITLE_HASH.key?(title) ? I18n.t(PAGE_TITLE_HASH[title]) : title
  def module_href(mod) = mod.url_local.to_s.empty? ? service_path(mod.id) : mod.url_local
  def module_icon(service_name) = ConfigModule.where(name: service_name)&.first&.font
  def repository_url = Rails.configuration.x_option(:repository_url)
  def repository_branch = Rails.configuration.x_option(:repository_branch)
  def repository_main_branch? = repository_branch == 'main'
  def repository_version = Rails.configuration.x_option(:repository_version)

  def repository_version_url
    if repository_main_branch?
      [repository_url, '-', 'tree', repository_version]
    else
      [repository_url, '-', 'commits', repository_branch, '?ref_type=heads']
    end.join('/')
  end

  def configmodule_name(module_object)
    raise 'should be a module object' unless module_object.instance_of?(Module)

    module_object.name.demodulize
  end

  def back_to(uri)
    content_tag(:a, href: uri, class: 'retour', title: I18n.t('actions.return')) do
      content_tag(:i, nil, class: %w[fa fa-arrow-left])
    end
  end
end
