module AuditsHelper
  def audit_sentence(audit)
    return audit_folder_sentence(audit) if audit.auditable.instance_of?(::Folder)

    audit_file_sentence(audit)
  end

  def audit_folder_sentence(audit)
    if audit.action == 'create'
      t :create, name: audit.audited_changes['name'], parent_name: parent_name(audit.audited_changes['parent_id']),
                 type: t(audit.auditable.class.name)

    elsif audit.action == 'update'
      update_sentence(audit)
    end
  end

  def update_sentence(audit)
    key, (_, new_value) = audit.audited_changes.first
    case key
    when 'name'
      t :update, field: t(key), name: new_value, type: t(audit.auditable.class.name)
    when 'filename_audit'
      t :file_renamed, new_name: new_value
    when 'content_audit'
      t :file_changed
    else
      parent_id = new_value
      if parent_id == current_classroom.trash_folder.id
        t :trash, type: t(audit.auditable.class.name)
      else
        t :move, parent_name: parent_name(parent_id), type: t(audit.auditable.class.name)
      end
    end
  end

  def audit_file_sentence(audit)
    if audit.action == 'create'
      t :create, name: audit.auditable.filename, parent_name: parent_name(audit.audited_changes['record_id']),
                 type: t(audit.auditable.class.name)
    elsif audit.action == 'update'
      update_sentence(audit)
    end
  end

  def parent_name(id)
    folder = Folder.find(id)
    return stringified_breadcrumb_for_participant(folder) if folder

    t :unknown_folder
  rescue StandardError
    t :unknown_folder
  end

  def audit_icon(audit)
    return audit.auditable.instance_of?(::Folder) ? 'folder' : 'file' if audit.action == 'create'

    key, (_, new_value) = audit.audited_changes.first
    case key
    when 'name', 'filename_audit'
      'pencil'
    when 'record_id', 'parent_id'
      if new_value == current_classroom.trash_folder.id
        'trash'
      else
        'folder-tree'
      end
    when 'content_audit'
      'file-pen'
    end
  end
end
