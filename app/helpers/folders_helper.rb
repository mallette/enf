module FoldersHelper
  def displayable_file?(file)
    return true if file.blob.image?
    return true if file.content_type.include?('pdf')
    return true if file.blob.video?
    return true if file.blob.audio?

    collabora_file?(file)
  end

  def human_filesize(number)
    number_to_human_size(number, locale: :fr, precision: 0) if number.positive?
  end

  def element_name(element) = element[:name] == 'files' ? element.filename : element[:name]

  def stringified_folder_name(folder)
    case folder
    when current_classroom.root_folder then I18n.t('folders.root')
    when current_classroom.trash_folder then I18n.t('folders.trash')
    else folder.name
    end
  end

  def stringified_breadcrumb_for_participant(folder, type: :file)
    breadcrumb = breadcrumb_for_participant(folder)
    breadcrumb.pop if type == :folder && breadcrumb.size > 1
    first = breadcrumb.first
    # first.name = '🖿' if first.name == '/'
    first.name = '📁' if first == current_classroom.root_folder
    first.name = '🗑️' if first == current_classroom.trash_folder
    breadcrumb.map(&:name).join(' / ')
  end

  def breadcrumb_for_participant(folder)
    paths = folder.path
    if current_participant.learning?
      found_learning_participant_index = paths.index { |f| f.participant_id == current_participant.id }
      return paths unless found_learning_participant_index

      paths[found_learning_participant_index].name = I18n.t('folders.special_link_for_learning')
      paths.delete_at(found_learning_participant_index - 1)
      paths
    else
      paths
    end
  end

  def collabora_file?(file)
    ['vnd.oasis.opendocument', 'vnd.openxmlformats-officedocument', 'application/xml'].map do |a|
      file.content_type.include?(a)
    end.any?
  end

  def icon_file(file)
    case file.filename.extension
    when 'odt' || 'doc' || 'docx'
      'file-alt'
    when 'ods' || 'xls' || 'xlsx'
      'file-invoice'
    when 'odp'
      'file-image'
    when 'csv'
      'file-csv'
    when 'zip' || 'rar' || 'tar.gz' || '7z'
      'file-archive'
    else 'file'
    end
  end

  def tree_collapse(folder)
    tree = "<li class='px-2 hover:bg-secondary-100'>"
    if folder[:children].count.positive?
      tree += "<div class='flex gap-1'>
                <a data-twe-collapse-init
                  class='flex items-center px-2 hover:bg-secondary-100 focus:text-primary active:text-primary cursor-pointer'
                  onclick='getElementById(\"#{folder[:id]}\").classList.toggle(\"hidden\");caret=getElementById(\"caret#{folder[:id]}\"); caret.classList.toggle(\"fa-caret-right\"); caret.classList.toggle(\"fa-caret-down\")'>
                #{icon 'fa-solid', 'caret-right', id: "caret#{folder[:id]}"}
                </a>"
      if folder[:write]
        tree += "<span id='span#{folder[:id]}' class='cursor-pointer' onclick='tree_select(\"#{folder[:id]}\")'>#{folder[:name]}</span>"
      else
        tree += "<span id='span#{folder[:id]}' class='text-gray-500'>#{folder[:name]}</span>"
      end
      tree += "</div><ul id='#{folder[:id]}' class='mr-2 hidden'>"
      folder[:children].each { |child| tree += tree_collapse(child) }
      tree += '</ul>'
    elsif folder[:write]
      tree += "<span id='span#{folder[:id]}' class='ml-6 cursor-pointer' onclick='tree_select(\"#{folder[:id]}\")'>#{folder[:name]}</span>"
    else
      tree += "<span id='span#{folder[:id]}' class='ml-6 text-gray-500'>#{folder[:name]}</span>"
    end
    tree += '</li>'
    tree.html_safe
  end

  def folder_footer(elements)
    folders = elements[:folders]
    content = []
    content << I18n.t('folders.total.folder_items', count: folders.size) if folders.size.positive?
    size = folders.sum { |element| element[:size] }
    if elements.include?(:files) && elements[:files].any?
      content << I18n.t('folders.total.file_items', count: elements[:files].size)
      size += elements[:files].sum { |element| element[:size] }
    end
    content = content.compact.join(' et ')
    content += " - #{number_to_human_size(size)}" if size.positive?
    content
  end
end
