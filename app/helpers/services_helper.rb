module ServicesHelper
  def zourit_image_tag(color, symbol, options = {})
    color = '#ad0c78' if color.blank?

    regex = /\Afa. fa-.*\z/
    symbol = 'link' if symbol.blank?
    symbol = "fas fa-#{symbol}" unless symbol.match?(regex)

    color1 = gradient_start(color.paint)
    color2 = gradient_end(color.paint)

    body = <<-EOF
    <svg>
      <defs>
            <linearGradient xlink:href="#gradient1-#{color}" id="gradient2-#{color}" x1="138" y1="390" x2="166" y2="376" gradientUnits="userSpaceOnUse" />
            <linearGradient id="gradient1-#{color}">
            <stop style="stop-color: #{color2}" offset="0" />
            <stop style="stop-color: #{color1}" offset="1" />
            </linearGradient>
            <linearGradient xlink:href="#gradient3-#{color}" id="gradient4-#{color}" x1="137" y1="378" x2="165" y2="385" gradientUnits="userSpaceOnUse" />
            <linearGradient id="gradient3-#{color}">
            <stop style="stop-color: #{color1}" offset="0" />
              <stop style="stop-color: #{color2}" offset="1" />
              </linearGradient>
      </defs>
      <g transform="scale(5) translate(-137,-368)" style="stroke-width:0.3">
        <path
          d="m 162.51976,369.35803 c -6.44094,-2.59878 -10.3949,5.72657 -14.46735,7.98048 -4.07245,2.2539 -9.88306,-0.30408 -9.96735,5.54295 -0.0843,5.84703 8.9044,5.2359 11.96448,7.19968 3.06009,1.96378 5.1078,7.86258 11.96449,7.19967 6.85668,-0.66291 -0.0966,-9.10871 0.25286,-13.96138 0.34946,-4.85267 6.69381,-11.36262 0.25287,-13.9614 z"
          style="fill: url(#gradient2-#{color}); fill-opacity:1;stroke-width:0.24318016" />
        <path
          d="m 138.94635,374.98678 c -1.91301,4.93909 2.87091,5.33191 3.76568,8.90446 0.89478,3.57254 -6.58822,11.17787 1.04074,12.50318 7.62897,1.32531 8.13449,-2.09944 11.8176,-2.89572 3.68311,-0.79629 9.0088,3.2426 10.16407,-2.03958 1.15527,-5.28218 -4.3067,-5.68817 -5.17253,-9.29714 -0.86581,-3.60896 3.54165,-9.99829 0.36611,-12.1105 -3.17555,-2.11221 -4.78229,2.16754 -10.21795,3.54339 -5.43566,1.37585 -9.8507,-3.54718 -11.76372,1.39191 z"
          style="fill: url(#gradient4-#{color}); fill-opacity:1;stroke-width:0.15127157" />
      </g>
    </svg>
    <i class="#{symbol}"></i>
    EOF

    options[:class] ||= ''
    options[:class] << ' zourit-icon'
    content_tag(:span, body.html_safe, options)
  end

  def service_bouton_theme(theme, name, color, font, desc, link, options = {})
    case theme
    when 'theme2'
      service_bouton_theme2(name, color, font, desc, link, options[:target])
    when 'theme3'
      service_bouton_theme3(name, color, font, desc, link, options[:target])
    when 'theme4'
      service_bouton_theme4(name, color, font, desc, link, options[:target])
    when 'theme5'
      service_bouton_theme5(name, color, font, desc, link, options[:target])
    when 'theme6'
      service_bouton_theme6(name, color, font, desc, link, options)
    when 'theme1'
      service_bouton_theme1(name, color, font, desc, link, options[:target])
    else
      service_bouton_theme7(name, color, font, desc, link, options)
    end
  end

  def service_bouton_theme1(name, color, font, desc, link, target)
    "<div class='text-center'>
      <div title=\"#{desc}\">
        <a href='#{link}' target='#{target}' class='flex flex-col items-center'>
          #{zourit_image_tag(color, font,
                             class: 'h-auto rotate-0 transition duration-300 ease-in-out hover:rotate-6
                                      hover:scale-110 [&~div]:hover:scale-125')}
          <div class='mb-12 text-md transition duration-300 ease-in-out dark:text-gray-200'>#{name}</div>
        </a>
      </div>
    </div>".html_safe
  end

  def service_bouton_theme2(name, color, font, desc, link, target)
    "<a href='#{link}' target='#{target}' class='bg-gray-200 flex flex-col items-center p-4 gap-2 rounded-lg hover:shadow-lg
                              transition duration-300 ease-in-out
                              hover:bg-gray-800 dark:hover:bg-gray-600 hover:text-gray-200'>
      #{icon('fa-solid', font ? font.gsub('fas fa-', '') : 'link',
             size: '3x',
             style: "color: #{color}",
             class: 'transition duration-300 ease-in-out')}
      <div class='font-semibold text-xl text-center dark:text-gray-800'>#{name}</div>
      <div class='text-justify dark:text-gray-800'>#{desc}</div>
    </a>".html_safe
  end

  def service_bouton_theme3(name, color, font, desc, link, target)
    "<a href='#{link}'
        target='#{target}'
        title=\"#{desc}\"
        class='flex flex-col items-center p-4 gap-2 rounded-lg hover:scale-110
              transition duration-300 ease-in-out'>
      <div class='flex w-20 justify-center rounded-full p-4' style='background-color: #{color};'>
        #{icon('fa-solid', font ? font.gsub('fas fa-', '') : 'link',
               size: '3x',
               class: 'text-white')}
      </div>
      <div class='font-semibold text-md dark:text-gray-200'>#{name}</div>
    </a>".html_safe
  end

  def service_bouton_theme4(name, color, font, desc, link, target)
    body = <<-BODY
    <a  href='#{link}'
        target='#{target}'
        title="#{desc}"
        class='bg-gradient-to-r from-gray-800 to-gray-500 flex flex-row justify-between items-center text-right p-4 gap-2 rounded-lg
              transition duration-300 ease-in-out hover:scale-110 text-gray-200'>
      #{icon('fa-solid', font ? font.gsub('fas fa-', '') : 'link',
             size: '3x',
             style: "color: #{color}",
             class: 'transition duration-300 ease-in-out')}
      <div class='font-semibold text-sm md:text-md'>#{name}</div>
    </a>
    BODY

    body.html_safe
  end

  def service_bouton_theme5(name, color, font, _desc, link, target)
    "<a href='#{link}' target='#{target}' class='bg-gray-200 flex flex-col items-center p-4 gap-2 rounded-lg hover:shadow-lg
                              transition duration-300 ease-in-out
                              hover:bg-gray-800 dark:hover:bg-gray-600 hover:text-gray-200'>
      #{icon('fa-solid', font ? font.gsub('fas fa-', '') : 'link',
             size: '3x',
             style: "color: #{color}",
             class: 'transition duration-300 ease-in-out')}
      <div class='font-semibold text-xl text-center dark:text-gray-800'>#{name}</div>
    </a>".html_safe
  end

  # theme David
  def service_bouton_theme6(name, color, font, desc, link, options = {})
    button = if options[:module_name]
               image_tag("david/#{options[:module_name]}.png", size: '150x150', class: '').to_s
             else
               "<div class='flex w-36 justify-center rounded-full p-12' style='background-color: #{color};'>
                  #{icon('fa-solid', font ? font.gsub('fas fa-', '') : 'link',
                         size: '3x',
                         class: 'text-white')}
                </div>"
             end

    "<div class='text-center'>
      <div title=\"#{desc}\">
        <a  href='#{link}'
            target='#{options[:target]}'
            class='flex flex-col items-center p-4 gap-2 rounded-lg hover:scale-150 transition duration-300 ease-in-out'>
          #{button}
          <div class='font-semibold text-md dark:text-gray-200 -mt-3'>#{name}</div>
        </a>
      </div>
    </div>".html_safe
  end

  # theme Bichik
  def service_bouton_theme7(name, color, font, desc, link, options = {})
    button = if options[:module_name]
               "<div class='flex w-20 justify-center rounded-full p-12 relative bg-zourit-6'>
                #{image_tag("bichik/#{options[:module_name]}.svg", size: '50x50',
                                                                   class: 'z-50 absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2')}
                </div>"
             else
               "<div class='flex w-20 justify-center rounded-full py-8 px-12' style='background-color: #{color}; text-shadow: 0 0 3px #000;'>
                  #{icon('fa-solid', font ? font.gsub('fas fa-', '') : 'link',
                         class: 'text-white fa-2x')}
                </div>"
             end

    "<div class='text-center'>
      <div title=\"#{desc}\">
        <a  href='#{link}'
            target='#{options[:target]}'
            class='flex flex-col items-center p-4 gap-2 rounded-lg hover:scale-125 transition duration-300 ease-in-out'>
          #{button}
          <div class='font-semibold text-md dark:text-gray-200 mt-0'>#{name}</div>
        </a>
      </div>
    </div>".html_safe
  end

  private

  def gradient_start(color)
    color.lighten(20)
  end

  def gradient_end(color)
    color.darken(20)
  end

  SERVICES_PAGE_TITLE_HASH = {
    'Forms' => 'services.forms',
    'Garradin' => 'services.garradin',
    'Nextcloud' => 'services.nextcloud',
    'Sympa' => 'services.sympa',
    'Framemo' => 'services.framemo',
    'Peertube' => 'services.peertube',
    'Benevalibre' => 'services.benevalibre',
    'JitsiMeet' => 'services.jitsimeet',
    'Etherpad' => 'services.etherpad',
    'Zimbra' => 'services.zimbra',
    'Dolibarr' => 'services.dolibarr',
    'BigBlueButton' => 'services.bigbluebutton'
  }

  def services_page_title(title)
    SERVICES_PAGE_TITLE_HASH.key?(title) ? I18n.t(SERVICES_PAGE_TITLE_HASH[title]) : title
  end
end
