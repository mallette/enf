module ButtonsBoxHelper
  ROUNDED = 'rounded-lg'.freeze
  ROUND_LEFT_ONLY = 'rounded-l-lg'.freeze
  ROUND_RIGHT_ONLY = 'rounded-r-lg'.freeze
  BG_NORMAL = 'bg-zourit-1 hover:bg-zourit-2'.freeze
  BG_CONFIRM = 'bg-zourit-7 hover:bg-red-500'.freeze
  BUTTON_STYLE = 'text-gray-100 hover:text-gray-100 p-2 text-sm ring-2 ring-inset ring-zourit-1 hover:ring-zourit-3'.freeze
  ICON_STYLE = 'w-4 text-center'.freeze

  def buttons_box(buttons = [])
    return unless buttons

    # Rk: below, `return` statements are mandatory for bypassing `Enumerable#tap` which actually returns self!
    buttons.tap do |first, *middle, last|
      return create_button(first, ROUNDED).html_safe if solo?(middle, last)

      html = [create_button(first, ROUND_LEFT_ONLY)]
      html << middle.map { |button| create_button(button) }
      html << create_button(last, ROUND_RIGHT_ONLY)
      return html.compact.join.html_safe
    end
  end

  private

  def solo?(middle, last) = middle.empty? && !last

  def create_button(options, rounded = nil)
    options[:href] ||= '#'
    options[:class] = [BUTTON_STYLE, rounded, confirm_style(options)]
                      .compact.join(' ')
    tag.a(**options) { fa_icon(options[:icon], class: ICON_STYLE) }
  end

  def confirm_style(options)
    if options.include?(:data_turbo_confirm) || options.dig(:data, :turbo_confirm)
      BG_CONFIRM
    else
      BG_NORMAL
    end
  end
end
