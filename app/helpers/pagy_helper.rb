module PagyHelper
  # preferable to sort_link, prepend page=1 as second args (options like) automatically
  def pagy_sort_link(search_object, attribute, *args, &)
    prepend_args_with_common_pagy_options(args)
    sort_link(search_object, attribute, *args, &)
  end

  # TODO: needs some refactoring
  def pagy_toggle_link(search_object, attribute, *args, &)
    # TODO: use => search_object.context.object.klass.human_attribute_name(attribute)

    params = preserve_q
    state = params.dig(:q, attribute).to_s == 'true'
    params[:q] = params[:q].merge(archived: !state)

    url = url_for(params:)

    # build_toggle_field_tag("q[#{attribute}]", t("actions.#{attribute}"), state, on_change: "Turbo.visit('#{url}')")
    build_toggle_field_tag("q[#{attribute}]", '', state, on_change: "Turbo.visit('#{url}')")
  end

  # TODO: needs some refactoring
  def pagy_search_field(search_object, attribute, *args)
    params = preserve_q
    value = params.dig(:q, attribute)

    args << {} unless args.last.is_a?(Hash)
    url = url_for(params:)
    url += url.include?('?') ? '&' : '?'
    url += "q[#{attribute}]="
    args.last.merge!({
                       onkeypress: "if (event.key === \"Enter\") {Turbo.visit('#{url}'+event.target.value)}",
                       class: 'rounded-full pl-9 text-sm w-full h-8 text-gray-800 dark:text-gray-400 bg-gray-200 dark:bg-gray-700'
                     })

    tag.div(class: 'relative') do
      concat icon('fa-solid', 'search', class: 'absolute text-gray-400 top-2 left-3')
      concat search_field_tag("q[#{attribute}]", value, *args)
    end
  end

  # preserve ransack search parameters from default parameter 'q'
  def preserve_q(search_object = nil)
    { q: request.params[:q] || {} }
  end

  # in case q exists and q[:sym] exists, its content is returned else nil
  def optional_q(sym) = request.params.fetch(:q, nil)&.fetch(sym, nil)

  private

  def prepend_args_with_common_pagy_options(args)
    args << {} unless args.last.is_a?(Hash)
    args.last.merge!(
      { page: 1,
        data: {
          'turbo-action': 'advance'
        } }
    )
  end
end
