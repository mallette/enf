class DailyJob < ApplicationJob
  include SemanticLogger::Loggable

  SUBTASK_CLASSES = [
    Subtasks::GrafImporter,
    Subtasks::ShuttleFormSchedulerTask
  ].freeze

  after_enqueue :log_scheduled_at
  after_perform :reschedule

  rescue_from(StandardError) do |e|
    logger.error(e)
    reschedule
  end

  def perform(asynchronous: true, notification: true)
    @asynchronous = asynchronous
    @notification = notification
    SUBTASK_CLASSES.each { |st| perform_subtask(st) }
  end

  def reschedule(force: false, verbose: true)
    return unless force || scheduled_at

    logger.level = :fatal unless verbose # to prevent logging
    self.class.set({ wait_until: (DateTime.now + 1.day).midnight + 4.hours, attempts: nil }).perform_later
    logger.level = :info unless verbose # re-enable default logging
  end

  private

  # a subtask must define the :run and :description methods
  def perform_subtask(subtask_class)
    return unless check_abstract_subtask(subtask_class)

    description = subtask_class.send :description
    logger.measure_info(description) do
      subtask_class.send :run, asynchronous: @asynchronous, notification: @notification
    rescue StandardError => e
      logger.error("an error has occured during: #{description}", e)
    end
  end

  def check_abstract_subtask(subtask_class)
    return true if subtask_class.include?(Subtasks::AbstractSubtask)

    logger.error("Subtask module #{subtask_class.name} must include Subtasks::AbstractSubtask and implement the 2 methods :run and :description")
    false
  end

  def log_scheduled_at = logger.info("next perform at: #{format_time(scheduled_at)}")
  def format_time(datetime) = datetime.strftime('%d/%m/%Y %H:%M')
end
