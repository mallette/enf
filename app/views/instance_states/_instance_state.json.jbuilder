json.extract! instance_state, :id, :title, :importance, :created_at, :updated_at
json.url instance_state_url(instance_state, format: :json)
