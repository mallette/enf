class VisiosController < BichikController
  include BookmarkableController

  before_action :bbb_authorized!
  before_action :set_pagy_visios, only: :index
  before_action :set_visio, except: %i[index new create]
  before_action :set_bbb_session, only: %i[update destroy url button_to_bbb]
  before_action :create_authorized!, only: %i[new create]
  before_action :modify_authorized!, only: %i[edit update destroy]

  def index; end

  def show
    # @visio_link = @visio.start_meeting(@session, @visio.friendly_id, current_participant.name)
    # rescue RestClient::NotAcceptable
    # redirect_to visios_path, notice: I18n.t('modules.bigbluebutton.visioconferencedoesnotexists')
  end

  def button_to_bbb
    @type_page = 'noheader'
    @visio_link = @visio.start_meeting(@session, @visio.friendly_id, current_participant.name)
  rescue StandardError
    @visio_link = nil
  end

  def new
    @visio = Visio.new(started_at: DateTime.now)
    @visio.allow_recording = true
  end

  def edit; end

  def create
    @visio = Visio.new(visio_params)
    @visio.participant = current_participant
    @visio.config_module = ConfigModule.find_by(name: 'BigBlueButton')
    @visio.started_at = @visio.started_at.in_time_zone(current_timezone)

    if @visio.save
      @visio.send_configuration(@visio.get_session)
      redirect_to @visio, notice: I18n.t('modules.bigbluebutton.successfulcreate', visio_name: @visio.name)
    else
      render :new
    end
  end

  def update
    visiop = visio_params
    visiop[:started_at] = visio_params[:started_at].in_time_zone(current_timezone)
    if @visio.update(@session, visiop)
      redirect_to @visio, notice: I18n.t('modules.bigbluebutton.successfulupdate', visio_name: @visio.name)
    else
      render :edit
    end
  end

  def destroy
    if @visio.destroy
      redirect_to visios_path, notice: I18n.t('modules.bigbluebutton.successfuldestroy')
    else
      redirect_to visios_path, error: I18n.t('modules.bigbluebutton.cannotdeletevisio')
    end
  end

  def toggle_archive
    @visio.toggle_archive(current_participant)
    redirect_to visios_path(q: q_params)
  end

  def toggle_bookmark
    @visio.toggle_bookmark(current_participant)
    redirect_to visios_path(q: q_params)
  end

  def url
    render json: { url: @visio.start_meeting(@session, @visio.friendly_id, current_participant.name) }
  end

  private

  def bbb_authorized! = config_module_authorized!('BigBlueButton')
  def create_authorized! = not_tutoring(visios_path)
  def modify_authorized! = only_owner!(@visio.participant, visios_path)

  def visio_params
    params.require(:visio).permit(:name, :comment, :started_at, :duration, :number_of_participants, :allow_recording,
                                  :moderator_approval, :anyone_can_start, :anyone_join_as_moderator, :mute_on_start)
  end

  def q_params = params.fetch(:q, {}).permit!

  def filtering
    filter = q_params.slice(:archived)
    filter[:archived] ||= false
    filter
  end

  def set_pagy_visios
    visios = Visio.with_full(current_participant, filtering:)
    @q = visios.ransack(default_bookmarkable_sort(q_params, Visio))
    @pagy, @visios = pagy(@q.result)
  end

  def set_visio
    @visio = Visio.find_full(current_participant, params[:id])
  end

  def set_bbb_session = @session = @visio.get_session
end
