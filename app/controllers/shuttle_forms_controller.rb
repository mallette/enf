class ShuttleFormsController < BichikController
  before_action :set_shuttle_form, only: %i[edit update]
  before_action :set_teaching_units, only: %i[index edit turbo_menu]
  before_action :set_menu_message, only: %i[index edit turbo_menu]

  def index; end

  def turbo_menu
    active_tu = TeachingUnit.find(params[:id])

    render turbo_stream: turbo_stream.replace('shuttle_form_menu_teacher', partial: 'menu_teacher',
                                                                           locals: { teaching_units: @teaching_units,
                                                                                     active_tu:,
                                                                                     message: @menu_message })
  end

  def edit; end

  def update
    params_to_update = shuttle_form_params.merge(signatures)
    if @shuttle_form.update(params_to_update)
      flash[:notice] = 'La fiche navette a été mise à jour.'
      render turbo_stream: [
        turbo_stream.replace('shuttle_form', partial: 'form', locals: { shuttle_form: @shuttle_form }),
        flash_stream
      ]
    else
      render :edit, status: :unprocessable_entity
    end
  end

  private

  def set_teaching_units
    @teaching_units = current_classroom.teaching_units.where('start_date<?', Date.today).order('start_date DESC')
  end

  def set_menu_message
    @menu_message = ''

    if current_classroom.learnings.count.zero?
      @menu_message = 'Aucun stagiaire dans cette promo'
    elsif @teaching_units.count.zero?
      first_teaching_unit = current_classroom.teaching_units.order(:start_date).first
      @menu_message = if first_teaching_unit
                        'La première fiche navette sera disponible à partir ' \
                          "du #{first_teaching_unit.start_date.strftime('%d/%m/%Y')}"
                      else
                        'Aucune unité pédagogique pour cette promo'
                      end
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_shuttle_form
    @shuttle_form = ShuttleForm.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def shuttle_form_params
    permitted = []
    permissions = current_participant.shuttle_form_permissions(@shuttle_form)
    if permissions[:learner_theory][:w]
      permitted += %i[learner_theory1 learner_theory2 learner_theory3 learner_theory4
                      learner_theory5]
    end
    if permissions[:learner_training][:w]
      permitted += %i[learner_training1 learner_training2 learner_training3
                      learner_training4 learner_training5 learner_training6]
    end
    if permissions[:tutor_training][:w]
      permitted += %i[tutor_training1 tutor_training2 tutor_training3 tutor_training4
                      tutor_training5 tutor_training6]
    end
    permitted << 'teacher_theory1' if permissions[:teacher_theory][:w]
    permitted << 'teacher_training1' if permissions[:teacher_training][:w]
    params.require(:shuttle_form).permit(permitted)
  end

  def signatures
    sign_params = {}
    if params['learner_theory_signature']
      sign_params[:learner_theory_signed_at] = DateTime.now
      sign_params[:learner_theory_signed] = true
      sign_params[:learner_status] = :learner_theory_signed
    end
    if params['teacher_theory_signature']
      sign_params[:teacher_theory_signed_at] = DateTime.now
      sign_params[:teacher_theory_signed_by_id] = current_user.id
      sign_params[:teacher_status] = :teacher_theory_signed
    end
    if params['learner_training_signature']
      sign_params[:learner_training_signed_at] = DateTime.now
      sign_params[:learner_training_signed] = true
      sign_params[:learner_status] = :learner_training_signed
    end
    if params['teacher_training_signature']
      sign_params[:teacher_training_signed_at] = DateTime.now
      sign_params[:teacher_training_signed_by_id] = current_user.id
      sign_params[:teacher_status] = :teacher_training_signed
    end
    if params['tutor_training_signature']
      sign_params[:tutor_training_signed_at] = DateTime.now
      sign_params[:tutor_training_signed_by_id] = current_user.id
      sign_params[:tutor_status] = :tutor_signed
    end

    sign_params
  end
end
