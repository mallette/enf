class ClassroomsController < BichikController
  before_action :set_classroom,
                only: %w[show enable_module_for_participant disable_module_for_participant edit_user new_user teachers
                         learners]

  def index; end

  def show
    learners
    teachers
    tutors
  end

  def teachers
    @qt = @classroom.participants.where(participable_type: 'Teaching').ransack(params[:teachers],
                                                                               search_key: :teachers)
    @qt.sorts = 'users.last_name asc' if @qt.sorts.empty?
    scope = @qt.result.includes(:user)

    @pagy_teachers, @teacher_users = pagy(scope)
  end

  def learners
    @ql = @classroom.participants.where(participable_type: 'Learning').ransack(params[:learners],
                                                                               search_key: :learners)
    @ql.sorts = 'users.last_name asc' if @ql.sorts.empty?
    scope = @ql.result.includes(:user)

    @pagy_learners, @learner_users = pagy(scope)
  end

  def tutors
    @qtut = @classroom.participants.where(participable_type: 'Tutoring').ransack(params[:tutors], search_key: :learners)
    @qtut.sorts = 'users.last_name asc' if @ql.sorts.empty?
    scope = @qtut.result.includes(:user)

    @pagy_tutors, @tutor_users = pagy(scope)
  end

  def enable_module_for_participant
    participant = Participant.find(params[:participant_id])
    cm = ConfigModule.find(params[:module_id])

    participant.enable!(cm)
    redirect_to classroom_path(@classroom), notice: 'Module ajouté'
  end

  def disable_module_for_participant
    participant = Participant.find(params[:participant_id])
    cm = ConfigModule.find(params[:module_id])

    participant.disable!(cm)
    redirect_to classroom_path(@classroom), notice: 'Module retiré'
  end

  def edit_user
    logger.info("edit domain #{params[:classroom_id]}, user #{params[:user_id]}")
    @user = User.find(params[:user_id])
    @div_options_ids = {}
    @classroom.config_modules.each do |mod|
      @div_options_ids[mod.id] = @user.enabled?(mod, @classroom)
    end
  end

  def new_user
    @user = User.new
    @div_options_ids = {}
    @classroom.config_modules.each do |mod|
      @div_options_ids[mod.id] = true
    end
  end

  def update_user
    user = User.find(params[:user_id])
    res = update_user_helper(user, params)
    if res[:result]
      redirect_to domain_path(user.domain.id), flash: { notice: res[:message] }
    else
      redirect_to edit_domain_user_path(user.domain.id, user.id), flash: { alert: res[:message] }
    end
  end

  def create_user
    res = create_user_helper(params, @domain)
    if res[:result]
      redirect_to domain_path(@domain.id), flash: { success: res[:message] }
    else
      redirect_to new_domain_user_path(@domain.id), flash: { alert: res[:message] }
    end
  end

  def delete_user
    user = User.find(params[:user_id])
    domain = user.domain
    if user.destroy
      redirect_to domain_path(domain.id), flash: { success: I18n.t('domains.delete_user.success', email: user.email) }
    else
      redirect_to domain_path(domain.id), flash: { alert: I18n.t('domains.delete_user.failure', email: user.email) }
    end
  end

  private

  def set_classroom
    @classroom = current_classroom
  end

  def index_teachers_params
    params[:teachers]&.permit(:order, :query, :page, :items_per_page)
  end

  def index_learners_params
    params[:learners]&.permit(:order, :query, :page, :items_per_page)
  end
end
