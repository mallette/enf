class ServicesController < BichikController
  # GET /services
  def index
    @type_page = current_user ? 'home' : 'login'
    @url = params[:url]
    @links = Link.joins(:participant).where(participant: { classroom: current_classroom }, global: true)
    @userlinks = Link.where(participant: current_participant, global: false)
  end

  # GET /services/1
  def show
    @type_page = 'iframe'
    id = params[:id]
    config_module = begin
      ConfigModule.find(id)
    rescue StandardError
      logger.warn("no ConfigModule found for id <#{id}>, keep going anyway!")
      nil
    end

    redirect_to services_path, notice: (I18n.t 'services.noactif').to_s unless config_module

    unless current_user.participant(current_classroom).enabled?(config_module)
      redirect_to services_path,
                  notice: (I18n.t 'services.noallowed').to_s
    end

    domaininfo = DomainInfo.where(config_module:, classroom_id: current_classroom.id).first

    redirect_to services_path, notice: (I18n.t 'services.nomodule').to_s unless domaininfo&.instance_id

    @url_embedded = domaininfo.instance.url

    # FIXME: L'url du dossier est en dur pour nextcloud. Il faudrait le passer de manière plus générique si on ajoute de nouveau services externe
    @url_embedded += "/index.php/apps/files/files?dir=/#{current_classroom.code}" if config_module.name == 'Nextcloud'
    @title = config_module.name

    log_statistics(config_module.name, domaininfo.instance.name)
  end

  private

  # TODO: améliorer le eval avec binding plus tard!
  def evaluate_config_module(name, _service_context)
    eval("Core::#{name}::Main.get_login_url(_service_context)")
  end
end
