class WopiController < ApplicationController
  skip_before_action :verify_authenticity_token # authenticity replaced by participant stored in params[:access_token]!
  before_action :set_participant
  before_action :set_file
  before_action :folder_readable!
  before_action :folder_writable!, only: :update_filecontent

  def file
    render json: { BaseFileName: @file.filename,
                   Size: @file.blob.byte_size,
                   UserCanWrite: @file.folder.write?(@participant),
                   UserId: @participant.id,
                   UserFriendlyName: @participant.name,
                   UserCanNotWriteRelative: true }
  end

  def filecontent = render plain: @file.blob.download

  def update_filecontent
    @file.update content_audit: DateTime.now
    @file.blob.upload request.body
    @file.blob.analyze
    @file.audits.last.update user_id: @participant.id, user_type: 'Participant'
    render plain: ''
  end

  private

  def set_participant = @participant = Participant.find_by(token: params[:access_token])

  def set_file
    @file = ActiveStorage::Attachment.find(params[:id])
    @file if @file.folder.read?(@participant)
  end

  def folder_readable!
    head :forbidden unless @file.folder.read?(@participant)
  end

  def folder_writable!
    head :forbidden unless @file.folder.write?(@participant)
  end
end
