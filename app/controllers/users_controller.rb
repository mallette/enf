class UsersController < BichikController
  def edit
    if current_user.id != params[:id].to_i
      redirect_to services_path, notice: (ts 'application.law')
      return
    end

    @user = current_user
    @user.time_zone = 'Paris' if @user.time_zone.nil?
  end

  def update
    # uniquement pour l'utilisateur courrant
    redirect_to services_path, notice: (I18n.t 'application.law') and return unless current_user.id == params[:id].to_i

    @user = User.find(params[:id])

    if params[:user][:password] != params[:user][:password_confirmation]
      redirect_to user_path(@user.id),
                  alert: (I18n.t 'users.problem_password') and return
    end

    password_pattern = Rails.configuration.x_option(:password_pattern)
    unless params[:user][:password].empty? || params[:user][:password] =~ password_pattern
      redirect_to user_path(@user.id),
                  alert: (I18n.t 'users.problem_password_pattern') and return
    end

    if @user.update(user_params)
      redirect_to services_path, flash: { notice: t('.noticeupdate') }
    else
      redirect_to user_path(@user.id), flash: { alert: t('.notice_no_update') }
    end
  end

  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation, :email, :first_name, :last_name, :theme,
                                 :dark_mode, :time_zone)
  end
end
