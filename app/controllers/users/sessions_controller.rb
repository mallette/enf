module Users
  class SessionsController < Devise::SessionsController
    include Semantic::AnsiColors

    after_action :set_main_session_tag, only: [:create]

    def new
      @type_page = 'login'
      super
    end

    def create
      super
      SemanticLogger.tagged(current_user.graf_id) do
        logger.info("#{current_user} has successfully signed in by using method :#{colorize(login_type, BOLD)}",
                    current_user_info)
      end
    end

    private

    def set_main_session_tag = session[main_session_tag] = current_user.graf_id
    def main_session_tag = Rails.application.config.x.action_controller.main_session_tag
    def login_type = signed_in_with_email? ? :email : :graf_id
    def signed_in_with_email? = params.dig(:user, :login).include?('@')

    def current_user_info
      {
        fullname: current_user.name,
        email: current_user.email,
        graf_id: current_user.graf_id
      }
    end
  end
end
