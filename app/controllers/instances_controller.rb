# frozen_string_literal: true

class InstancesController < ApplicationController
  before_action :set_config_module
  before_action :set_instance, only: %i[show edit update destroy]
  before_action :set_instance_options, only: %i[show edit new]
  before_action :super_admin_only

  # GET /instances
  # GET /instances.json
  def index
    @instances = Instance.all
  end

  # GET /instances/1
  # GET /instances/1.json
  def show; end

  # GET /instances/new
  def new
    @instance = Instance.new
    @instance.config_module = @config_module
    @instance.name = @config_module.name
    @instance.description = @config_module.resume
  end

  # GET /instances/1/edit
  def edit; end

  # POST /instances
  # POST /instances.json
  def create
    @instance = Instance.new(instance_params)
    @instance.config_module = @config_module

    respond_to do |format|
      if @instance.save
        save_instance_options(params['meta']) if params['meta']

        format.html do
          redirect_to config_module_path(@config_module),
                      notice: (I18n.t 'instance_stories.noticecreate').to_s
        end
        format.json { render :show, status: :created, location: @instance }
      else
        logger.warn @instance.errors.inspect
        format.html { render :new }
        format.json { render json: @instance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /instances/1
  def update
    if @instance.update(instance_params)
      redirect_to config_module_path(@config_module), flash: { notice: t('.noticeupdate') }
    else
      render :edit
    end
  end

  # DELETE /instances/1
  def destroy
    @instance.destroy
    redirect_to config_module_path(params[:config_module_id]), flash: { notice: t('.noticedestroy') }
  end

  private

  def set_instance_options
    @instance_meta_options = {}
    InstanceOption.where(instance_id: @instance).each do |io|
      @instance_meta_options[io.meta_option_id] = io.value
    end
  end

  def save_instance_options(meta_params)
    # delete previous instance_options
    InstanceOption.where(instance_id: @instance.id).delete_all

    # then, create new ones
    meta_params.each do |key, value|
      InstanceOption.create(
        value:,
        instance_id: @instance.id,
        meta_option_id: key
      )
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_instance
    @instance = Instance.find(params[:id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_config_module
    @config_module = ConfigModule.find(params[:config_module_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def instance_params
    params.require(:instance).permit(:config_module_id, :name, :description, :active, :datas, :url,
                                     :credential_username, :credential_password)
  end
end
