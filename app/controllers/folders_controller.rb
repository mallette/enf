class FoldersController < BichikController
  include FoldersHelper

  before_action :set_folder, except: %i[index create tree]
  def index
    @folder = current_classroom.root_folder
    show
    return unless current_participant.learning?

    participant_folder = current_classroom.folders.with_bookmarked(current_participant).find_by(participant: current_participant)
    folders = prepare_elements_from_folders([participant_folder], name: I18n.t('folders.special_link_for_learning'))
    folders += @elements[:folders]
    @elements = { folders:, files: [] }
  end

  def show
    redirect_to(folders_path, alert: 'Erreur sur le dossier') && return unless @folder

    unless @folder.read?(current_participant)
      redirect_to(@folder.parent_id ? @folder.parent : folders_path, alert: 'Pas le droit') && return
    end

    @is_trash = @folder.trash?
    @in_trash = @folder.in_trash?
    @order = params[:order] || 'name'
    @direction = params[:direction] || 'ASC'

    folders = prepare_elements_from_folders(
      Folder.with_bookmarked(current_participant)
                    .where(parent_id: @folder.id)
                    .order("bookmarked DESC, #{@order == 'size' ? 'name' : @order} #{@direction}")
    )

    files = prepare_elements_from_files(fetch_files)
    @elements = { folders:, files: }
  end

  def new
    @folder = Folder.new
  end

  def edit; end

  def details
    @element = @folder
    @type = :folder
  end

  def file_details
    @element = @folder.files.find(params[:file_id])
    @type = :file
    render :details
  end

  def create
    if folder_params[:parent_id]
      parent = Folder.find(folder_params[:parent_id])
      unless parent.write?(current_participant)
        redirect_to parent, alert: "Pas le droit d'écriture"
        return false
      end
    end
    @folder = Folder.new(folder_params)
    @folder.classroom_id = current_classroom.id

    if @folder.save
      redirect_to parent, notice: I18n.t('folders.created')
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if folder_params[:name] && folder_params[:name].empty?
      redirect_to @folder, alert: I18n.t('folders.cant_be_empty') && return
    end

    if @folder.write?(current_participant)
      previous_parent = @folder.parent
      if @folder.update(folder_params)
        redirect_to params[:redirect_to_parent] ? @folder.parent : @folder,
                    notice: I18n.t('folders.updated'),
                    status: :see_other
      else
        logger.warn("#{@folder} could not be moved to #{@folder.parent} because loop cycle has been detected!")
        redirect_to previous_parent, alert: I18n.t('folders.loop_cycle_detection')
      end
    else
      redirect_to @folder, alert: I18n.t('folders.no_write_rigth')
    end
  end

  def destroy
    parent = @folder.parent
    if @folder.destroyable?(current_participant)
      if @folder.in_trash?
        @folder.destroy
      else
        @folder.update deleted: true, parent: current_classroom.trash_folder, permissions: @folder.recursive_permissions
      end
      redirect_to parent, notice: 'Folder was successfully destroyed.', status: :see_other
    else
      redirect_to parent, alert: 'Pas le droit.'
    end
  end

  def file
    @file = @folder.files.find(params[:file_id])
    return unless collabora_file?(@file)

    # TODO: ne plus avoir l'url de collabora en dur
    @url = "https://lool.zourit.net/browser/baa6eef/cool.html?WOPISrc=#{request.base_url}/wopi/files/#{@file.id}&lang=fr&access_token=#{current_participant.generate_or_get_token}"
  end

  def rename_file
    file = @folder.files.find(params[:file_id])
    new_filename = params[:file][:name]
    file.update filename_audit: new_filename, filename: new_filename if new_filename
    @folder.check_filenames
    redirect_to @folder
  end

  def move_or_copy_file
    redirect_to(@folder, alert: 'pas le droit') && return unless @folder.write?(current_participant)

    file = @folder.files.find(params[:file_id])
    new_parent = Folder.find(params[:folder][:parent_id])
    redirect_to(@folder, alert: 'pas le droit') && return unless new_parent.write?(current_participant)

    if params[:commit] == 'Copier'
      copy_file file, new_parent
    else
      move_file file, new_parent
    end
  end

  def move_file(file, new_parent)
    file.update record_id: new_parent.id
    redirect_to @folder, notice: 'Fichier déplacé'
  end

  def copy_file(file, new_parent)
    new_parent.files.attach(io: StringIO.new(file.download),
                            filename: file.filename,
                            content_type: file.content_type)
    redirect_to @folder, notice: 'Fichier copié'
  end

  def move_or_copy_folder
    new_parent = Folder.find(params[:folder][:parent_id])
    if params[:commit] == 'Copier'
      @folder.copy_to(new_parent)
      redirect_to new_parent, notice: 'Dossier copié'
    else
      update
    end
  end

  def delete_file
    file = @folder.files.find(params[:file_id])

    if @folder.trash?
      redirect_to @folder && return unless file.write?(current_participant)

      file.destroy
    else
      redirect_to @folder && return unless @folder.write?(current_participant)

      file.update deleted: true, classroom_id: current_classroom.id, permissions: @folder.recursive_permissions,
                  record_id: current_classroom.trash_folder.id
    end
    redirect_to @folder
  end

  def delete_elements
    folders = Folder.find(params[:folders].split('|'))
    files = ActiveStorage::Attachment.find(params[:files].split('|'))

    folders.each do |folder|
      next unless folder.destroyable?(current_participant)

      if folder.in_trash?
        folder.destroy
      else
        folder.update deleted: true, parent: current_classroom.trash_folder,
                      permissions: folder.recursive_permissions
      end
    end

    files.each do |file|
      next unless file.write?(current_participant)

      if @folder.trash?
        file.destroy
      else
        file.update deleted: true, classroom_id: current_classroom.id, permissions: @folder.recursive_permissions,
                    record_id: current_classroom.trash_folder.id
      end
    end

    redirect_to @folder
  end

  def bookmark
    @folder.bookmarked?(current_participant) ? @folder.unbookmark(current_participant) : @folder.bookmark(current_participant)
    redirect_to @folder.parent == current_classroom.root_folder ? folders_path : @folder.parent
  end

  def bookmark_file
    file = @folder.files.with_bookmarked(current_participant).find(params[:file_id])
    file.bookmarked?(current_participant) ? file.unbookmark(current_participant) : file.bookmark(current_participant)
    redirect_to @folder
  end

  def create_blank_file
    name = params[:file][:name]
    content_type = params[:file][:ext]
    filename = "#{name}.#{content_type}"
    @folder.files.attach(io: StringIO.new, filename:, content_type:)

    redirect_to @folder
  end

  def search
    @search_query = params[:search_query]
    redirect_to(folder_path(@folder)) && return unless @search_query.present?

    @is_trash = @folder ? @folder.trash? : false
    @folders = @folder.search_children(@search_query, current_participant)
    @files = @folder.search_files(@search_query, current_participant)
  end

  def tree
    tree = Folder.tree(current_classroom, current_participant)
    render turbo_stream: turbo_stream.replace('tree', partial: 'tree', locals: { tree: })
  end

  def zip = zip_kit_stream(filename: "#{@folder.name}.zip") { |stream| @folder.generate_zip(stream) }

  private

  def folder_params = params.require(:folder).permit(:name, :parent_id, files: [])

  def set_folder
    @folder = Folder.with_bookmarked(current_participant).find(params[:id])
    return if @folder.read?(current_participant)

    logger.warn "#{current_participant} has no read access to #{@folder}"
    redirect_to folders_path, notice: 'Droits insuffisants'
  end

  # FIXME: folders is an array and options is valid for all folders !
  def prepare_elements_from_folders(folders, options = {})
    result = []
    folders.each do |f|
      next unless f.read?(current_participant)

      result << { type: 'folder',
                  name: options[:name] || f.name,
                  id: f.id,
                  size: f.recursive_size,
                  created_at: f.created_at,
                  updated_at: f.updated_at,
                  write: f.write?(current_participant),
                  destroyable: f.destroyable?(current_participant),
                  icon: 'folder',
                  bookmark: f.bookmarked,
                  url: folder_path(f),
                  permissions: f.recursive_permissions,
                  model: f }
    end
    if @order == 'size'
      if @direction == 'ASC'
        result.sort_by! { |r| r[:size] }
      else
        result.sort_by! { |r| -r[:size] }
      end
    end

    order_result(result)
  end

  def prepare_elements_from_files(files)
    result = []
    files.each do |f|
      url = if displayable_file?(f) && !@is_trash
              file_path(@folder.id, f.id)
            else
              rails_blob_path(f, disposition: 'attachment')
            end
      result << { type: 'file',
                  name: f.filename.to_s,
                  id: f.id,
                  size: f.byte_size,
                  created_at: f.created_at,
                  updated_at: f.updated_at || f.created_at,
                  write: @folder.write?(current_participant),
                  destroyable: @folder.trash? ? f.write?(current_participant) : @folder.write?(current_participant),
                  icon: icon_file(f),
                  bookmark: f.bookmarked,
                  url:,
                  permissions: f.permissions,
                  model: f,
                  signed_id: f.signed_id }
    end
    order_result(result)
  end

  def order_result(result)
    if @order == 'size'
      if @direction == 'ASC'
        result.sort_by! { |r| r[:size] }
      else
        result.sort_by! { |r| -r[:size] }
      end
    end
    result
  end

  def fetch_files
    order = @order == 'size' ? 'name' : @order
    order = 'active_storage_blobs.filename' if order == 'name'
    @folder.files.with_bookmarked(current_participant).joins(:blob).includes(:blob).order("bookmarked DESC, #{order} #{@direction}").select do |f|
      f.read?(current_participant)
    end
  end
end
