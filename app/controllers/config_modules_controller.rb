class ConfigModulesController < ApplicationController
  before_action :super_admin_only
  before_action :set_module, only: %i[show edit update]

  # GET /config_modules
  # GET /config_modules.json
  def index
    @modules = []
    @modules_configured, @modules_to_configure = ConfigModule.all.partition { |m| m.instances.count.positive? }
  end

  # GET /config_modules/1
  # GET /config_modules/1.json
  def show
    @instances = Instance.where(config_module_id: @module.id)
  end

  # GET /config_modules/1/edit
  def edit; end

  # PATCH/PUT /config_modules/1
  # PATCH/PUT /config_modules/1.json
  def update; end

  # GET /config_modules/:domain_id/infos.json
  def infos
    @modules = ConfigModule.all
    @infos = []
    @modules.each do |mod|
      domain_infos = DomainInfo.where(
        domain_id: params[:domain_id],
        config_module_id: mod.id
      ).first

      info = {
        id: mod.id,
        name: mod.name,
        resume: mod.resume,
        font: mod.font,
        domain_datas: mod.domain_datas ? eval(mod.domain_datas) : nil
      }
      if domain_infos
        value = domain_infos[:domain_datas] ? eval(domain_infos[:domain_datas]) : nil
        @infos << info.merge(value:)
      else
        @infos << info.merge(value: nil)
      end
    end
  end

  private

  def set_module
    @module = ConfigModule.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def config_params
    params.require(:config).permit(:url, :active, :new_page)
  end
end
