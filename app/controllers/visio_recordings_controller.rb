class VisioRecordingsController < BichikController
  before_action :bbb_authorized!
  before_action :set_visio, only: %i[index destroy visio_recordings after_destroy download]
  before_action :set_dataset, only: ['download']

  # GET /visios/1/recordings
  # GET /visios/1/recordings.json
  def index
    return if @visio.participant == current_participant

    redirect_to @visio, notice: (I18n.t 'modules.bigbluebutton.authorizedaccesvisioconference')
  end

  def visio_recordings
    unless @visio.participant == current_participant
      redirect_to visios_path, notice: (I18n.t 'modules.bigbluebutton.authorizedaccesvisioconference')
    end
    set_dataset
  rescue StandardError
    @recordings_processing = 0
  end

  # Bidouille : Get affiché après un recording destroy pour éviter le rechargement en boucle infinie
  # de la page index des recordings
  def after_destroy; end

  # DELETE /visios/1/recordings/1
  # DELETE /visios/1/recordings/1.json
  def destroy
    if @visio.delete_room_recording(@visio.get_session, params[:record_id])
      redirect_to visio_recordings_path(@visio), notice: I18n.t('modules.bigbluebutton.successfuldestroyrecording')
    else
      redirect_to visio_recordings_path(@visio), error: I18n.t('modules.bigbluebutton.cannotdeletevisio')
    end
  end

  def download
    visio_recording = @dataset[:items].select { |recording| recording.record_id == params[:record_id] }.first
    redirect_to visio_recordings_path(@visio) && return unless visio_recording

    video_format = visio_recording.formats.select { |format| format[:recording_type] == 'video' }.first
    redirect_to visio_recordings_path(@visio) && return unless video_format

    source = URI("#{video_format[:url].gsub('/playback', '')}video-0.m4v")
    filename = "#{@visio.name}-#{visio_recording.recorded_at.in_time_zone(current_timezone).strftime('%Y-%m-%d-%H-%M')}.m4v"

    Net::HTTP.start(source.host, source.port, use_ssl: true) do |http|
      req = Net::HTTP::Get.new source
      http.request(req) do |res|
        send_data res.read_body, filename:, disposition: 'download'
      end
    end
  end

  private

  def bbb_authorized! = config_module_authorized!('BigBlueButton')

  def set_dataset
    @session = @visio.get_session
    recordings = @visio.get_room_recordings(@session, @visio.friendly_id)
    recordings.map! { |recording| VisioRecording.new(recording.deep_symbolize_keys) }
    @dataset = dataset_pagination(recordings, index_params)
    @recordings_processing = @visio.get_room_recordings_processing(@session, @visio.friendly_id)
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_visio
    @visio = Visio.find(params[:id])
  rescue StandardError => e
    flash.alert = "#{I18n.t('modules.bigbluebutton.dontexist')}|#{e}"
    redirect_to visios_path
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def visio_params
    params.require(:visio).permit(:name, :comment, :date_start, :duration, :number_of_participants,
                                  :owner_id, :url_visios)
  end

  def index_params
    params.permit(:author, :order, :query, :page, :items_per_page, :archived)
  end
end
