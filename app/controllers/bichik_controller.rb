# frozen_string_literal: true

class BichikController < ApplicationController
  include BichikHelper
  before_action :authenticated_participant!, unless: -> { turbo_frame_request? }
  before_action :set_modules, unless: -> { turbo_frame_request? }

  def teacher_only! = coerce_or_redirect! current_participant.teaching?
  def learner_only! = coerce_or_redirect! current_participant.learning?
  def tutor_only! = coerce_or_redirect! current_participant.tutoring?
  def teacher_or_learner_only! = coerce_or_redirect!(current_participant.teaching? || current_participant.learning?)

  private

  # coerce condition
  def coerce_or_redirect!(precondition)
    return if precondition

    logger.error("<#{current_participant}> does not enforce coerced condition: <#{action_name}>", caller[0])
    redirect_to services_path, alert: (I18n.t 'application.law')
  end

  def set_modules
    logger.debug 'FIXME: set_modules should be called sparingly!'
    @modules = current_participant.config_modules.order(:order)
  end

  def authenticated_participant!
    if user_signed_in?
      redirect_to participants_path unless current_participant
    else
      redirect_to new_user_session_path
    end
  end

  def config_module_authorized!(config_module_name)
    return if current_participant.config_module_by_name_enabled?(config_module_name)

    redirect_to(services_path, notice: (I18n.t 'application.law'))
  end

  def only_owner!(owner, return_path = services_path)
    return if owner == current_participant

    logger.warn "Not Authorized: current participant <#{current_participant}> do not own actual resource, ...should have been <#{owner}> instead!"
    redirect_to return_path, alert: I18n.t('application.law')
  end

  def not_tutoring(return_path)
    return unless current_participant.tutoring?

    redirect_to return_path, alert: I18n.t('application.law')
  end
end
