class LinksController < BichikController
  include BookmarkableController
  include LinksHelper

  before_action :set_global, only: %i[index new create]
  before_action :set_pagy_links, only: :index
  before_action :set_fa_list, only: %i[new create edit]
  before_action :set_link, except: %i[index new create]
  before_action :only_owner_or_global_teacher_classroom!, except: %i[index new create]

  def index; end

  def new
    @link = Link.new(symbol: 'link')

    if turbo_frame_request?
      render partial: 'icons'
    else
      render :new
    end
  end

  def edit
    if @link.global && !current_participant.teaching?
      redirect_to links_path,                  notice: (I18n.t 'status.autorize')

    elsif @link.participant != current_participant
      redirect_to links_path,                  notice: (I18n.t 'status.autorize')
    elsif turbo_frame_request?
      render partial: 'icons'
    else
      render :edit
    end
  end

  def create
    @link = Link.create_with(participant: current_participant).new(link_params)
    @link.global = @global && current_participant.teaching?

    if @link.save
      redirect_to links_path(extract_q_global), notice: I18n.t('links.successfull_create')
    else
      set_fa_list
      render_turbo_error(@link, turbo_frame: 'link_form', partial: 'form')
    end
  end

  def update
    @link.update(link_params)
    if @link.save
      redirect_to links_path(q: { global: @link.global }), notice: I18n.t('links.successfull_update')
    else
      set_fa_list
      render_turbo_error(@link, turbo_frame: 'link_form', partial: 'form')
    end
  end

  def destroy
    if current_classroom == @link.classroom
      global = @link.global
      @link.destroy
      respond_to do |format|
        format.html { redirect_to links_url(q: { global: }), notice: I18n.t('links.successfull_delete') }
        format.json { head :no_content }
      end
    else
      redirect_to links_url(q: { global: }), notice: 'Vous n\'avez pas la permission de supprimer ce lien'
    end
  end

  private

  def link_params = params.require(:link).permit(:title, :resume, :font, :url, :color)
  def q_params = params.fetch(:q, {}).permit!

  def extract_q_global
    global = params.dig(:q, :global)
    return unless global

    { q: { global: } }
  end

  def set_global
    @global = current_participant.teaching? ? string_to_bool(q_params[:global]) : false
  end

  def set_pagy_links
    links = Link.where(global: @global).participant_user_included

    links = if @global
              links.where(participant: { classroom: current_classroom })
            else
              links.where(participant: current_participant)
            end

    @q = links.ransack(default_bookmarkable_sort(q_params, Link))
    @pagy, @links = pagy(@q.result)
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_link
    @link = Link.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def link_params
    params.require(:link).permit(:title, :resume, :url, :font, :color, :symbol)
  end

  def set_fa_list
    icons = icons_list_model(params[:query])
    @pagy, @icons = pagy_array(icons, limit: 100)
  end

  def index_params
    params.permit(:order, :query, :page, :items_per_page)
  end

  def links_filters_and_order(links, params)
    if params[:query].present?
      links = links.where('title ILIKE ? or resume ILIKE ?', "%#{params[:query]}%",
                          "%#{params[:query]}%")
    end

    if params[:order]
      _, order_direction = params[:order].split(',')
      order_direction = 'DESC' unless %w[ASC DESC].include?(order_direction)
    else
      order_direction = 'DESC'
    end
    order_column = 'title'

    links.order("#{order_column} #{order_direction}")
  end

  def only_owner_or_global_teacher_classroom!
    return if @link.participant == current_participant
    return if current_classroom == @link.classroom && current_participant.teaching? && @link.global

    redirect_to links_path(global: params[:global]), alert: I18n.t('application.law')
  end
end
