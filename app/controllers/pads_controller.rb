class PadsController < BichikController
  include BookmarkableController

  before_action :etherpad_authorized!
  before_action :set_pagy_pads, only: :index
  before_action :set_pad, except: %i[index new create]
  before_action :create_authorized!, only: %i[new create]
  before_action :modify_authorized!, only: %i[edit update destroy]

  def index; end

  def show
    @type_page = 'iframe'
    @url_embedded = @pad.full_url
  end

  def new
    @pad = Pad.new
  end

  def create
    @pad = weird_creation_from_configmodule(pad_params)
    if @pad.save
      redirect_to @pad, notice: I18n.t('modules.etherpad.noticecreate', pad_name: @pad.name)
    else
      render_turbo_error(@pad, turbo_frame: 'pad_form', partial: 'form')
    end
  end

  def edit; end

  def update
    if @pad.update(pad_params)
      redirect_to pads_path, notice: I18n.t('modules.etherpad.noticeupdate', pad_name: @pad.name)
    else
      render_turbo_error(@pad, turbo_frame: 'pad_form', partial: 'form')
    end
  end

  def destroy
    @pad.destroy
    redirect_to pads_path, notice: I18n.t('modules.etherpad.noticedestroy')
  end

  def toggle_archive
    @pad.toggle_archive(current_participant)
    redirect_to pads_path(q: q_params)
  end

  def toggle_bookmark
    @pad.toggle_bookmark(current_participant)
    redirect_to pads_path(q: q_params)
  end

  private

  def etherpad_authorized! = config_module_authorized!('Etherpad')
  def create_authorized! = not_tutoring(pads_path)
  def modify_authorized! = only_owner!(@pad.participant, pads_path)

  def pad_params = params.require(:pad).permit(:name, :comment)
  def q_params = params.fetch(:q, {}).permit!

  def filtering
    filter = q_params.slice(:archived)
    filter[:archived] ||= false
    filter
  end

  def set_pagy_pads
    pads = Pad.with_full(current_participant, filtering:)
    @q = pads.ransack(default_bookmarkable_sort(q_params, Pad))
    @pagy, @pads = pagy(@q.result)
  end

  def set_pad
    @pad = Pad.find_full(current_participant, params[:id])
  end

  # FIXME: why is it so complex!?
  def weird_creation_from_configmodule(params)
    @pad = Pad.new(params)
    @pad.uuid = UUIDTools::UUID.random_create.to_s
    @pad.participant = current_participant

    config_module = ConfigModule.find_by(name: 'Etherpad')
    @pad.config_module = config_module
    domain_info = DomainInfo.find_by(classroom: current_classroom, config_module:)
    url_server = domain_info.instance.url
    @pad.full_url = "#{url_server}p/#{@pad.uuid}"
    @pad
  end
end
