class InvitationsController < BichikController
  before_action :from_invitable,
                except: %i[resend destroy]

  before_action :teacher_only!, only: :invite_per_groups

  # GET /invitations/from/:invitable_type/:invitable_id
  def index
    @q = Invitation.where(invitable: @invitable).ransack(params[:query])
    @pagy, @invitations = pagy(@q.result)
    render :index
  end

  # GET /invitations/from/:invitable_type/:invitable_id/groups
  def invite_per_groups
    all_groups_selected = Group.where(classroom_id: current_classroom.id)
                               .where(
                                 id: Invitation
                                 .where(invitable: @invitable)
                                 .where(recipient_type: Group.to_s)
                                 .pluck(:recipient_id)
                               )

    @q_selected = all_groups_selected.ransack(params[:query])
    @q_selected.sorts = 'name DESC'
    @pagy_selected, @groups_selected = pagy(@q_selected.result)

    @q_unselected = Group.where(classroom_id: current_classroom.id).where.not(id: all_groups_selected).ransack(params[:query])
    @q_unselected.sorts = 'name DESC'
    @pagy_unselected, @groups_unselected = pagy(@q_unselected.result)
  end

  # POST /invitations/from/:invitable_type/:invitable_id/groups/:id
  def invite_group_add
    group = Group.find(params[:id])
    Invitation.create(invitable: @invitable, recipient: group)
    @count = group.participants.map do |participant|
      if participant_invitation_exists?(@invitable, participant)
        # TODO: not yet perfect, previous group invitations might have already invited this user too!
        logger.info "participant <#{participant}> was individually and already invited from this invitable <#{@invitable}>"
        0
      else
        send_email(@invitable, participant.email)
        1
      end
    end.sum
    logger.info "new invitation group <#{group.id} - #{group.name}> to invitable <#{@invitable}>"
    redirect_to :invite_per_groups, notice: "#{@count} emails d'invitation vont être envoyés"
  end

  # DELETE /invitations/from/:invitable_type/:invitable_id/groups/:id
  def invite_group_del
    group = Group.find(params[:id])
    @invitable.invitations.where(recipient: group).destroy_all
    logger.info "del invitation group <#{group.id} - #{group.name}> from invitable <#{@invitable}>"
    redirect_to :invite_per_groups, alert: "Le groupe #{group.name} a été retiré des invités"
  end

  # POST /invitations/from/:invitable_type/:invitable_id/participants/:id
  def invite_participant_add
    participant = Participant.find(params[:id])
    Invitation.create(invitable: @invitable, recipient: participant)
    send_email(@invitable, participant.user.email)
    logger.info "new invitation participant <#{participant.participable_type} - #{participant.name}> to invitable <#{@invitable}>"
    redirect_to :invite_per_participants, notice: "Une invitation va être envoyée à #{participant.name}"
  end

  # DELETE /invitations/from/:invitable_type/:invitable_id/participant/:id
  def invite_participant_del
    participant = Participant.find(params[:id])
    @invitable.invitations.where(recipient: participant).destroy_all
    logger.info "del invitation participant <#{participant.participable_type} - #{participant.name}> from invitable <#{@invitable}>"
    redirect_to :invite_per_participants, notice: "#{participant.name} a été retiré des invités"
  end

  # GET /invitations/from/:invitable_type/:invitable_id/participants
  def invite_per_participants
    involved_participants = current_participant.involved_participants

    selected_participants = involved_participants
                            .where(id: Invitation.where(invitable: @invitable)
                         .where(recipient_type: Participant.to_s)
                         .pluck(:recipient_id))
    @q_selected = selected_participants.ransack(params[:query])
    @q_selected.sorts = 'last_name first_name'
    @pagy_selected, @participants_selected = pagy(@q_selected.result)

    @q_unselected = involved_participants.where.not(id: selected_participants).ransack(params[:query])
    @q_unselected.sorts = 'last_name first_name'
    @pagy_unselected, @participants_unselected = pagy(@q_unselected.result)
  end

  # POST /invitations/from/:invitable_type/:invitable_id/emails
  def invite_per_emails; end

  # PATCH /invitations/from/:invitable_type/:invitable_id/emails
  def invite_per_emails_submit
    new_emails = params[:emails][:new_emails]
    count = invite_new_external_and_count(new_emails)
    redirect_to(build_invitations_path(@invitable), notice: I18n.t('invitations.new_invitations_sent', count:))
  end

  # POST /invitations/from/:invitable_type/:invitable_id
  def invite
    count = invite_users_then_groups_then_external_and_count
    redirect_to(build_invitations_path(@invitable), notice: I18n.t('invitations.new_invitations_sent', count:))
  end

  # PATCH /invitations/:id
  def resend
    invitation = Invitation.find(params[:id])
    count = resend_and_count(invitation)

    redirect_to build_invitations_path(invitation.invitable),
                notice: I18n.t('invitations.invitations_resent', count:)
  end

  # DELETE /invitation/:id
  def destroy
    invitation = Invitation.find(params[:id])
    path = build_invitations_path(invitation.invitable)
    invitation.destroy

    redirect_to path, notice: I18n.t('invitations.destroy_success')
  end

  private

  def build_invitations_path(invitable)
    invitations_path(invitable.class.to_s.downcase, invitable.id)
  end

  def resend_and_count(invitation)
    recipient = invitation.recipient
    if invitation.recipient.is_a? Group
      recipient.participants.each { |p| send_email(invitation.invitable, p.email) }
      recipient.users.count
    else
      external = recipient.nil?
      email = external ? invitation.external_recipient : recipient.email
      send_email(invitation.invitable, email, external:)
      1
    end
  end

  def invite_users_then_groups_then_external_and_count
    invite_new_users_and_count +
      invite_new_groups_and_count +
      invite_new_external_and_count
  end

  def from_invitable
    invitable_model = Object.const_get(params[:invitable_type].capitalize) # FIXME: prefer using String#constantize instead!
    raise "class <#{invitable_model}> is not a Model" unless invitable_model.superclass == ApplicationRecord
    raise "model <#{invitable_model}> does not include Invitable" unless invitable_model.include?(Invitable)

    @invitable = invitable_model.find(params[:invitable_id])
    only_owner!(@invitable.participant, services_path)

    @group_shares = Invitation.where(invitable: @invitable, recipient_type: Group.to_s)
    @internal_shares = Invitation.where(invitable: @invitable, recipient_type: User.to_s)
    @external_shares = Invitation.where(invitable: @invitable, recipient_type: nil)
  end

  def participant_invitation_exists?(invitable, participant)
    participant == invitable.participant || Invitation.exists?(invitable:, recipient: participant)
  end

  def external_user_invitation_exists?(invitable, email)
    Invitation.exists?(invitable:, external_recipient: email)
  end

  def group_invitation_exists?(invitable, group_id)
    Invitation.exists?(invitable:, recipient: Group.find(group_id))
  end

  def send_email(invitable, email, external: false)
    if invitable.instance_of?(::Meeting)
      InvitationUserNotifier.meeting_invit_email(@current_user, email, invitable).deliver_later
    else
      # FIXME: url_local, on devrait demander à l'invitable directement son ConfigModule
      config_module = ConfigModule.find_by(url_local: "/#{invitable.class.to_s.downcase}s")

      domain_info = DomainInfo.find_by(classroom: current_classroom, config_module:)

      host = request.host_with_port.to_s
      url = if external
              if invitable.is_a?(Visio) # FIXME: remove binding to Visio!
                "#{domain_info.instance.url}/rooms/#{invitable.friendly_id}/join"
              elsif invitable.is_a?(Pad)
                "#{domain_info.instance.url}p/#{invitable.uuid}"
              else
                "#{domain_info.instance.url}/#{invitable.uuid}"
              end
            else
              "https://#{host}#{config_module.url_local}/#{invitable.id}"
            end
      InvitationUserNotifier.invit_email(@current_user, email, invitable, url, host,
                                         invitable.class.to_s.downcase).deliver_later
    end
  end

  def invite_new_users_and_count
    return 0 unless params[:invitation_users]

    params[:invitation_users].split(',').map do |user_id|
      next 0 if user_invitation_exists?(@invitable, user_id)

      recipient = User.find(user_id)
      Invitation.create(invitable: @invitable, recipient:)
      send_email(@invitable, recipient.email)
      1
    end.sum
  end

  def invite_new_groups_and_count
    return 0 unless params[:invitation_groups]

    params[:invitation_groups].split(',').map do |group_id|
      next 0 if group_invitation_exists?(@invitable, group_id)

      group = Group.find(group_id)
      Invitation.create(invitable: @invitable, recipient: group)
      group.users.map do |u|
        if user_invitation_exists?(@invitable, u.id)
          # TODO: not yet perfect, previous group invitations might have already invited this user too!
          logger.info "user <#{u.name}> was individually and already invited from this invitable <#{@invitable}>"
          0
        else
          send_email(@invitable, u.email)
          1
        end
      end.sum
    end.sum
  end

  def invite_new_external_and_count(emails)
    return 0 unless emails

    emails = normalize_email_separator_space_or_semicolon(emails)
    emails.split(',').map do |email|
      next 0 if external_user_invitation_exists?(@invitable, email)

      if email.present?
        Invitation.create(invitable: @invitable, external_recipient: email)
        send_email(@invitable, email, external: true)
        1
      else
        0
      end
    end.sum
  end

  def normalize_email_separator_space_or_semicolon(emails) = emails.tr(' ', ',').tr(';', ',')
end
