class MeetingsController < BichikController
  include ActionView::RecordIdentifier
  before_action :set_meeting, only: %i[show edit update destroy]

  def index
    @zoom = false
    @started_at = params.fetch(:started_at, Time.now.beginning_of_day).to_datetime.in_time_zone(current_user.time_zone)
    @start_week = @started_at.beginning_of_week
    @end_week = @started_at.end_of_week
    @dates_of_week = (@start_week.to_date..@end_week.to_date).to_a

    # TODO: with_full? owned? invited? shared? see in Participant_Feature
    @meetings = Meeting.with_invited(current_participant).where(started_at: @start_week..@end_week).order(:started_at)

    @teaching_sequences = current_classroom.teaching_sequences.where(started_at: @start_week..@end_week).order(:started_at)
    @visios = Visio.with_invited(current_participant)
                   .where(started_at: @start_week..@end_week).order(:started_at)
  end

  def show
    return if @meeting.invitations.map { |invitation| invitation.participant_invited?(current_participant) }.any?

    redirect_to meetings_path,
                alert: t('application.law')
  end

  def new
    @meeting = Meeting.new
    at = params[:started_at] ? DateTime.parse(params[:started_at]) : Time.now.in_time_zone(current_user.time_zone)
    @meeting.started_at = at
    @meeting.ended_at = at + 1.hour
  end

  def edit
    redirect_to meetings_path, alert: t('application.law') unless @meeting.participant == current_participant
  end

  def create
    @meeting = Meeting.new(meeting_params)
    @meeting.participant = current_participant
    @meeting.started_at = meeting_params[:started_at].in_time_zone(current_timezone)
    @meeting.ended_at = meeting_params[:ended_at].in_time_zone(current_timezone)

    if @meeting.save
      if params[:invitation]
        redirect_to invitations_path(Meeting.to_s.downcase, @meeting.id, return_path: edit_meeting_path(@meeting))
      else
        redirect_to meetings_path(started_at: @meeting.started_at, anchor: '6'),
                    notice: 'Meeting was successfully created.'
      end
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @meeting.participant == current_participant
      meetingp = meeting_params
      meetingp[:started_at] = meetingp[:started_at].in_time_zone(current_timezone)
      meetingp[:ended_at] = meetingp[:ended_at].in_time_zone(current_timezone)

      if @meeting.update(meetingp)
        if params[:invitation]
          redirect_to invitations_path(Meeting.to_s.downcase, @meeting.id, return_path: edit_meeting_path(@meeting))
        else
          redirect_to meetings_path(started_at: @meeting.started_at, anchor: '6'),
                      notice: 'Meeting was successfully updated.'
        end
      else
        render :edit, status: :unprocessable_entity
      end
    else
      redirect_to meetings_path, alert: t('application.law')
    end
  end

  def destroy
    started_at = @meeting.started_at
    if params[:recursive]
      @meeting.destroy_recurring
    else
      @meeting.destroy
    end
    redirect_to meetings_path(started_at:), notice: 'Meeting was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_meeting
    @meeting = Meeting.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def meeting_params
    params.require(:meeting).permit(:name, :started_at, :ended_at, :description)
  end
end
