class GroupsController < BichikController
  before_action :set_classroom
  before_action :set_group, only: %i[users destroy show update]
  # TODO: vérifier les permissions
  # before_action :admin_only

  def index
    @q = @classroom.participants.select(:id, :user_id, 'users.last_name', 'users.first_name',
                                        'users.email').joins(:user).ransack(params[:q])
    @q.sorts = 'users.last_name users.first_name' if @q.sorts.empty?
    scope = @q.result(distinct: true)

    @pagy, @participants = pagy(scope)

    user_id = params[:user_id]
    @user_groups_ids = user_id ? User.find(user_id).groups_uuid : []
    @groups = @classroom.groups
    @group = Group.new

    if turbo_frame_request?
      render partial: 'participants', locals: { participants: @participants }
    else
      render :index
    end
  end

  def show
    @members = @group.participants.includes(:user).order('users.last_name', 'users.first_name')

    @q = @classroom.participants
                   .select(:id, :user_id, 'users.last_name', 'users.first_name', 'users.email')
                   .joins(:user).where.not(id: @members.map(&:id))
                   .ransack(params[:q])
    @q.sorts = 'users.last_name users.first_name' if @q.sorts.empty?
    scope = @q.result(distinct: true)

    @pagy, @participants = pagy(scope)

    if turbo_frame_request?
      render partial: 'participants', locals: { participants: @participants }
    else
      render :show
    end
  end

  # GET /clasroom/1/groups/1/users.json
  def users; end

  def create
    group_name = group_params[:name]
    members = group_params[:users].split(',')
    if members.count.positive?
      participants = Participant.find(members)

      @group = Group.new(name: group_name, classroom: @classroom, participants:)

      if @group.save
        logger.info("création du groupe <#{group_name}>")
        redirect_to groups_path(@classroom.id), notice: (I18n.t 'groups.noticecreate').to_s
      else
        error = I18n.t('groups.error')
        logger.warn("impossible de créer le groupe <#{group_name}> car: #{error}")
        redirect_to groups_path(@classroom.id), alert: "#{I18n.t('groups.error.create', group_name:)}|#{error}"
      end
    else
      redirect_to groups_path(@classroom.id),
                  flash: { alert: "#{I18n.t('groups.error.create', group_name:)}|#{I18n.t('groups.addusergroup')}" }
    end
  end

  def destroy
    if @group.automatic?
      redirect_to groups_path(@classroom), alert: I18n.t('application.law')
    else
      @group.destroy
      redirect_to groups_path(@classroom), notice: (I18n.t 'groups.noticedestroy')
    end
  end

  def update
    if @group.automatic?
      redirect_to @group, alert: I18n.t('application.law')
    else
      if params[:delete] == '1'
        participant = Participant.find(params[:member])
        @group.participants.delete(participant)
      else
        participants = Participant.where(id: params[:member].split(','))
        @group.participants += participants
      end
      redirect_to group_path(@classroom.id, @group.id), notice: (I18n.t 'groups.noticeupdate')
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_classroom
    @classroom = Classroom.find(params[:id])
  end

  def set_group
    @group = Group.find(params[:group_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def group_params
    params.require(:group).permit(:id, :name, :users)
  end
end
