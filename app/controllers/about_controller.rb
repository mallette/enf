class AboutController < BichikController
  def index
    @teams = [
      { name: 'Jean-Noël ROUCHON', role: 'about.devbackend' },
      { name: 'Philippe VINCENT', role: 'about.devbackend' },
      { name: 'François AUDIRAC', role: 'about.testing_and_documentation' },
      { name: 'Charlotte SIMON', role: 'about.tester' },
      { name: 'Maïtané LENOIR', role: 'about.webdesigner' },
      { name: 'Jérôme MUNIER', role: 'about.devbackend' },
      { name: 'Nicolas VENEROSY', role: 'about.devbackend' },
      { name: 'Guillaume BOURBIER', role: 'about.devbackend' },
      { name: 'Gaëtan BANDAMA', role: 'about.devbackend' }
    ]
  end
end
