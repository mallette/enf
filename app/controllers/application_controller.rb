# frozen_string_literal: true

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include ApplicationHelper
  include HttpAcceptLanguage::AutoLocale
  include Pagy::Backend

  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  around_action :tag_log

  # Only allow modern browsers supporting webp images, web push,
  # badges, import maps, CSS nesting, and CSS :has.
  # allow_browser versions: :modern

  UUID_FOR_EXTERNAL_SURVEY = 'external-'
  UUID_FOR_USER_EXTERNAL_ACCES_TO_SERVICES = 'external-'

  def flash_stream = turbo_stream.replace('notification', partial: 'layouts/notification')

  # FIXME: deprecated, refactor with pagy instead!
  def dataset_pagination(dataset, params, default_items_per_page = 10)
    page = params && params[:page].present? ? params[:page].to_i : 1
    count = dataset.count
    items_per_page = (params && params[:items_per_page]).to_i
    items_per_page = default_items_per_page if items_per_page < default_items_per_page
    items_per_page = count if items_per_page > count

    start = (page - 1) * items_per_page
    stop = start + items_per_page - 1

    {
      items: dataset[start..stop],
      page:,
      items_per_page:,
      count:
    }
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:theme])
    devise_parameter_sanitizer.permit(:account_update, keys: [:theme])
  end

  def after_sign_out_path_for(_resource_or_scope) = new_user_session_path

  private

  def tag_log(&) = SemanticLogger.tagged(session[Rails.application.config.x.action_controller.main_session_tag], &)

  def participant_type_only!(participant_type)
    return if current_user&.participant_type(current_classroom) == participant_type

    logger.error(
      "failed attempt to current_user <#{current_user}> of type <#{current_user&.participant_type(current_classroom)}>"
    )
    redirect_to services_path, alert: I18n.t('application.law')
  end

  def authorized_access?(domain_id = nil)
    return false unless @current_user

    @current_user.superadmin || (@current_user.admin && @current_user.domain.id == domain_id)
  end
end
