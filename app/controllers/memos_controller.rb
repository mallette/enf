class MemosController < BichikController
  include BookmarkableController

  before_action :framemo_authorized!
  before_action :set_pagy_memos, only: :index
  before_action :set_memo, except: %i[index new create]
  before_action :create_authorized!, only: %i[new create]
  before_action :modify_authorized!, only: %i[edit update destroy]

  def index; end

  def show
    @type_page = 'iframe'
    config_module = ConfigModule.find_by(name: 'Framemo')
    domain_info = DomainInfo.find_by(classroom: current_classroom, config_module:)
    url_server = domain_info.instance.url
    if url_server
      @url_embedded = url_server
      @url_embedded += '/' unless @url_embedded[-1] == '/'
      @url_embedded += @memo.uuid
    else
      redirect_to services_path, notice: (I18n.t 'modules.framemo.noactif').to_s
    end
  end

  def new
    @memo = Memo.new
  end

  def edit; end

  def create
    @memo = Memo.new(memo_params)
    @memo.participant = current_participant

    if @memo.save
      redirect_to @memo, notice: I18n.t('modules.framemo.noticecreate', memo_name: @memo.name)
    else
      render_turbo_error(@memo, turbo_frame: 'memo_form', partial: 'form')
    end
  end

  def update
    if @memo.update(memo_params)
      redirect_to memos_path, notice: I18n.t('modules.framemo.noticeupdate', memo_name: @memo.name)
    else
      render_turbo_error(@memo, turbo_frame: 'memo_form', partial: 'form')
    end
  end

  def destroy
    @memo.destroy
    redirect_to memos_path, notice: I18n.t('modules.framemo.noticedestroy')
  end

  def toggle_archive
    @memo.toggle_archive(current_participant)
    redirect_to memos_path(q: q_params)
  end

  def toggle_bookmark
    @memo.toggle_bookmark(current_participant)
    redirect_to memos_path(q: q_params)
  end

  private

  def framemo_authorized! = config_module_authorized!('Framemo')
  def create_authorized! = not_tutoring(memos_path)
  def modify_authorized! = only_owner!(@memo.participant, memos_path)

  def memo_params = params.require(:memo).permit(:name, :comment)
  def q_params = params.fetch(:q, {}).permit!

  def filtering
    filter = q_params.slice(:archived)
    filter[:archived] ||= false
    filter
  end

  def set_pagy_memos
    memos = Memo.with_full(current_participant, filtering:)
    @q = memos.ransack(default_bookmarkable_sort(q_params, Memo))
    @pagy, @memos = pagy(@q.result)
  end

  def set_memo
    @memo = Memo.find_full(current_participant, params[:id])
  end
end
