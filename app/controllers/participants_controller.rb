class ParticipantsController < ApplicationController
  include BichikHelper
  include ActionView::Helpers::TranslationHelper

  before_action :authenticate_user!

  def index
    @modules = []
    @type_page = 'login'
    @participants = current_user.participants.where(enabled: true).joins(:classroom).where(classroom: { enabled: true })
    switch_to(@participants.first) if @participants.one?
  end

  def choose = switch_to current_user.participants.find(params[:id])

  private

  def switch_to(participant)
    unless participant.enabled && participant.classroom.enabled
      reset_session && redirect_to(new_user_session_path, alert: 'Désactivé') && return if @participants&.one?

      redirect_to(participants_path, alert: 'Désactivé') && return
    end

    logger.info("User <#{current_user.name}> enrolls <#{participant}>")
    session[:participant_id] = participant.id
    participant.update last_login: DateTime.now
    redirect_to services_path,
                notice: ts('welcome_aboard', classroom: participant.classroom.name)
  end
end
