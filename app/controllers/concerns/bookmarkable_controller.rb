module BookmarkableController
  extend ActiveSupport::Concern
  include SortableController

  included do
    def default_bookmarkable_sort(items, model_class)
      prepend_sort_by_bookmark_desc(default_sort_from_model(items, model_class))
    end

    private

    def prepend_sort_by_bookmark_desc(items)
      items[:s] = Array.wrap(items[:s]).prepend('bookmarked desc').compact
      items
    end
  end
end
