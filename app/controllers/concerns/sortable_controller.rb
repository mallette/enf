module SortableController
  extend ActiveSupport::Concern
  included do
    def default_sort_from_model(items, model_class)
      ancestors = model_class.ancestors
      unless ancestors.include?(ApplicationRecord) && ancestors.include?(Sortable)
        raise "model_class must:\n
         inherit from ApplicationRecord\n
         include Sortable"
      end

      return items if items[:s]

      items[:s] = model_class.instance_variable_get(Sortable::DEFAULT_SORT_VARIABLE_NAME)
      items
    end
  end
end
