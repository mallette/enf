Bichik
======

Bichik provides a platform for digital school management system.

First Init
----------

* # configure database.yml
* rails db:schema:load
* rails db:seed
* rails classrooms:create_from_graf

* rails console
  * # find a teacher user and level up to SuperAdmin
  * u=User.find_by(last_name:'xxxx')
  * u.superadmin=true
  * u.save
* # connect with superAdmin
  * # Menu / Les Services
  * Bigbluebutton, Framemo, Etherpad, Zigouigoui
    * ...

* rails modules:add_instance_to_all_classrooms
  * # activate all
* rails modules:activate_module_for_all_users
  * # activate all

* ./bin/dev

[More information for IT people](./doc/developers.md)
