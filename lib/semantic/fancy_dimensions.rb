require 'ostruct'

module Semantic
  # extra dimensions for customizing the logging format
  module FancyDimensions
    def self.new(rails: '╣x╠', before: 0, after: 0, terminus: false)
      OpenStruct.new(rails:, before:, after:, terminus:) # rubocop:disable Style/OpenStructUse
    end

    def self.start
      OpenStruct.new(rails: '╓─╖') # rubocop:disable Style/OpenStructUse
    end

    def self.end
      OpenStruct.new(rails: '╣ ╠') # rubocop:disable Style/OpenStructUse
    end

    def self.around
      OpenStruct.new(rails: '╣ ╠') # rubocop:disable Style/OpenStructUse
    end
  end
end
