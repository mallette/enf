module Semantic
  # custom Error for dispatching a remanent tag
  class TagWrapError < StandardError
    attr_reader :error, :tag

    def initialize(tag, error)
      @tag = tag
      @error = error
      super("my error message contains tag #{tag}")
    end

    def to_s = "#{self.class}#{@tag}"
  end
end
