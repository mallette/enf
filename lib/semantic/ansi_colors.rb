module Semantic
  # common definitions and constants
  module AnsiColors
    ANSI_REGEX = /\e\[[0-9;]*m/ # TODO: support for \x1b and \033

    # FORMAT
    CLEAR        = "\e[0m".freeze
    BOLD         = "\e[1m".freeze
    UNDERLINE    = "\e[4m".freeze

    # TEXT 16-colors
    TEXT_BLACK   = "\e[30m".freeze
    TEXT_RED     = "\e[31m".freeze
    TEXT_GREEN   = "\e[32m".freeze
    TEXT_BROWN   = "\e[33m".freeze
    TEXT_BLUE    = "\e[34m".freeze
    TEXT_MAGENTA = "\e[35m".freeze
    TEXT_CYAN    = "\e[36m".freeze
    TEXT_WHITE   = "\e[37m".freeze

    # TEXT GRAY SHADES 256-colors
    TEXT_GRAY_800     = "\e[38;5;232m".freeze
    TEXT_GRAY_700     = "\e[38;5;233m".freeze
    TEXT_GRAY_600     = "\e[38;5;235m".freeze
    TEXT_GRAY_500     = "\e[38;5;238m".freeze
    TEXT_GRAY_400     = "\e[38;5;241m".freeze
    TEXT_GRAY_300     = "\e[38;5;244m".freeze
    TEXT_GRAY_200     = "\e[38;5;249m".freeze
    TEXT_GRAY_100     = "\e[38;5;252m".freeze

    # TEXT 256-colors
    TEXT_PINK         = "\e[38;5;175m".freeze
    TEXT_ORANGE       = "\e[38;5;202m".freeze
    TEXT_YELLOW       = "\e[93m".freeze

    # DARK TEXT
    DARK_TEXT_BLACK   = "\e[90m".freeze
    DARK_TEXT_RED     = "\e[91m".freeze
    DARK_TEXT_GREEN   = "\e[92m".freeze
    DARK_TEXT_YELLOW  = TEXT_YELLOW
    DARK_TEXT_BLUE    = "\e[94m".freeze
    DARK_TEXT_MAGENTA = "\e[95m".freeze
    DARK_TEXT_CYAN    = "\e[96m".freeze
    DARK_TEXT_WHITE   = "\e[97m".freeze

    # LIGHT TEXT
    LIGHT_TEXT_RED = "\e[38;5;9m".freeze

    # BACKGROUND
    BG_BLACK   = "\e[40m".freeze
    BG_WHITE   = "\e[47m".freeze
    BG_GRAY    = "\e[100m".freeze
    BG_RED     = "\e[41m".freeze
    BG_GREEN   = "\e[42m".freeze
    BG_BROWN   = "\e[43m".freeze
    BG_YELLOW  = "\e[103m".freeze
    BG_BLUE    = "\e[44m".freeze
    BG_MAGENTA = "\e[45m".freeze
    BG_CYAN    = "\e[46m".freeze

    # DARK BACKGROUND
    DARK_BG_RED     = "\e[101m".freeze
    DARK_BG_GREEN   = "\e[102m".freeze
    DARK_BG_YELLOW  = BG_YELLOW
    DARK_BG_BLUE    = "\e[104m".freeze
    DARK_BG_MAGENTA = "\e[105m".freeze
    DARK_BG_CYAN    = "\e[106m".freeze

    # helper methods
    # --------------

    def colorize(text, tint = color, clear = CLEAR) = "#{tint}#{text}#{clear}"
  end
end
