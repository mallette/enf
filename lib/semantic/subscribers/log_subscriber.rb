module Semantic
  module Subscribers
    # Abstract LogSubscriber
    class LogSubscriber
      attr_reader :logger, :event_group

      def initialize(logger_name = nil, **_options)
        @event_group = self.class.name.demodulize.underscore.to_sym
        @logger = SemanticLogger[logger_name || @event_group]
      end

      def any_hook(event)
        logger.warn(
          "#{@event_group} hook=<#{event.name.split('.')[0]}> needs a proper message handling!", event.payload.keys
        )
      end
    end
  end
end
