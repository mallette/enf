module Semantic
  # Abstract colorized formatter
  class AbstractFormatter < SemanticLogger::Formatters::Color
    include AnsiColors
    TAG_NONE = ''.freeze
    CENTER_SIZE = 20
    TAG_FIXED_LENGTH = Rails.application.config.x.action_controller.main_session_tag_fixed_length || 10

    def initialize(color_map:, time_format: nil)
      super(color_map:)
      @time_format = time_format
    end

    private

    def exception
      return unless log.exception

      if log.exception.is_a?(Semantic::TagWrapError)
        exc_wrapper = log.exception
        # puts "TAG_WRAP detected #{exc_wrapper.tag}"

        exc = exc_wrapper.error
        log.tags = Array.wrap(exc_wrapper.tag)
      else
        exc = log.exception
      end

      clazz = colorize("#{exc.class}\n", color_map[:fatal])
      message = colorize(exc.message.chomp(''), color_map[:error])
      backtrace = exc.backtrace
      stack = Semantic::Helper.stackisize(*backtrace)
      ["#{clazz}#{message}", stack].compact.join("\n")
    end

    def origin
      true_class = log.name == log.name.upcase_first
      taint = true_class ? TEXT_CYAN : TEXT_GRAY_400
      colorize(centerize(log.name), taint)
    end

    def tags
      first_tag, taint = log.tags.empty? ? [TAG_NONE, CLEAR] : [log.tags.first, BG_GRAY + TEXT_YELLOW]
      colorize(centerize(first_tag, TAG_FIXED_LENGTH), taint)
    end

    def named_tags
      return if log.named_tags.empty?

      log.named_tags
    end

    def centerize(text, max_length = CENTER_SIZE) = text.reverse.truncate(max_length).reverse.center(max_length)
  end
end
