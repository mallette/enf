require 'io/console'
require 'amazing_print'
require 'json'

module Semantic
  # wraps meanwhile takes care of ansi colors
  class FancyFormatter < AbstractFormatter
    FOREMAN_PREFIX_LENGTH = 18
    FAILOVER_WRAP = 80
    CHAR_FATAL = '⯶'.freeze
    TERMINUS_STRING = '╙─╜'.freeze
    RENDERED_VIEW_DURATION = 100
    TOTAL_RENDERED_VIEW_DURATION = RENDERED_VIEW_DURATION * 5

    def initialize(time_format: nil)
      super(time_format:, color_map: ColorMap.new(
        debug: CLEAR + TEXT_GRAY_400,
        info: CLEAR + TEXT_GRAY_100,
        warn: CLEAR + BG_YELLOW + TEXT_BLACK,
        error: CLEAR + DARK_BG_RED + TEXT_WHITE,
        fatal: CLEAR + BG_MAGENTA + BOLD + TEXT_WHITE
      ))
      @memory = nil
    end

    def call(log, logger)
      # puts 'ok2'
      log = alter(log)

      self.log = log
      self.logger = logger
      self.color = color_map[log.level]

      computed_exception = exception
      result = wrap_level(compute_useful_length, message, payload, computed_exception)
      return result unless computed_exception

      result << "\n" << build_terminus
    rescue StandardError => e
      puts "Error during formatting: #{e.message}"
      puts e.backtrace.join("\n")
    end

    private

    def build_regex_redirected
      dev_port = Rails.application.routes.default_url_options[:port]
      dev_hosts = ['127.0.0.1', 'localhost', Rails.application.routes.default_url_options[:host]].uniq.join('|')
      dev_from = "http://(?:#{dev_hosts}):#{dev_port}"
      regex_s = "^(Redirected to )#{dev_from}(.*)"
      Regexp.new(regex_s)
    end

    def two_captures_last_as_bold(message, regex)
      match = message.match(regex)
      return "unmatched: #{message}" unless match

      m1, m2 = match.captures
      "#{m1}#{BOLD}#{m2}#{CLEAR}"
    end

    def alter(log)
      if log.name =~ /^(ActionView|ActiveRecord)::Base/
        log.level = :debug
        log.message.lstrip!
        if log.name == 'ActiveRecord::Base'
          unbold!(log.message)

          if log.message.starts_with?('↳ ')
            log.message = ansi_trace(log.message, '⇄')
          else
            sql_match = log.message.match(/\s+\[\[.*\]\]$/)
            if sql_match
              sql_command = sql_match.pre_match
              sql_type, sql_entry = sql_command.split("\e[0m")
              sql_color_entry = sql_entry.match(ANSI_REGEX).to_s
              sql_args = JSON.parse(sql_match.to_s).map(&:last)
              sql_args.each_with_index do |val, index|
                sql_arg = val.inspect
                sql_arg = "'#{val.gsub("'", "''")}'" if val.is_a?(String)
                sql_entry.gsub!("$#{index + 1}", colorize(sql_arg, BOLD) + sql_color_entry)
              end
              log.message = [sql_type, sql_entry].join(CLEAR)
            end
          end
        elsif log.name == 'ActionView::Base'
          match = log.message.match(/^Rendered( layout| collection of|) (.*?\.erb)(.*)(\(Duration:.*\))/)
          if match
            m1, m2, m3, m4 = match.captures
            duration = m4.match(/Duration: (\d+\.?\d*)ms/).match(1).to_f
            duration = duration < RENDERED_VIEW_DURATION ? '' : m4.to_s
            log.message = "⇤ Rendered#{m1} app/views/#{m2}#{m3}#{duration}"
          end
        end

      end

      log
    end

    def draw_fatal(char = CHAR_FATAL) = BG_MAGENTA + BOLD + TEXT_WHITE + char + CLEAR
    def build_prefix(char) = "#{tags} #{origin} ╣#{colorize(char)}╠  "
    def build_terminus = "#{tags} #{origin} #{TERMINUS_STRING}  "

    def build_dimensions(dimensions)
      "#{tags} #{origin} #{dimensions.rails}  "
    end

    def compute_useful_length
      IO.console.winsize[1] - FOREMAN_PREFIX_LENGTH
    rescue StandardError
      FAILOVER_WRAP
    end

    def unbold!(text) = text.gsub!(BOLD, '')

    def message
      colorize(log.message) if log.message
    end

    def payload
      return unless log.payload

      log.payload.ai(indent: 2, object_id: false)
    end

    def level_char
      case log.level
      when :info, :debug then ' '
      else log.level.to_s.chr.upcase
      end
    end

    def wrap_level(length, *items)
      prefix = log.dimensions ? build_dimensions(log.dimensions) : build_prefix(level_char)
      continuation = build_prefix('┆')

      result = items.map do |item|
        ::Semantic::AnsiWrapper.wrap(item, length, prefix, continuation)
      end

      if log.dimensions&.terminus
        terminus = ::Semantic::AnsiWrapper.wrap(' ', length, build_terminus)
        result << terminus
      end

      log.dimensions&.before&.times { result.unshift('') }
      log.dimensions&.after&.times { result << '' }
      result.compact.join("\n")
    end
  end
end
