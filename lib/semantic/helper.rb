module Semantic
  # useful public functions
  module Helper
    class << self
      include AnsiColors
      def stackisize(*items, symbol: '➟')
        return if items.empty?

        Rails.backtrace_cleaner
             .clean(items)
             .map { |item| ansi_trace(item, symbol) }
             .join("\n")
      end

      private

      def ansi_trace(trace, symbol)
        match = trace.match(/(↳ )?(.*:\d+)(:in `)?(.*'?)/) # only m2(=file) and m4(=optional function) are useful
        return trace unless match

        _, m2, _, m4 = match.captures
        "#{symbol} #{m2} #{colorize(m4.chop, BOLD)}"
      end
    end
  end
end
