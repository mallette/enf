namespace :participants do
  desc 'Search duplicates participants'

  task :search_for_duplicates, %i[classroom] => :environment do |_, args|
    classroom_id = args.fetch(:classroom, nil)&.to_i
    classroom = Classroom.find(classroom_id)

    users = {}
    classroom.learnings.each do |l|
      users[l.user.id] ? users[l.user.id] << l.id : users[l.user.id] = [l.id]
    end
    duplicates = users.select { |_, value| value.count > 1 }
    duplicates.each do |user_id, learning_ids|
      user = User.find user_id
      logger.info "Duplicate user <#{user.name}> :"
      old = Learning.find(learning_ids.min).participant
      new = Learning.find(learning_ids.max).participant
      logger.info "   -> Participant #{old.id}"
      logger.info "   -> Participant #{new.id}"
      logger.info "  Merge participant #{old.id} to #{new.id}..."
      merge_shuttle_forms(old, new)
      merge_folders(old, new)
      merge_visios(old, new)
      merge_memos(old, new)
      merge_pads(old, new)
      merge_audits(old, new)

      logger.info "Destroy old participant #{old.id}..."
      old.destroy

      logger.info 'Renamme participant personnal folder...'
      new.classroom.folders.find_by(participant: new).update name: new.name
    end
  end

  def merge_shuttle_forms(old_participant, new_participant)
    logger.info '  Merge shuttle forms...'
    old_participant.classroom.teaching_units.each do |teaching_unit|
      old_sf = old_participant.participable.shuttle_forms.find_by(teaching_unit:)
      new_sf = new_participant.participable.shuttle_forms.find_by(teaching_unit:)
      next unless old_sf && new_sf

      logger.info "   -> merging shuttle form <#{old_sf.id}> to <#{new_sf.id}>"
      new_sf.learner_theory1 = "#{old_sf.learner_theory1}\n#{new_sf.learner_theory1}" if old_sf.learner_theory1
      new_sf.learner_theory2 = "#{old_sf.learner_theory2}\n#{new_sf.learner_theory2}" if old_sf.learner_theory2
      new_sf.learner_theory3 = "#{old_sf.learner_theory3}\n#{new_sf.learner_theory3}" if old_sf.learner_theory3
      new_sf.learner_theory4 = "#{old_sf.learner_theory4}\n#{new_sf.learner_theory4}" if old_sf.learner_theory4
      new_sf.learner_theory5 = "#{old_sf.learner_theory5}\n#{new_sf.learner_theory5}" if old_sf.learner_theory5
      new_sf.teacher_theory1 = "#{old_sf.teacher_theory1}\n#{new_sf.teacher_theory1}" if old_sf.teacher_theory1
      new_sf.learner_training1 = "#{old_sf.learner_training1}\n#{new_sf.learner_training1}" if old_sf.learner_training1
      new_sf.learner_training2 = "#{old_sf.learner_training2}\n#{new_sf.learner_training2}" if old_sf.learner_training2
      new_sf.learner_training3 = "#{old_sf.learner_training3}\n#{new_sf.learner_training3}" if old_sf.learner_training3
      new_sf.learner_training4 = "#{old_sf.learner_training4}\n#{new_sf.learner_training4}" if old_sf.learner_training4
      new_sf.learner_training5 = "#{old_sf.learner_training5}\n#{new_sf.learner_training5}" if old_sf.learner_training5
      new_sf.learner_training6 = "#{old_sf.learner_training6}\n#{new_sf.learner_training6}" if old_sf.learner_training6
      new_sf.tutor_training1 = "#{old_sf.tutor_training1}\n#{new_sf.tutor_training1}" if old_sf.tutor_training1
      new_sf.tutor_training2 = "#{old_sf.tutor_training2}\n#{new_sf.tutor_training2}" if old_sf.tutor_training2
      new_sf.tutor_training3 = "#{old_sf.tutor_training3}\n#{new_sf.tutor_training3}" if old_sf.tutor_training3
      new_sf.tutor_training4 = "#{old_sf.tutor_training4}\n#{new_sf.tutor_training4}" if old_sf.tutor_training4
      new_sf.tutor_training5 = "#{old_sf.tutor_training5}\n#{new_sf.tutor_training5}" if old_sf.tutor_training5
      new_sf.tutor_training6 = "#{old_sf.tutor_training6}\n#{new_sf.tutor_training6}" if old_sf.tutor_training6
      new_sf.teacher_training1 = "#{old_sf.teacher_training1}\n#{new_sf.teacher_training1}" if old_sf.teacher_training1

      new_sf.save
    end
  end

  def merge_folders(old_participant, new_participant)
    logger.info '  Merge folders...'
    old_folder = old_participant.classroom.folders.find_by(participant: old_participant)
    new_folder = new_participant.classroom.folders.find_by(participant: new_participant)

    old_folder.children.each do |child|
      new_gran_child_parent = new_folder.children.find_by name: child.name
      child.children.each do |gran_child|
        logger.info "   -> moving folder <#{gran_child.name}>"
        gran_child.update parent: new_gran_child_parent
      end
      child.files.each do |file|
        logger.info "   -> moving file <#{file.filename}>"
        file.update record: new_gran_child_parent
      end
    end
  end

  def merge_visios(old_participant, new_participant)
    logger.info '  Merge visios...'
    Visio.where(participant: old_participant).each do |visio|
      visio.participant = new_participant
      visio.save
    end
  end

  def merge_memos(old_participant, new_participant)
    logger.info '  Merge memos...'
    Memo.where(participant: old_participant).each do |memo|
      memo.update participant: new_participant
    end
  end

  def merge_pads(old_participant, new_participant)
    logger.info '  Merge pads...'
    Pad.where(participant: old_participant).each do |pad|
      pad.update participant: new_participant
    end
  end

  def merge_audits(old_participant, new_participant)
    logger.info '  Merge audits...'
    Audited::Audit.where(user_id: old_participant.id).each do |audit|
      audit.update user_id: new_participant.id
    end
  end
end
