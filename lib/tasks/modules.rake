namespace :modules do
  include SemanticLogger::Loggable

  desc "Ajout d'une instance à toutes les promos"
  task add_instance_to_all_classrooms: :environment do
    add_instance_to_all_classrooms
  end

  desc "Activation d'un module pour tous les utilisateurs"
  task activate_module_for_all_users: :environment do
    activate_module_for_all_users
  end

  def add_instance_to_all_classrooms
    logger.info "Ajout d'une nouvelle instance à toutes les promos"

    module_id = choose_module_from_terminal
    instance_id = choose_instance_from_terminal(module_id)
    instance = Instance.find(instance_id)
    logger.error "instance <#{instance_id}> not found!" && exit unless instance
    perform_add_instance_to_all_classrooms(module_id, instance_id)
  end

  def perform_add_instance_to_all_classrooms(module_id, instance_id)
    Classroom.all.each do |classroom|
      if DomainInfo.where(config_module_id: module_id, classroom_id: classroom.id).count.positive?
        logger.warn "#{classroom.code} : Service déjà actif pour ce domaine"
      elsif DomainInfo.create(config_module_id: module_id, classroom_id: classroom.id, instance_id:)
        logger.info "#{classroom.code} : Ajout de l'instance pour ce domaine"
      else
        logger.error "#{classroom.code} : Erreur lors de l'ajout de l'instance"
      end
    end
  end

  def activate_module_for_all_users
    logger.info "Ajout d'un service à tous les utilisateurs"

    module_id = choose_module_from_terminal
    cm = ConfigModule.find(module_id)
    logger.error "moduleinstance <#{module_id}> not found!" && exit unless cm
    perform_activate_module_for_all_users(module_id)
  end

  def perform_activate_module_for_all_users(module_id)
    Classroom.all.each do |classroom|
      classroom.learners.each do |user|
        user.classroom_user_config_modules.find_or_create_by(classroom_id: classroom.id,
                                                             config_module_id: module_id)
        logger.info "Activation pour le stagiaire <#{user.email}>"
      end
      classroom.teachings.each do |t|
        t.user.classroom_user_config_modules.find_or_create_by(classroom_id: classroom.id,
                                                               config_module_id: module_id)
        logger.info "Activation pour le formateur <#{t.user.email}>"
      end
      classroom.tutors.each do |user|
        user.classroom_user_config_modules.find_or_create_by(classroom_id: classroom.id,
                                                             config_module_id: module_id)
        logger.info "Activation pour le tuteur <#{user.email}>"
      end
    end
  end

  private

  def choose_module_from_terminal
    puts 'Veuillez choisir le service concerné parmi la liste :'
    configmodules = ConfigModule.all.order(:id)
    configmodules.each do |mod|
      puts "#{mod.id}. #{mod.name}"
    end
    print 'Votre choix (un entier): '
    gets.chomp.to_i
  end

  def choose_instance_from_terminal(module_id)
    puts 'Veuillez choisir une instance :'
    Instance.where(config_module_id: module_id).each do |instance|
      puts "#{instance.id}. #{instance.name}"
    end
    gets.chomp.to_i
  end
end
