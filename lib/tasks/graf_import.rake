namespace :graf do
  desc "Populate classroom from Graf. \n\
two optional arguments may be provided: \n\
- classroom: integer # for quick import of ONE classroom\n\
- notification: boolean # to send report notification by email, default: false\n\

example:\n\
rails \"graf:import\"\n\
rails \"graf:import[18587]\"\n\
rails \"graf:import[18587,true]\""

  task :import, %i[classroom notification] => :environment do |_, args|
    classroom_id = args.fetch(:classroom, nil)&.to_i
    notification = args.fetch(:notification, false)

    Subtasks::GrafImporter.run(
      asynchronous: false,
      notification:,
      classroom_id:
    )
  end
end
