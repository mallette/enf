namespace :fixture_generator do
  desc 'generate fixtures for a given sql query from the current development database'

  task :fixture_generator, %i[sql file_name] => :environment do |t, args|
    args.with_defaults(sql: nil, file_name: nil)
    p "creating fixture - #{args.file_name}"
    File.open("#{Rails.root}/test/fixtures/#{args.file_name}.yml", 'a+') do |file|
      data = ActiveRecord::Base.connection.select_all(args.sql)
      file.write data.each_with_index.with_object({}) { |(record, index), hash|
        hash[index.zero? ? 'one' : 'two'] = record
      }.to_yaml
    end
  end
end
