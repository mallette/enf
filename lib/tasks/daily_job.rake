namespace :daily_job do
  desc 'Force daily_job to perform now'
  task :now, %i[notification] => :environment do |_, args|
    notification = args.fetch(:notification, false)
    DailyJob.perform_now(asynchronous: false, notification:)
  end
end
