module Subtasks
  module GrafImporter
    include SemanticLogger::Loggable
    include AbstractSubtask

    class << self
      def description = 'import all classrooms from GRAF Api'

      def run(asynchronous: true, notification: true, classroom_id: nil)
        logger.debug("asynchronous=#{asynchronous}, notification=#{notification}")
        logger.warn("quick import for classroom <#{classroom_id}>") if classroom_id

        @global_messages = []
        @director_per_classroom_id = {}
        @contact_per_classroom = {}
        @messages_per_classroom = {}

        graf = Services::Graf.new
        classrooms_ids = classroom_id.nil? ? fetch_classrooms(graf) : Array.wrap(classroom_id)

        Parallel.each(classrooms_ids, in_threads: Rails.configuration.x_option(:max_threads)) do |classroom_id|
          classroom_node = graf.classroom(classroom_id)

          study_director_email = if classroom_node['studyDirector']
                                   classroom_node['studyDirector']['emailAddress']['address']
                                 else
                                   nil
                                 end
          @director_per_classroom_id[classroom_id] = study_director_email
          logger.debug("emailAddress of studyDirector for classroom graf_id=#{classroom_id} is: #{study_director_email}")

          unless classroom_node
            @global_messages << "Impossible de trouver le classroom_node <#{classroom_id}>"
            next
          end
          school_id = create_or_update_school(classroom_node['company'])
          unless classroom_node['skillsBlock']
            @global_messages << "Pas de bloc de compétences pour la promo <#{classroom_node['identifier']}>... on passe"
            next
          end
          skill_block_id = create_or_update_skill_block(classroom_node['skillsBlock'])
          classroom = create_or_update_classroom(classroom_node, school_id, skill_block_id)
          @messages_per_classroom[classroom] = []
          create_or_update_skill_units(graf.credits_by_skill_block(classroom_node['skillsBlock']['id']), classroom.id,
                                       skill_block_id)

          create_or_update_teacher(classroom, classroom_node['studyDirector'])
          create_or_update_contributors(graf, classroom)

          registrations = classroom_node['attendees'].select do |a|
            a['type'].zero? and [2, 3].include?(a['status'])
          end
          import_registrations(graf, registrations, classroom, skill_block_id)

          teaching_units = graf.teaching_units(classroom.graf_id)
          create_or_update_teaching_units(teaching_units, classroom) if teaching_units

          teaching_sequences_nodes = graf.teaching_sequences(classroom)
          create_or_update_teaching_sequences(teaching_sequences_nodes) if teaching_sequences_nodes
        rescue StandardError => e
          if @messages_per_classroom[classroom]
            @messages_per_classroom[classroom] << "Impossible de créer la promo <#{classroom_node.is_a?(Hash) && classroom_node['identifier']}> : #{e}"
          else
            logger.error "Impossible de créer la promo <#{classroom_id}>"
          end
        end

        report(asynchronous, notification)
      end

      private

      def report(asynchronous, notification)
        logger.debug("report notification=#{notification}")
        unless @global_messages.empty?
          logger.error('=========GLOBAL========')
          @global_messages.each { |message| logger.error(message) }
          logger.error('=================')
        end
        # FIXME: notification to ? for global_messages

        @messages_per_classroom.each do |classroom, messages|
          next if messages.empty?

          # TODO: à activer seulement quand les Cemea seront prêt
          # email = @contact_per_classroom[classroom]&.email
          # if email.nil?
          #   responsible_found = false # FIXME: to be tested!!!!
          #   email = @director_per_classroom_id[classroom.graf_id]
          # else
          #   responsible_found = true
          # end
          email = 'enf@cemea.asso.fr'
          responsible_found = true

          logger.error("--------CLASSROOM <#{classroom.code}>---------------------")
          messages.each { |message| logger.error(message) }
          logger.error('-----------------------------------------------------')

          next unless notification

          logger.info("send notification to <#{email}> as responsible=<#{responsible_found}>")
          mail = GrafImportNotifier.emit(email, responsible_found, classroom, messages)
          asynchronous ? mail.deliver_later : mail.deliver_now
        end
      end

      def fetch_classrooms(graf)
        logger.measure_info 'Fetch classrooms from graf' do
          classroom_to_import = Live::Constants::CLASSROOM_TO_IMPORT
          if Rails.env.development? && classroom_to_import.positive?
            logger.warn('importation sélective depuis CLASSROOM_TO_IMPORT', classroom_to_import)
            classrooms_ids = Array.wrap(classroom_to_import)
          else
            classrooms_ids = graf.classrooms(only_enf: true).map { |c| c['id'] }
            graf_classrooms_yml = Rails.root.join('config', 'graf_classrooms.yml')
            classrooms_ids += YAML.load_file(graf_classrooms_yml) if File.exist?(graf_classrooms_yml)
            logger.warn("importation de #{classrooms_ids.size} promos")
          end
          return classrooms_ids
        end
      end

      def import_registrations(graf, registrations, classroom, skill_block_id)
        registrations.each do |registration|
          node = { 'emailAddress' => { 'address' => registration['emailAddress'] ? registration['emailAddress']['address'] : nil },
                   'firstName' => registration['attendee']['firstName'],
                   'name' => registration['attendee']['name'],
                   'id' => registration['attendee']['id'] }
          user = create_or_update_user(classroom, node)
          next unless user

          r = Learning.find_by(graf_id: registration['id'])
          unless r&.participant
            logger.info "Création de l'inscription de <#{user.name}>"
            r = Learning.new(graf_id: registration['id'], skill_block_id:)
            Participant.create(user_id: user.id, classroom_id: classroom.id, participable: r)
          end
          import_tutors(graf, r)
        end
      end

      def import_tutors(graf, registration)
        rotations = graf.rotation_location(registration.graf_id)
        return unless rotations

        rotations.each do |rotation|
          next unless rotation['tutor']

          node = { 'emailAddress' => { 'address' => rotation['tutorEmailAddress'] },
                   'firstName' => rotation['tutor.firstName'],
                   'name' => rotation['tutor.birthName'],
                   'id' => rotation['tutor.id'] }
          user = create_or_update_user(registration.classroom, node)
          create_or_update_tutoring(user, registration, rotation) if user
        end
      end

      def create_or_update_tutoring(user, registration, rotation)
        tutoring = Participant.find_by(participable_type: 'Tutoring', user:,
                                       classroom: registration.classroom)&.participable
        if tutoring
          logger.info "Tutoring from tutor <#{user.name}> for student <#{registration.user.name}> already exist. Update if needed."
          tutoring.update(start_date: rotation['startDate'], end_date: rotation['endDate'])
          tutoring.learnings << registration unless tutoring.learnings.include?(registration)
        else
          logger.info "Create tutoring from tutor <#{user.name}> for student <#{registration.user&.name}>."
          tutoring = Tutoring.new(graf_id: rotation['id'],
                                  start_date: rotation['startDate'], end_date: rotation['endDate'])
          Participant.create(classroom: registration.classroom, user_id: user.id,
                             participable: tutoring)
          tutoring.learnings << registration
        end
        tutoring.fix_folders_permissions
        tutoring
      end

      def create_or_update_user(classroom, user_node)
        return unless user_node

        email = user_node['emailAddress'] ? user_node['emailAddress']['address'] : ''
        if email.to_s.empty?
          @messages_per_classroom[classroom] << "L'utilisateur #{user_node['name']} #{user_node['firstName']} (#{user_node['id']}) n'a pas d'adresse " \
                                                'mail renseignée sur Graf. Import impossible.'
          return
        end

        first_name = user_node['firstName']
        last_name = user_node['name']
        graf_id = user_node['id']
        user = User.find_by(graf_id:)
        if user
          logger.info "User <#{user.name}> already exist. Update if needed."
          user.update(email:, first_name:, last_name:)
          user
        else
          logger.info "Create user <#{email}>."
          password = SecureRandom.hex
          User.create(email:, first_name:, last_name:, graf_id:, password:, password_confirmation: password)
        end
      end

      def create_or_update_teacher(classroom, teacher_node)
        unless teacher_node
          @messages_per_classroom[classroom] << "Aucun responsable pédagogique pour la promo <#{classroom.code}>"
          return
        end
        user = create_or_update_user(classroom, teacher_node)

        logger.debug "responsible teacher <#{user.name} #{user.email}> found for classroom <#{classroom.code}>"
        @contact_per_classroom[classroom] = user

        teacher_participant = Participant.create_with(participable: Teaching.new).find_or_create_by(classroom:, user:,
                                                                                                    participable_type: 'Teaching')
        teacher_participant.participable.update(responsible: true)
      end

      def create_or_update_contributors(graf, classroom)
        contributors_nodes = graf.contributors(classroom)
        return unless contributors_nodes

        contributors_nodes.each do |contributor_node|
          next unless contributor_node['enfCoResponsible']

          user_node = graf.user(contributor_node['partner']['id'])
          user = create_or_update_user(classroom, user_node)
          next if Participant.find_by(classroom:, user:, participable_type: 'Teaching')

          Participant.create(classroom:, user:, participable: Teaching.new)
        end
      end

      def create_or_update_teaching_units(tu_nodes, classroom)
        index = 0

        rejected, accepted = tu_nodes.partition { |tu| tu['startDateT'].nil? || tu['endDateT'].nil? }
        rejected.each do |tu|
          @messages_per_classroom[classroom] <<
            "Unité pédagogique <#{tu['name']}> ne contient pas de date de début et/ou de fin => import impossible !"
        end

        accepted.sort { |a, b| a['startDateT'] <=> b['startDateT'] }.each do |tu|
          index += 1
          name = sanitize_teaching_unit_name tu['name']
          start_date = tu['startDateT']
          end_date = tu['endDateT']

          previous_teaching_unit = TeachingUnit.where('classroom_id = ? and start_date < ?', classroom.id,
                                                      start_date).order('start_date DESC').first
          if previous_teaching_unit
            logger.info('set training_end_date from previous last teaching unit with start_date: ', start_date)
            previous_teaching_unit.update(training_end_date: start_date)
          elsif index > 1
            @report[classroom] <<
              'probable chevauchement entre les unités pédagogiques qui ne se terminent pas en cascade. ' \
              'Il faut impérativement corriger depuis Graf!'
            # break
          end

          teaching_unit = TeachingUnit.find_by(graf_id: tu['id'])
          if teaching_unit
            teaching_unit.update(name:, start_date:, end_date:)
          else
            TeachingUnit.create(name:, start_date:, end_date:, classroom:,
                                graf_id: tu['id'])
          end
        end
        delete_old_teaching_units(tu_nodes, classroom)
        update_last_teaching_unit_with_classroom_endate(classroom)
      end

      def update_last_teaching_unit_with_classroom_endate(classroom)
        previous_teaching_unit = TeachingUnit.where(classroom:).order('start_date DESC').first
        return unless previous_teaching_unit

        training_end_date = classroom.end_date
        logger.info('set teaching_end_date from classroom.end_date: ', training_end_date)
        previous_teaching_unit&.update(training_end_date:)
      end

      def delete_old_teaching_units(tu_nodes, classroom)
        graf_teaching_units = tu_nodes.map { |tu| tu['id'] }
        bichik_teaching_units = TeachingUnit.where(classroom:).map(&:graf_id)
        teaching_units_to_delete = bichik_teaching_units - graf_teaching_units
        TeachingUnit.where(id: teaching_units_to_delete).destroy_all
      end

      def create_or_update_school(company)
        School.upsert({ name: company['name'], graf_id: company['id'] }, unique_by: :graf_id).rows.first.first
      end

      def create_or_update_skill_block(skill_block_node)
        block = SkillBlock.find_by(graf_id: skill_block_node['id'])
        block ||= SkillBlock.create(graf_id: skill_block_node['id'])
        block.id
      end

      def create_or_update_skill_units(skill_units_node, classroom_id, skill_block_id)
        skill_units_node.each do |skill_unit_node|
          su = SkillUnit.find_by(graf_id: skill_unit_node['id'])
          next if su

          SkillUnit.create(graf_id: skill_unit_node['id'],
                           code: skill_unit_node['code'],
                           classroom_id:,
                           skill_block_id:)
        end
      end

      def create_or_update_classroom(classroom_node, school_id, skill_block_id)
        code = classroom_node['identifier']
        name = classroom_node['name']
        id = classroom_node['id']
        start_date = classroom_node['startDateTime']
        end_date = classroom_node['endDateTime']

        raise 'Invalid classroom code' if code.include?('/')
        raise 'Invalid classroom name' if name.include?('/')

        classroom = Classroom.find_by(graf_id: id)

        if classroom
          classroom.update!(code:, school_id:, skill_block_id:, quota: 50, name:,
                            start_date:, end_date:)
        else
          classroom = Classroom.create graf_id: id, code:, school_id:, skill_block_id:, quota: 50, name:,
                                       start_date:, end_date:
        end
        classroom
      end

      def create_or_update_teaching_sequences(teaching_sequences_nodes)
        teaching_sequences_nodes.each do |teaching_sequences_node|
          teaching_sequence = TeachingSequence.find_by(graf_id: teaching_sequences_node['id'])
          if teaching_sequence
            teaching_sequence.update name: teaching_sequences_node['name'],
                                     description: teaching_sequences_node['description'],
                                     started_at: teaching_sequences_node['startDateT'],
                                     ended_at: teaching_sequences_node['endDateT']
          else
            teaching_unit = TeachingUnit.find_by(graf_id: teaching_sequences_node['teachingUnit']['id'])
            skill_unit = SkillUnit.find_by(graf_id: teaching_sequences_node['credit']['id'])

            if teaching_unit && skill_unit
              TeachingSequence.create name: teaching_sequences_node['name'],
                                      description: teaching_sequences_node['description'],
                                      graf_id: teaching_sequences_node['id'],
                                      started_at: teaching_sequences_node['startDateT'],
                                      ended_at: teaching_sequences_node['endDateT'],
                                      teaching_unit:,
                                      skill_unit:
            end
          end
        end
      end

      def sanitize_teaching_unit_name(name)
        name.gsub('/', '-').gsub('\\', '-')
      end
    end
  end
end
