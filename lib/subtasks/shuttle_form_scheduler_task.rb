module Subtasks
  module ShuttleFormSchedulerTask
    include SemanticLogger::Loggable
    include Subtasks::AbstractSubtask

    class << self
      def description = 'process every pending shuttleform and switches learner_status, teacher_status, tutor_status'

      # see: doc/diagrams/fiche_navette.md
      def run(asynchronous: true, notification: true)
        @asynchronous = asynchronous
        @notification = notification
        schedule1
        schedule2
        schedule3
        schedule4
        schedule5
      end

      private

      ## please have a look at [docs/diagram.md|ShuttleForm Workflow]
      def schedule1
        logger.info('schedule1 running...')
        sfs = ShuttleForm
              .where(learner_status: :learner_inactive)
              .joins(:teaching_unit)
              .where('teaching_units.start_date <= ?', Date.today)
        sfs.each(&:learner_theory_update!)
        logger.info("schedule1 has updated <#{sfs.size}> ShuttleForms")
      end

      def schedule2
        logger.info('schedule2 running...')
        sfs = ShuttleForm
              .where(learner_status: %i[learner_theory_update learner_theory_signed])
              .joins(:teaching_unit)
              .where('teaching_units.end_date <= ?', Date.today - 2.days)
        sfs.each do |sf|
          sf.learner_training_update!
          sf.update learner_theory_signed_at: DateTime.now unless sf.learner_theory_signed_at?
          sf.teacher_theory_update! unless sf.teacher_theory_signed?
          sf.tutor_update!
        end
        logger.info("schedule2 has updated <#{sfs.size}> ShuttleForms")
      end

      def schedule3
        logger.info('schedule3 running...')
        sfs = ShuttleForm
              .where(teacher_status: :teacher_theory_update)
              .joins(:teaching_unit)
              .where('teaching_units.end_date <= ?', Date.today - 7.days)
        sfs.each do |sf|
          sf.update(teacher_theory_signed_at: DateTime.now, teacher_theory_signed_by_id: nil,
                    teacher_status: :teacher_theory_signed)
        end
        logger.info("schedule3 has updated <#{sfs.size}> ShuttleForms")
      end

      def schedule4
        logger.info('schedule4 running...')
        count = 0
        ShuttleForm
          .where(learner_status: ShuttleForm.learner_statuses[:learner_theory_update])
          .or(ShuttleForm.where(tutor_status: ShuttleForm.tutor_statuses[:tutor_update]))
          .each do |sf|
          next_start_date = sf.next&.teaching_unit&.start_date || sf.classroom.end_date
          next unless next_start_date <= Date.today

          count += 1
          sf.update(learner_status: :learner_training_signed,
                    tutor_status: :tutor_signed,
                    learner_training_signed_at: DateTime.now,
                    tutor_training_signed_at: DateTime.now,
                    tutor_training_signed_by_id: nil)
          sf.update learner_theory_signed_at: DateTime.now unless sf.learner_theory_signed_at
        end

        logger.info("schedule4 has updated <#{count}> ShuttleForms")
      end

      def schedule5
        logger.info('schedule5 running...')
        count = 0
        ShuttleForm
          .where(teacher_status: :teacher_training_update)
          .each do |sf|
          next_start_date = sf.next&.teaching_unit&.start_date || sf.classroom.end_date
          next unless next_start_date <= Date.today - 1.day

          count += 1
          sf.update(teacher_status: :teacher_training_signed,
                    teacher_training_signed_at: DateTime.now,
                    teacher_training_signed_by_id: nil)
        end
        logger.info("schedule5 has updated <#{count}> ShuttleForms")
      end
    end
  end
end
