module Subtasks
  module AbstractSubtask
    def run(asynchronous: true, notification: true)
      raise NotImplementedError,
            'You should implement the abstract method `def run(asynchronous: true, notification: true):nil`'
    end

    def description
      raise NotImplementedError,
            'You should implement the abstract method `def description():String`'
    end
  end
end
