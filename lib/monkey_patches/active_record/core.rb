module ActiveRecord::Core
  def pretty_print(pp)
    return super if custom_inspect_method_defined?

    pp.object_address_group(self) do
      if @attributes

        # BEFORE:
        # attr_names = attributes_for_inspect.select { |name| _has_attribute?(name.to_s) }
        attr_names = attribute_names
        # END

        pp.seplist(attr_names, proc { pp.text ',' }) do |attr_name|
          attr_name = attr_name.to_s
          pp.breakable ' '
          pp.group(1) do
            pp.text attr_name
            pp.text ':'
            pp.breakable
            value = attribute_for_inspect(attr_name)
            pp.text value
          end
        end
      else
        pp.breakable ' '
        pp.text 'not initialized'
      end
    end
  end
end
