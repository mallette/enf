# MonkeyPatch of SemanticLogger
# provides better handling for payload regardless of message
module SemanticLogger
  class Base # rubocop:disable Style/Documentation
    # Log message at the specified level
    def log_internal(level, index, message = nil, payload = nil, exception = nil) # rubocop:disable Metrics/AbcSize,Metrics/CyclomaticComplexity,Metrics/MethodLength,Metrics/PerceivedComplexity
      # Handle variable number of arguments by detecting exception object and payload hash.
      if exception.nil? && payload.nil? && message.respond_to?(:backtrace) && message.respond_to?(:message)
        exception = message
        message   = nil
      elsif exception.nil? && payload && payload.respond_to?(:backtrace) && payload.respond_to?(:message)
        exception = payload
        payload   = nil
      elsif message && !message.is_a?(String)
        payload = message
        message = 'payload is'
      end

      log = Log.new(name, level, index)
      should_log =
        if exception.nil? && payload.nil? && message.is_a?(Hash)
          # All arguments as a hash in the message.
          log.assign(**log.extract_arguments(message))
        elsif exception.nil? && message && payload && payload.is_a?(Hash)
          # Message supplied along with a hash with the remaining arguments.
          log.assign(**log.extract_arguments(payload, message))
        else
          # All fields supplied directly.
          log.assign(message:, payload:, exception:)
        end

      # Add result of block to message or payload if not nil
      if block_given?
        result = yield(log)
        if result.is_a?(String)
          log.message = log.message.nil? ? result : "#{log.message} -- #{result}"
        elsif result.is_a?(Hash)
          log.assign_hash(result)
        end
      end

      # Log level may change during assign due to :on_exception_level
      self.log(log) if should_log && should_log?(log)
    end
  end
end
