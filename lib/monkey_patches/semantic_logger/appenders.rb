module SemanticLogger
  class Appenders < Concurrent::Array
    def close
      closed_appenders = []
      each do |appender|
        logger.trace "Closing appender: #{appender.name}"

        # PVINCENT's change
        closed_appenders << appender # closed_ missing!!!!
        # END OF change

        appender.flush
        appender.close
      rescue StandardError => e
        logger.error "Failed to close appender: #{appender.name}", e
      end
      closed_appenders.each { |appender| delete(appender) }
      logger.trace 'All appenders closed and removed from appender list'
    end
  end
end
