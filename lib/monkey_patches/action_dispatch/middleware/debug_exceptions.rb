require 'action_dispatch/middleware/exception_wrapper'
require 'action_dispatch/routing/inspector'
require 'action_view'

module ActionDispatch
  # THIS IS A HACK TO unwrap TagWrapError when detected!
  # TagWrapError contains a 'tag' for SemanticLogger and the cause itself
  class DebugExceptions
    private

    def render_exception(request, exception, wrapper)
      ## PVINCENT's ADDITION for TagWrapError

      log_error(request, wrapper)
      if exception.is_a?(Semantic::TagWrapError)
        backtrace_cleaner = request.get_header('action_dispatch.backtrace_cleaner')
        wrapper = ExceptionWrapper.new(backtrace_cleaner, exception.error)
      end
      ## HERE it was commented off
      # log_error(request, wrapper)
      ## END OF ADDITION

      raise exception unless request.get_header('action_dispatch.show_detailed_exceptions')

      begin
        content_type = request.formats.first
      rescue ActionDispatch::Http::MimeNegotiation::InvalidType
        content_type = Mime[:text]
      end
      if api_request?(content_type)
        render_for_api_request(content_type, wrapper)
      else
        render_for_browser_request(request, wrapper)
      end
    end
  end
end
