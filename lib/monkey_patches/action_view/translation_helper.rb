require 'action_view/helpers/tag_helper'
require 'active_support/core_ext/string/access'
require 'i18n/exceptions'

# WARNING: this a monkey patch, rails restart required after any changes!
module ActionView
  module Helpers
    module TranslationHelper
      # saving original t method
      alias t_orig t

      def t(key, **options)
        key, options = t2_normalize_key_scope(key, **options)
        options[:safe] ? I18n.t(key, **options) : t_orig(key, **options)
      end

      def ts(key, **options)
        t(key, **options.merge(safe: true))
      end

      private

      def t2_normalize_key_scope(key, **options)
        options[:scope] ||= []
        if key.is_a? String
          # detect whether scope is absolute or relative
          scope = key.strip.delete_prefix('.').split('.')
          key = scope.pop
          scope = virtual_path_as_scope if scope.count.zero?
        else
          # as symbol infers relative scope
          scope = virtual_path_as_scope
        end
        options[:scope] += scope

        [key, options]
      end

      # re-use the magical @virtual_path provided by Rails
      def virtual_path_as_scope
        @virtual_path.nil? ? Array.wrap(@_lookup_context.prefixes.first) : @virtual_path.gsub(%r{/_?}, '.').split('.')
      end
    end
  end
end
