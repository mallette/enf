module SemanticLogger
  def self.fast_tag(tag)
    return yield if tag.nil? || tag == ''

    t = Thread.current[:semantic_logger_tags] ||= []
    begin
      t << tag
      yield
    # PVINCENT's addition
    rescue StandardError => e
      raise Semantic::TagWrapError.new(tag, e)
    # END OF addition
    ensure
      t.pop
    end
  end
end
