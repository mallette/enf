module RailsLiveReload
  # MonkeyPath Watcher
  class Watcher
    def initialize
      @files = {}
      @sockets = []

      # puts "Watching AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA: #{root}"
      # RailsLiveReload.patterns.each do |pattern, rule|
      #   puts "  #{pattern} => #{rule}"
      # end

      build_tree
      start_socket
      start_listener
    end
  end
end
