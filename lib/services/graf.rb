module Services
  class Graf
    include SemanticLogger::Loggable
    HTTP_SUCCESS = 200

    def initialize
      config = Rails.configuration
      @url = config.x_option :graf_url
      @jsessionid = login(config.x_option(:graf_username), config.x_option(:graf_password))
    end

    def classrooms(only_enf: false)
      # On récupère dans Graf toutes les promos qui sont en cours et qui correspond aux identifiants B, F ou T
      logger.measure_debug 'classrooms' do
        data = {
          criteria: [{
            operator: 'and',
            criteria: [
              { fieldName: 'courseStatus', operator: '!=', value: 5 },
              { fieldName: 'courseStatus', operator: '!=', value: 1 },
              { fieldName: 'endDateTime',  operator: '>',  value: DateTime.now },
              # { fieldName: 'startDateTime', operator: '<', value: DateTime.now },
              {
                operator: 'or',
                criteria: [
                  { fieldName: 'identifier', operator: 'like', value: '%-B%' },
                  { fieldName: 'identifier', operator: 'like', value: '%-F%' },
                  { fieldName: 'identifier', operator: 'like', value: '%-T%' }
                ]
              }
            ]
          }]
        }

        data[:criteria][0][:criteria] << { fieldName: 'activateEnf', operator: '=', value: true } if only_enf

        response = RestClient.post("#{@url}/ws/rest/com.axelor.apps.training.db.Course/search",
                                   JSON.generate(fields: %w[id identifier name updatedOn startDateTime endDateTime],
                                                 related: {
                                                   skillsBlock: %w[name id credits],
                                                   attendees: %w[attendee.birthName attendee.firstName updatedOn
                                                                 attendee.birthDate attendee.mobilePhone type
                                                                 emailAddressStr emailAddress.address status
                                                                 skillsBlock attendee.name rotationLocation],
                                                   studyDirector: %w[birthName name firstName updatedOn birthDate
                                                                     mobilePhone emailAddress.address],
                                                   contributors: %w[fullName enfCoResponsible],
                                                   company: %w[name id]
                                                 },
                                                 data:),
                                   cookies: { JSESSIONID: @jsessionid },
                                   content_type: 'application/json',
                                   accept: 'application/json')
        raise response unless response.code == HTTP_SUCCESS

        JSON.parse(response.body)['data'] || []
      rescue RestClient::ExceptionWithResponse => e
        logger.error "Unable to get classrooms : #{e.inspect}"
      end
    end

    def classroom(graf_id)
      # TODO: contributors ne fonctionne pas pour le moment.
      logger.measure_debug "classroom <#{graf_id}>" do
        response = RestClient.post("#{@url}/ws/rest/com.axelor.apps.training.db.Course/#{graf_id}/fetch",
                                   JSON.generate(fields: %w[id identifier name updatedOn startDateTime endDateTime activateEnf],
                                                 related: {
                                                   skillsBlock: %w[name id credits],
                                                   attendees: %w[attendee.birthName attendee.firstName updatedOn
                                                                 attendee.birthDate attendee.mobilePhone type
                                                                 emailAddressStr emailAddress.address status
                                                                 skillsBlock attendee.name rotationLocation],
                                                   studyDirector: %w[birthName name firstName updatedOn birthDate
                                                                     mobilePhone emailAddress.address],
                                                   contributors: %w[fullName enfCoResponsible],
                                                   company: %w[name id]
                                                 }),
                                   cookies: { JSESSIONID: @jsessionid },
                                   content_type: 'application/json',
                                   accept: 'application/json')
        raise response unless response.code == HTTP_SUCCESS

        JSON.parse(response.body)['data'].first
      rescue RestClient::ExceptionWithResponse => e
        logger.error "Unable to get classroom <#{graf_id}>: #{e.inspect}"
      end
    end

    def classroom_by_identifier(identifier)
      logger.measure_debug "classroom_by_identifier <#{identifier}>" do
        response = RestClient.post("#{@url}/ws/rest/com.axelor.apps.training.db.Course/search",
                                   JSON.generate(data: {
                                                   _domain: 'self.identifier IS :identifier AND self.courseStatus IS NOT 5
                                                         AND self.courseStatus IS NOT 1',
                                                   _domainContext: { identifier: }
                                                 }),
                                   cookies: { JSESSIONID: @jsessionid },
                                   content_type: 'application/json',
                                   accept: 'application/json')
        raise response unless response.code == HTTP_SUCCESS

        JSON.parse(response.body)['data']
      rescue RestClient::ExceptionWithResponse => e
        logger.error "Unable to get classroom with identifier <#{identifier}>: #{e.inspect}"
      end
    end

    def user(graf_id)
      logger.measure_debug "user <#{graf_id}>" do
        response = RestClient.post("#{@url}/ws/rest/com.axelor.apps.base.db.Partner/#{graf_id}/fetch",
                                   JSON.generate(fields: %w[id birthName name firstName updatedOn birthDate
                                                            mobilePhone],
                                                 related: {
                                                   emailAddress: %w[address],
                                                   attendees: %w[birthName name firstName updatedOn birthDate mobilePhone
                                                                 type emailAddressStr attendee]
                                                 }),
                                   cookies: { JSESSIONID: @jsessionid },
                                   content_type: 'application/json',
                                   accept: 'application/json')
        raise response unless response.code == HTTP_SUCCESS

        JSON.parse(response.body)['data'].first
      rescue RestClient::ExceptionWithResponse => e
        logger.error "Unable to get partner <#{graf_id}>: #{e.inspect}"
      end
    end

    def credits_by_skill_block(skill_block_id)
      logger.measure_debug "credits_by_skill_block <#{skill_block_id}>" do
        response = RestClient.post("#{@url}/ws/rest/com.axelor.apps.training.db.Credit/search",
                                   JSON.generate(data: {
                                                   _domain: 'self.skillsBlock.id IS :skb',
                                                   _domainContext: { skb: skill_block_id }
                                                 }),
                                   cookies: { JSESSIONID: @jsessionid },
                                   content_type: 'application/json',
                                   accept: 'application/json')
        raise response unless response.code == HTTP_SUCCESS

        JSON.parse(response.body)['data']
      rescue RestClient::ExceptionWithResponse => e
        logger.error "Unable to get credits for block <#{skill_block_id}>: #{e.inspect}"
      end
    end

    def credit(credit_id)
      logger.measure_debug "credit <#{credit_id}>" do
        response = RestClient.get("#{@url}/ws/rest/com.axelor.apps.training.db.Credit/#{credit_id}",
                                  cookies: { JSESSIONID: @jsessionid },
                                  content_type: 'application/json',
                                  accept: 'application/json')
        raise response unless response.code == HTTP_SUCCESS

        JSON.parse(response.body)['data']
      rescue RestClient::ExceptionWithResponse => e
        logger.error "Unable to get credits <#{credit_id}>: #{e.inspect}"
      end
    end

    def contributors(classroom)
      logger.measure_debug "contributors for classroom <#{classroom.graf_id}>" do
        response = RestClient.post("#{@url}/ws/rest/com.axelor.apps.training.db.Contributor/search",
                                   JSON.generate(data: {
                                                   _domain: 'self.course.id IS :classroom',
                                                   _domainContext: { classroom: classroom.graf_id }
                                                 },
                                                 related: {
                                                   partner: %w[lastName firstName birthDate updatedOn]
                                                 }),
                                   cookies: { JSESSIONID: @jsessionid },
                                   content_type: 'application/json',
                                   accept: 'application/json')
        raise response unless response.code == HTTP_SUCCESS

        JSON.parse(response.body)['data']
      rescue RestClient::ExceptionWithResponse => e
        logger.error "Unable to get contributors for classroom <#{classroom.graf_id}>"
      end
    end

    def teaching_units(classroom_id)
      logger.measure_debug "teaching_units for classroom <#{classroom_id}>" do
        response = RestClient.post("#{@url}/ws/rest/com.axelor.apps.training.db.TeachingUnit/search",
                                   JSON.generate(data: {
                                                   _domain: 'self.course.id IS :classroom',
                                                   _domainContext: { classroom: classroom_id }
                                                 }),
                                   cookies: { JSESSIONID: @jsessionid },
                                   content_type: 'application/json',
                                   accept: 'application/json')
        raise response unless response.code == HTTP_SUCCESS

        JSON.parse(response.body)['data']
      rescue RestClient::ExceptionWithResponse => e
        logger.error "Unable to get teaching_units for classroom <#{classroom_id}>"
      end
    end

    def teaching_sequences(classroom)
      logger.measure_debug "teaching_sequences for classroom <#{classroom.graf_id}>" do
        response = RestClient.post("#{@url}/ws/rest/com.axelor.apps.training.db.Lecture/search",
                                   JSON.generate(data: {
                                                   _domain: 'self.course.id IS :classroom',
                                                   _domainContext: { classroom: classroom.graf_id }
                                                 }),
                                   cookies: { JSESSIONID: @jsessionid },
                                   content_type: 'application/json',
                                   accept: 'application/json')
        raise response unless response.code == HTTP_SUCCESS

        JSON.parse(response.body)['data']
      rescue RestClient::ExceptionWithResponse => e
        logger.error "Unable to get teaching_sequences for classroom <#{classroom.graf_id}>"
      end
    end

    def rotation_location(registration_id)
      logger.measure_debug "rotation_location for registration <#{registration_id}>" do
        response = RestClient.post("#{@url}/ws/rest/com.axelor.apps.training.db.RotationLocation/search",
                                   JSON.generate(data: {
                                                   _domain: 'self.individualRegistration.id IS :registration',
                                                   _domainContext: { registration: registration_id }
                                                 },
                                                 fields: %w[id tutorEmailAddress startDate endDate tutor.birthName tutor.firstName tutor.updatedOn tutor.id
                                                            tutor]),
                                   cookies: { JSESSIONID: @jsessionid },
                                   content_type: 'application/json',
                                   accept: 'application/json')
        raise response unless response.code == HTTP_SUCCESS

        JSON.parse(response.body)['data']
      rescue RestClient::ExceptionWithResponse => e
        logger.error "Unable to get teaching_sequences for classroom <#{classroom.graf_id}>"
      end
    end

    private

    def login(username, password)
      logger.measure_info "endpoint <#{@url}> logging in as <#{username}>" do
        response = RestClient.post("#{@url}/login.jsp", JSON.generate({ username:, password: }),
                                   content_type: 'application/json')
        raise "unable to connect to graf <#{@url}> with default credentials" unless response.code == HTTP_SUCCESS

        response.cookies['JSESSIONID']
      end
    end
  end
end
