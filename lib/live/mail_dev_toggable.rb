module Live
  module MailDevToggable
    def toggle_mail_dev(value, dimensions: nil)
      return unless Rails.env.development?

      cheat_to = Rails.configuration.action_mailer.smtp_settings.dig(:cheat, :to)
      raise 'during development, please override smtp.yml to customize the field <cheat:to>!' if value && !cheat_to

      Rails.configuration.action_mailer.perform_deliveries = value
      logger.error("📬 Can emit email due to MAIL_DEV=true, CHEAT_TO:#{cheat_to}", dimensions:) if value
    end
  end
end
