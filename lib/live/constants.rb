module Live
  module Constants
    extend Definable
    extend MailDevToggable

    MAIL_DEV                = boolean(false) { |v| toggle_mail_dev v }
    STIMULUS_DEBUG          = boolean false
    ACTION_VIEW             = boolean false
    ACTIVE_RECORD           = boolean false
    ACTIVE_RECORD_CACHE     = boolean false
    CLASSROOM_TO_IMPORT     = integer
  end
end
